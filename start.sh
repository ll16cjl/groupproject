#!/usr/bin/env bash
echo "Starting webserver for API..."
# source ./Web/Backend/venv/bin/activate
export FLASK_APP=Web/Backend/app/run.py
echo $FLASK_APP
flask run &

echo "Starting till system..."
mkdir Desktop-App/desktop-build
pushd Desktop-App/desktop-build
qmake ../bike-hire-desktop-app.pro
make
open bike-hire-desktop-app.app
popd

echo "Starting web frontend..."
npm start --prefix ./Web/Frontend/App ||
	echo "Failed while attempting to start frontend, trying npm install..." &
	npm install --prefix ./Web/Frontend/App &
	npm start --prefix ./Web/Frontend/App


echo "All 3 components should now be running."
