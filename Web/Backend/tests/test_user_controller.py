from app import app, db
from controllers.register_controller import RegisterController
import os
import tempfile
import pytest
import json


@pytest.fixture
def client():
	db_fd, app.config['DATABASE'] = tempfile.mkstemp()
	app.config['TESTING'] = True
	basedir = os.path.abspath(os.path.dirname(__file__))
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
	db.create_all()
	client = app.test_client()

	yield client
	db.session.remove()
	db.drop_all()

	os.close(db_fd)
	os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
	return json.loads(resp.data.decode())[key]


def get_auth(client):
	reg_resp = client.post("/register", data=dict(email="testuser@test.com", password="testpassword", first="Chris",
												  second="London", staff=True, staff_pass="CHANGEMETOACOMPLEXPASS"))
	login_resp = client.get("/session", data=dict(email="testuser@test.com", password="testpassword"))
	return json.loads(login_resp.data.decode())["token"]


def test_get_user_info_valid(client):
	token = get_auth(client)
	resp = client.get("/user/info", data=dict(auth_token=token))

	assert get_data_from_resp(resp, "success") is True


def test_get_user_info_invalid(client):
	token = ""
	resp = client.get("/user/info", data=dict(auth_token=token))

	assert get_data_from_resp(resp, "success") is False


def test_update_user_info(client):
	token = get_auth(client)
	resp = client.put("/user/info", data=dict(auth_token=token, first_name="newname"))
	resp = client.get("/user/info", data=dict(auth_token=token))
	assert get_data_from_resp(resp, "first_name") == "newname"


def test_update_user_info_no_change(client):
	token = get_auth(client)
	resp = client.put("/user/info", data=dict(auth_token=token))
	resp = client.get("/user/info", data=dict(auth_token=token))
	assert get_data_from_resp(resp, "first_name") == "Chris"

