from app import app, db
import os
import tempfile
import pytest
import json


@pytest.fixture
def client():
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    basedir = os.path.abspath(os.path.dirname(__file__))
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
    db.create_all()
    client = app.test_client()

    yield client
    db.session.remove()
    db.drop_all()

    os.close(db_fd)
    os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
    return json.loads(resp.data.decode())[key]


def test_register_valid(client):
    """
    Attempts to register with a set of valid data
    """
    email = "t1s@tests.com"
    password = "TestPass123"
    first = "Test"
    second = "Account"
    resp = client.post("/register", data=dict(email=email, password=password, first=first, second=second))
    assert get_data_from_resp(resp, "success") is True


def test_register_taken_email(client):
    """
    Attempts to register with a set of invalid data
    """
    email = "t1@tests.com"
    password = "TestPass123"
    first = "Test"
    second = "Account"
    resp = client.post("/register", data=dict(email=email,
                                              password=password,
                                              first=first,
                                              second=second))  # First entry
    resp = client.post("/register", data=dict(email=email,
                                              password=password,
                                              first=first,
                                              second=second))  # Should fail as this data already exists
    assert get_data_from_resp(resp, "success") is False


def test_register_missing_details(client):
    """
    Attempts to register with data missing
    """
    email = "t1@tests.com"
    password = "TestPass123"
    first = "Test"
    second = "Account"
    resp = client.post("/register", data=dict(password=password,
                                              first=first,
                                              second=second))  # First entry
    assert get_data_from_resp(resp, "success") is False

