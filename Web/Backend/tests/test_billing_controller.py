from app import app, db
import os
import tempfile
import pytest
import json



@pytest.fixture
def client():
	db_fd, app.config['DATABASE'] = tempfile.mkstemp()
	app.config['TESTING'] = True
	basedir = os.path.abspath(os.path.dirname(__file__))
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
	db.create_all()
	client = app.test_client()

	yield client
	db.session.remove()
	db.drop_all()

	os.close(db_fd)
	os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
	return json.loads(resp.data.decode())[key]


def get_auth(client):
	reg_resp = client.post("/register", data=dict(email="testuser@test.com", password="testpassword", first="Chris",
												  second="London"))
	login_resp = client.get("/session", data=dict(email="testuser@test.com", password="testpassword"))
	return json.loads(login_resp.data.decode())["token"]


