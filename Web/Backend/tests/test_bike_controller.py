from app import app, db
from controllers.register_controller import RegisterController
import os
import tempfile
import pytest
import json



@pytest.fixture
def client():
	db_fd, app.config['DATABASE'] = tempfile.mkstemp()
	app.config['TESTING'] = True
	basedir = os.path.abspath(os.path.dirname(__file__))
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
	db.create_all()
	client = app.test_client()

	yield client
	db.session.remove()
	db.drop_all()

	os.close(db_fd)
	os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
	return json.loads(resp.data.decode())[key]


def get_auth(client):
	reg_resp = client.post("/register", data=dict(email="testuser@test.com", password="testpassword", first="Chris",
												  second="London", staff=True, staff_pass="CHANGEMETOACOMPLEXPASS"))
	login_resp = client.get("/session", data=dict(email="testuser@test.com", password="testpassword"))
	return json.loads(login_resp.data.decode())["token"]


def register_test_location(client, token):
	resp = client.post("/location",
					   data=dict(name="testName", city="testCity", latitude="1", longitude="1", auth_token=token))
	assert get_data_from_resp(resp, "success") is True
	return get_data_from_resp(resp, "id")


def test_add_bike_valid(client):
	"""
	Adda a bike ot the database
	:param client:
	:return:
	"""
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))
	assert get_data_from_resp(resp, "success") is True


def test_add_bike_invalid(client):
	"""
	TODO: Implement this once validation has been implemented on the backend
	:param client:
	:return:
	"""
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))
	assert get_data_from_resp(resp, "success") is True


def test_get_bike(client):
	"""
	Add a bike and get a list of all bikes, ensure there is only one in the data
	:param client:
	:return:
	"""
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))

	resp = client.get("/bike", data=dict(auth_token=token, id='*'))
	assert len(get_data_from_resp(resp, "bikes")) is 1


def test_delete_bike(client):
	"""
	Add a bike, get its' ID then delete it
	:param client:
	:return:
	"""
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))

	resp = client.get("/bike", data=dict(auth_token=token, id='*'))

	bike = get_data_from_resp(resp, "bikes")[0]["id"]

	resp = client.delete("/bike", data=dict(auth_token=token, id=bike))
	assert get_data_from_resp(resp, "success") is True


def test_delete_bike_invalid(client):
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"

	# We know that there are not 200 bikes in the test database as we have not added them so we cna use this ID
	resp = client.delete("/bike", data=dict(auth_token=token, id=200))
	assert get_data_from_resp(resp, "success") is False


def test_add_bike_missing_data(client):
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))
	assert get_data_from_resp(resp, "success") is False


def test_alter_bike_details(client):
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))
	resp = client.get("/bike", data=dict(auth_token=token, id=1))
	make = get_data_from_resp(resp, "make")

	assert make is not None

	resp = client.put("/bike", data=dict(auth_token=token, id=1, make="UPDATED_MAKE"))

	assert get_data_from_resp(resp, "success") is True


def test_alter_bike_details_no_change(client):
	token = get_auth(client)
	location_id = register_test_location(client, token)
	price = "100"
	bike_model = "TestModel"
	bike_make = "testMake"
	resp = client.post("/bike", data=dict(model=bike_model, make=bike_make, price=price, location_id=location_id,
										  auth_token=token, category_id=1))
	resp = client.get("/bike", data=dict(auth_token=token, id=1))
	make = get_data_from_resp(resp, "make")

	assert make is not None

	resp = client.put("/bike", data=dict(auth_token=token, id=1))

	assert get_data_from_resp(resp, "success") is True
