from app import app, db
import os
import tempfile
import pytest
import json


@pytest.fixture
def client():
	db_fd, app.config['DATABASE'] = tempfile.mkstemp()
	app.config['TESTING'] = True
	basedir = os.path.abspath(os.path.dirname(__file__))
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
	db.create_all()
	client = app.test_client()

	yield client
	db.session.remove()
	db.drop_all()

	os.close(db_fd)
	os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
	return json.loads(resp.data.decode())[key]


def get_auth(client):
	reg_resp = client.post("/register", data=dict(email="testuser@test.com", password="testpassword", first="Chris",
												  second="London", staff=True, staff_pass="CHANGEMETOACOMPLEXPASS"))

	assert get_data_from_resp(reg_resp, "success") is True

	login_resp = client.get("/session", data=dict(email="testuser@test.com", password="testpassword"))
	return json.loads(login_resp.data.decode())["token"]


def test_create_rental_valid(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location", data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1719781758,
												   bike_ids=id_string, location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))
	assert get_data_from_resp(rental_resp, "success") is True

	pass


def test_create_rental_bad_date_range(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location", data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1519781758,
												   bike_ids=id_string, location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))
	assert get_data_from_resp(rental_resp, "success") is False and get_data_from_resp(rental_resp, "error") == 4000


def test_create_rental_bad_date_range_in_past(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location",
						   data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=10, end_time=5,
												   bike_ids=id_string,
												   location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))
	assert get_data_from_resp(rental_resp, "success") is False and get_data_from_resp(rental_resp, "error") == 4000


def test_create_rental_bikes_in_other_loc(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location", data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))
	loc_resp2 = client.post("/location", data=dict(auth_token=token, name="test2", city="test2", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1719781758,
												   bike_ids=id_string, location_id=get_data_from_resp(loc_resp2, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))
	assert get_data_from_resp(rental_resp, "success") is False


def test_create_rental_bikes_not_avail(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location",
						   data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1719781758,
												   bike_ids=id_string,
												   location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))

	# Try to rent the same bikes at the same time.
	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1719781758,
												   bike_ids=id_string,
												   location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=get_data_from_resp(billing_resp, "id")))
	assert get_data_from_resp(rental_resp, "success") is False and get_data_from_resp(rental_resp, "error") == 10001


def test_create_rental_invalid_billing(client):
	token = get_auth(client)

	# Create a location
	loc_resp = client.post("/location", data=dict(auth_token=token, name="test", city="test", latitude=1, longitude=1))

	# Create some bikes
	bike_1_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_2_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	bike_3_resp = client.post("/bike", data=dict(auth_token=token, location_id=get_data_from_resp(loc_resp, "id"),
												 make="TEST", model="TEST", price=1, category_id=1))
	# Create a billing address
	billing_resp = client.post("/billing", data=dict(auth_token=token, first_line="YEET road", post_code="ME72EJ"))
	id1 = get_data_from_resp(bike_1_resp, "id")
	id2 = get_data_from_resp(bike_2_resp, "id")
	id3 = get_data_from_resp(bike_3_resp, "id")
	id_string = f"[{id1}, {id2}, {id3}]"

	rental_resp = client.post("/rental", data=dict(auth_token=token, start_time=1619781758, end_time=1719781758,
												   bike_ids=id_string, location_id=get_data_from_resp(loc_resp, "id"),
												   payment_id=1, billing_id=10))
	assert get_data_from_resp(rental_resp, "success") is False and get_data_from_resp(rental_resp, "error") == 3001

