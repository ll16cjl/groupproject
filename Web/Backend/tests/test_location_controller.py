from app import app, db
from controllers.register_controller import RegisterController
import os
import tempfile
import pytest
import json


@pytest.fixture
def client():
	db_fd, app.config['DATABASE'] = tempfile.mkstemp()
	app.config['TESTING'] = True
	basedir = os.path.abspath(os.path.dirname(__file__))
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'testdb.db')
	db.create_all()
	client = app.test_client()

	yield client
	db.session.remove()
	db.drop_all()

	os.close(db_fd)
	os.unlink(app.config['DATABASE'])


def get_data_from_resp(resp, key):
	return json.loads(resp.data.decode())[key]


def get_auth(client):
	reg_resp = client.post("/register", data=dict(email="testuser@test.com", password="testpassword", first="Chris",
												  second="London", staff=True, staff_pass="CHANGEMETOACOMPLEXPASS"))
	login_resp = client.get("/session", data=dict(email="testuser@test.com", password="testpassword"))
	return json.loads(login_resp.data.decode())["token"]


def test_add_location(client):
	token = get_auth(client)
	name = "Test"
	city = "CityTest"
	latitude = 1
	longitude = 1

	resp = client.post("/location", data=dict(auth_token=token, latitude=latitude, longitude=longitude, city=city, name=name))

	assert get_data_from_resp(resp, "success") is True


def test_get_single_location(client):
	token = get_auth(client)
	name = "Test"
	city = "CityTest"
	latitude = 1
	longitude = 1
	resp = client.post("/location", data=dict(auth_token=token, latitude=latitude, longitude=longitude, city=city, name=name))

	resp = client.get("/location", data=dict(auth_token=token, id=1))

	assert get_data_from_resp(resp, "success") is True


def test_get_all_locations(client):
	token = get_auth(client)
	name = "Test"
	city = "CityTest"
	latitude = 1
	longitude = 1
	resp = client.post("/location", data=dict(auth_token=token, latitude=latitude, longitude=longitude, city=city, name=name))

	resp = client.get("/location", data=dict(auth_token=token, id='*'))

	assert get_data_from_resp(resp, "success") is True


def test_add_location_invalid(client):
	token = get_auth(client)
	name = "Test"
	city = "CityTest"
	latitude = 1
	longitude = 1

	resp = client.post("/location", data=dict(auth_token=token, longitude=longitude, city=city, name=name))

	assert get_data_from_resp(resp, "success") is False
