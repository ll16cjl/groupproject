from app import db
from models.bike import Bike
from models.location import Location

import math
"""
Allow for many-to-many relationship between
"""
association_table = db.Table('association_rental',
							 db.Column('rental_id', db.Integer, db.ForeignKey('rental.id'), primary_key=True),
							 db.Column('bike_id', db.Integer, db.ForeignKey('bike.id'), primary_key=True))


class Rental(db.Model):
	__tablename__ = "rental"
	id = db.Column("id", db.Integer, primary_key=True)
	start_time = db.Column("start_time", db.DateTime)
	end_time = db.Column("end_time", db.DateTime)
	reservation_time = db.Column("reservation_time", db.DateTime)
	location_id = db.Column("location_id", db.ForeignKey("location.id"))
	user_id = db.Column("user_id", db.ForeignKey("user.id"))
	bike_ids = db.relationship("Bike", secondary=association_table)
	payment_id = db.Column("payment_id", db.ForeignKey("payment.id"))
	billing_id = db.Column("billing_id", db.ForeignKey("billing.id"))

	def __init__(self, start_time, end_time, user_id,  reservation_time, location_id, bike_ids, payment, billing):
		self.start_time = start_time
		self.end_time = end_time
		self.reservation_time = reservation_time

		self.user_id = user_id
		self.location_id = location_id
		self.bike_ids = [Bike.query.filter_by(id=id).first() for id in bike_ids]
		print(self.bike_ids)
		self.payment_id = payment
		self.billing_id = billing

	def commit(self):
		db.session.add(self)
		db.session.commit()

	def get_start_time_epoch(self):
		return int(self.start_time.timestamp())

	def get_end_time_epoch(self):
		return int(self.end_time.timestamp())

	def get_cost(self):
		time_of_rental = ((self.end_time - self.start_time).total_seconds())
		factor = 1

		if time_of_rental >=3*24*60*60:
			factor = 0.5
		elif time_of_rental >= 1*24*60*60:
			factor = 0.25

		bike_costs = sum([bike.price / (60*60) for bike in self.bike_ids])
		total_cost = int(float(bike_costs) * float(time_of_rental)) / 100


		final_cost = (total_cost * factor)
		discount = total_cost - final_cost
		return total_cost, discount

	@staticmethod
	def get_available_bikes(start, end, loc_id):
		"""
		gets a list of bikes that can be rented for a specific rental time period at a specific location
		:param start: UNIX epoch of the start time
		:param end: UNIX epoch of the end time
		:param loc_id: ID of the location
		:return: A list of bikes that can be rented in a given period and location
		"""
		bikes_in_location = [x.id for x in Bike.query.filter_by(location=loc_id).all()]

		# get all of the rentals in the selected location
		for rental in Rental.query.filter_by(location_id=loc_id).all():
			if start > rental.get_start_time_epoch():
				continue

			# If the this rental starts after the new booking and does not start after
			if rental.get_start_time_epoch() <= start <= rental.get_end_time_epoch() and \
					end <= rental.get_end_time_epoch():
				for bike in [x.id for x in rental.bike_ids]:
					if bike in bikes_in_location:
						bikes_in_location.remove(bike)

			if rental.get_start_time_epoch() >= start <= rental.get_end_time_epoch() \
					and end <= rental.get_end_time_epoch():
				for bike in [x.id for x in rental.bike_ids]:
					if bike in bikes_in_location:
						bikes_in_location.remove(bike)

			if rental.get_start_time_epoch() <= start <= rental.get_end_time_epoch() <= end:
				for bike in [x.id for x in rental.bike_ids]:
					if bike in bikes_in_location:
						bikes_in_location.remove(bike)

			if rental.get_start_time_epoch() >= start <= rental.get_end_time_epoch() <= end:
				for bike in [x.id for x in rental.bike_ids]:
					if bike in bikes_in_location:
						bikes_in_location.remove(bike)

		# Return the list of bikes that are available in the location for the specified dates
		return bikes_in_location

	@staticmethod
	def get_available_bikes_all(start_time, end_time):
		bikes = []
		for location in Location.query.all():
			bikes += Rental.get_available_bikes(start_time, end_time, location.id)
		return bikes


