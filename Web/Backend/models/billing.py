from app import db

class Billing(db.Model):
	__tablename__ = "billing"
	id = db.Column("id", db.Integer, primary_key=True)
	user_id = db.Column("user_id", db.ForeignKey("user.id"))
	first_line = db.Column("first_line", db.String)
	post_code = db.Column("post_code", db.String)

	def __init__(self, user_id, first_line, post_code):
		self.user_id = user_id
		self.first_line = first_line
		self.post_code = post_code

	def commit(self):
		db.session.add(self)
		db.session.commit()

