from app import db
from flask_sqlalchemy import inspect
from app import app


class Location(db.Model):
	__tablename__ = "location"
	id = db.Column("id", db.Integer, primary_key=True)
	name = db.Column("name", db.String)
	city = db.Column("location", db.String)
	latitude = db.Column("latitude", db.Numeric(10, 5))
	longitude = db.Column("longitude", db.Numeric(10, 5))

	def __init__(self, name, city, latitude, longitude):
		self.name = name
		self.city = city
		self.latitude = latitude
		self.longitude = longitude

	def commit(self):
		db.session.add(self)
		db.session.commit()
