from app import db
import datetime
from models.billing import Billing
from models.exceptions.invalid_payment_exception import InvalidPaymentException


class Payment(db.Model):
	__tablename__ = "payment"
	id = db.Column("id", db.Integer, primary_key=True)
	user_id = db.Column("user_id", db.ForeignKey("user.id"))
	bank_name = db.Column("bank_name", db.String)
	card_type = db.Column("card_type", db.String)
	card_number = db.Column("card_number", db.String)
	expiry_date = db.Column("expiry_date", db.DateTime)
	name_on_card = db.Column("name_on_card", db.String)
	billing_address_id = db.Column("billing_address_id", db.ForeignKey("billing.id"))

	def __init__(self, user_id, bank_name, card_type, card_number, expiry_date, name_on_card, billing_address_id,
				 																					system=False):
		self.user_id = user_id
		self.bank_name = bank_name
		self.card_type = card_type
		self.card_number = card_number
		self.expiry_date = expiry_date
		self.name_on_card = name_on_card
		self.billing_address_id = billing_address_id

		# If this is true then this is one of the default payment methods for the till system and should not be validated
		if not system:
			if len(card_number) is not 16 or self.is_expired(expiry_date) or \
							Billing.query.get(int(billing_address_id)) is None:
				raise InvalidPaymentException("Invalid card details supplied!")

	def commit(self):
		db.session.add(self)
		db.session.commit()

	def take_payment(self, ccv):
		"""
		This method is just used to simulate payment verification. It takes the CCV code as it would normally and simply
		returns true providing the expiry date is OK etc
		:return: Status of payment
		"""
		# Then the card has expired
		if self.is_expired(self.expiry_date) or len(ccv) is not 3:
			return False
		return True

	def is_expired(self, date):
		"""
		Determines if a card is expired or not
		:param date: UNIX epoch
		:return: True or False
		"""
		return int(int(date.timestamp())) < int(datetime.datetime.today().timestamp())

