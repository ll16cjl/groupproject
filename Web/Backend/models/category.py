from app import db
from flask_sqlalchemy import inspect
from app import app


class Category(db.Model):
	__tablename__ = "category"
	id = db.Column("id", db.Integer, primary_key=True)
	category = db.Column("category", db.String)

	def __init__(self, category):
		self.category = category

	def commit(self):
		db.session.add(self)
		db.session.commit()

