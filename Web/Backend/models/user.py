from app import db
from flask_sqlalchemy import inspect
from app import auth, app
import bcrypt
import random
import string
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
import re
from models.exceptions.invalid_register_details_exception import InvaliRegisterDetailsException

priv_levels = {
	"user": 0,
	"staff": 1
}


class User(db.Model):
	__tablename__ = "user"
	id = db.Column("id", db.Integer, primary_key=True)
	email = db.Column("email", db.String(100), unique=True)
	hashed_pw = db.Column("hashed_pw", db.String(100), index=True)

	# TODO: Change the possible length for names
	first_name = db.Column("first_name", db.String(50), index=True)
	second_name = db.Column("last_name", db.String(50), index=True)

	privelege_level = db.Column("privelege_level", db.Integer)
	current_token = db.Column("current_token", db.String)  # This is used to store the most recent auth token.

	def __init__(self, email, first, second, privelege_level, passw=None):
		passw = passw if passw is not None else User.auto_gen_password()

		self.email = email
		self.first_name = first
		self.hashed_pw = self.encrypt_pass(passw.encode())  # Bcrypt requires we encode the string before we hash it
		self.second_name = second
		self.privelege_level = privelege_level

	def commit(self):
		db.session.add(self)
		db.session.commit()

	def verify_register_details(self):
		"""
		Verifies that the details supplied to the constructor earlier (stored in this instance) are valid,
		and the email is unique etc
		:returns: True if all values are OK, False otherwise
		"""
		email_regex = "^.+@.+\.[a-zA-Z]{2,3}$"

		if not 0 < len(self.first_name) < 20:
			raise InvaliRegisterDetailsException("Invalid first na?me!")
		if not 0 < len(self.second_name) < 20:
			raise InvaliRegisterDetailsException("Invalid second name!")
		if not re.match(email_regex, self.email):
			raise InvaliRegisterDetailsException("Invalid email!")
		if not User.query.filter_by(email=self.email).first():
			return True
		raise InvaliRegisterDetailsException("Email already in use!")

	def is_staff(self):
		return self.privelege_level is 1

	@auth.verify_password
	def verify_password(email, password):
		"""
		Verifies the password against the stored one in the database
		:param password: The provided password
		:return: True if details match
		"""

		# This function is almost identical to compare_password atm but we may want to check for more things in the
		# future so should stick with this for now
		user_obj = User.query.filter_by(email=email).first()
		if not user_obj:
			return False
		return bcrypt.checkpw(password.encode(), user_obj.hashed_pw)

	def encrypt_pass(self, raw):
		"""
		Uses BCrypt to encrypt a password and returns the hashed password
		:param raw: The raw password
		:return: The hashed password
		"""
		return bcrypt.hashpw(raw, bcrypt.gensalt())

	@staticmethod
	def verify_token(token):
		"""
		Verifies a token is valid and not expired
		:param token: The token to compare as an encoded string
		:return: Whether the token is valid or not
		"""
		if token is None:
			return None
		token = token.encode().decode("utf-8")  # Ensure the token is fully decoded
		serializer = Serializer(app.config["SECRET_KEY"])
		try:
			serializer.loads(token)
		except BadSignature:
			return None
		except SignatureExpired:
			return None
		return User.query.filter_by(id=id)

	def generate_token(self):
		"""
		Generates an authentication token for this user
		:return: A token for the specified user
		"""
		serializer = Serializer(app.config["SECRET_KEY"], expires_in=20000)
		token = serializer.dumps({'id': self.id}).decode()
		self.current_token = token
		self.commit()
		return token

	def change_email(self, email):
		"""
		Changes the user email if it is not in use.
		:param email: New email
		:return: True if changed, False if the email is already in use
		"""
		if User.query.filter_by(email=email).first() is None:
			self.email = email
			self.commit()
			return True
		return False

	def change_password(self, passw):
		"""
		Changes the user password. Assumes it is valid
		:param passw: String of the raw password
		:return:
		"""
		self.hashed_pw = self.encrypt_pass(passw.encode())
		self.commit()

	@staticmethod
	def get_current_user_by_token(token):
		return User.query.filter_by(current_token=token).first()

	@staticmethod
	def auto_gen_password():
		"""
		Auto generates a password of length 10. Mainly used by the store app to generate a
		password for the user
		:return: A string of length 10
		"""
		chars = string.ascii_uppercase + string.digits + string.ascii_lowercase
		return "".join(random.choice(chars) for _ in range(10))

