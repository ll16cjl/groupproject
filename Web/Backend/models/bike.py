from app import db
from flask_sqlalchemy import inspect
from app import app
from models.category import Category

class Bike(db.Model):
	__tablename__ = "bike"
	id = db.Column("id", db.Integer, primary_key=True)
	model = db.Column("model", db.String)
	location = db.Column("location_id", db.ForeignKey("location.id"))
	make = db.Column("make", db.String)
	price = db.Column("price", db.Integer)
	category_id = db.Column("category_id", db.ForeignKey("category.id"))

	def __init__(self, model, location_id, make, price, category):
		self.model = model
		self.location = location_id
		self.make = make
		self.price = price
		self.category_id = category

	def commit(self):
		db.session.add(self)
		db.session.commit()

