def is_field_empty(field):
	"""
	Will determine if a field is empty or not.
	used to check params supplied to the API
	:param field: any string or var
	:return: True if empty, False otherwise
	"""
	return field is None or field is "" or field is " "


def is_any_field_empty(fields):
	"""
	Will check a dictionary of parameters to see if any are empty
	:param fields: A dictionary of params
	:return: True if any of the params are empty
	"""
	for v in fields.values():
		if is_field_empty(v):
			return True


def verify_auth_token(token):
	"""
	Will verify an authentication token is OK
	Checks if the token is empty, invalid or None. If it is neither of those, it will then
	verify it against the user model function used to verify the status of an existing token
	:param token: String representing the token
	:return: True of OK, False otherwise
	"""
	from models.user import User  # Import this here so it doesn't screw with the imports in all controllers
	return (token is not None) and (token is not "") and (token is not " ") and (User.verify_token(token))


def is_staff(token):
	"""
	Determines if an auth token is assigned to a staff member or regular user.
	:param token: A valid auth token
	:return: True or False
	"""
	from models.user import User
	return User.get_current_user_by_token(token).is_staff()
