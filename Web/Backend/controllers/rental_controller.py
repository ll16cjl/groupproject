import logging
from flask_restful import Resource, reqparse
from models import Payment, Billing
from models.user import User
from flask import jsonify
from models.rental import Rental
from controllers import is_any_field_empty, verify_auth_token, is_staff
from threading import Lock
import ast
from datetime import datetime
from serverutil import email


class RentalController(Resource):
	def __init__(self):
		self.mutex = Lock()

	def post(self):
		"""
		Creates a new rental instance in the database
		:param start_time: UNIX epoch of the start time
		:param end_time: UNIX epoch of the end time
		:param auth_token: Valid user token
		:param location_id: The ID of the location for the rental to take place
		:param bike_ids: An array of bike ID's desired. Should be valid and use the availability endpoint to ensure this
		:return: Status
		"""
		self.mutex.acquire()  # Acquire the lock to ensure only one booking is made at a time

		parser = reqparse.RequestParser()
		parser.add_argument('start_time')
		parser.add_argument('end_time')
		parser.add_argument('auth_token')
		parser.add_argument('location_id')
		parser.add_argument('payment_id')
		parser.add_argument('billing_id')
		parser.add_argument('bike_ids')  # Should be an array regardless of size
		args = parser.parse_args()
		reservation_time = datetime.now()
		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error 10 when creating rental")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error 9 when creating rental")
			return jsonify(success=False, error=9)

		if int(args["end_time"]) <= int(args["start_time"]):
			logging.error("Got error 4000 when creating rental")
			return jsonify(success=False, error=4000)

		if int(args["start_time"]) < datetime.now().timestamp():
			logging.error("Got error 4001 when creating rental")
			return jsonify(success=False, error=4001)

		avail_bikes = set(Rental.get_available_bikes(int(args["start_time"]),
													 int(args["end_time"]),
													 int(args["location_id"])))

		if not set(ast.literal_eval(args["bike_ids"])).issubset(avail_bikes):
			logging.error("Got error 10001 when creating rental")
			return jsonify(success=False, error=10001)

		user_payment_methods = [x.id for x in
						Payment.query.filter_by(user_id=User.get_current_user_by_token(args["auth_token"]).id).all()]

		user_payment_methods.append(1)
		user_payment_methods.append(2)

		user_billing = [x.id for x in
						Billing.query.filter_by(user_id=User.get_current_user_by_token(args["auth_token"]).id).all()]

		if int(args["payment_id"]) not in user_payment_methods:
			logging.error("Got error 3000 when creating rental")
			return jsonify(success=False, error=3000)

		if int(args["billing_id"]) not in user_billing:
			logging.error("Got error 3001 when creating rental")
			return jsonify(success=False, error=3001)

		reservation = Rental(datetime.fromtimestamp(int(args["start_time"])),
							 datetime.fromtimestamp(int(args["end_time"])),
							 User.get_current_user_by_token(args["auth_token"]).id,
							 reservation_time,
							 int(args["location_id"]),
							 ast.literal_eval(args["bike_ids"]),
							 int(args["payment_id"]),
							 int(args["billing_id"]))
		reservation.commit()

		email.send_receipt_email(reservation)

		self.mutex.release()
		logging.info(f"Created new rental with ID {reservation.id}")
		return jsonify(success=True)

	def get(self):
		"""
		Returns all of the rentals by a specific user
		:param auth_token: Auth token of a user
		:return: An array of JSON objects
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('auth_token')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error 10 when getting rental")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error 9 when getting rental")
			return jsonify(success=False, error=9)

		if is_staff(args["auth_token"]):
			logging.info("Returning all rental data for staff member")
			reservations = Rental.query.filter_by().all()
			reservations = [{"start_time": int(x.start_time.timestamp()),
							 "end_time": int(x.end_time.timestamp()),
							 "id": x.id,
							 "reservation_time": int(x.reservation_time.timestamp()),
							 "user_id": x.user_id,
							 "location_id": x.location_id,
							 "bikes_ids": [bike.id for bike in x.bike_ids]} for x in reservations]
		else:
			reservations = Rental.query.filter_by(user_id=User.get_current_user_by_token(args["auth_token"]).id).all()
			reservations = [{"start_time": int(x.start_time.timestamp()),
							 "end_time": int(x.end_time.timestamp()),
							 "id": x.id,
							 "reservation_time": int(x.reservation_time.timestamp()),
							 "user_id": x.user_id,
							 "location_id": x.location_id,
							 "bikes_ids": [bike.id for bike in x.bike_ids]} for x in reservations]

		if reservations is not None and len(reservations) > 0:
			return jsonify(success=True, reservations=reservations)
		else:
			logging.error("Got error 10003 when getting rental")
			return jsonify(success=False, error=10003)  # TODO: Add warning to wiki
