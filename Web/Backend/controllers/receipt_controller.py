import logging
from flask_restful import Resource, reqparse
from serverutil.receipt import generate_receipt
from models.rental import Rental
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty


class ReceiptController(Resource):
	def get(self):
		"""
		Gets receipt data for a rental
		:param id: ID of the rental
		:param auth_token: auth_token of the user for this rental
		:return:
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('id')
		parser.add_argument("auth_token")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error code 10 when getting receipt data")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error code 9 when getting receipt data")
			return jsonify(success=False, error=9)

		receipt = generate_receipt(Rental.query.filter_by(id=args["id"]).first())

		if receipt is None:
			logging.error("Got error code 2 when getting receipt data")
			return jsonify(success=False, error=2)
		rent_id = args["id"]
		logging.info(f"Got receipt data for payment with ID {rent_id}")
		return jsonify(success=True,
					   name=receipt[0],
					   bikes=receipt[1],
					   location_city=receipt[2],
					   location_name=receipt[3],
					   cost=receipt[4],
					   discount=receipt[5],
					   start=receipt[6],
					   end=receipt[7],
					   digits=receipt[8][-4:]
					   )
