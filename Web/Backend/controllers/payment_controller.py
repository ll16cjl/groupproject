import logging
from flask_restful import Resource, reqparse
from models.payment import Payment
from models.user import User
from flask import jsonify
from datetime import datetime
from controllers import verify_auth_token, is_any_field_empty


class PaymentController(Resource):
	# TODO: Bank name needs to be removed fully
	def get(self):
		"""
		Returns a single payment method instance or all if '*' supplied
		:param id: ID of the payment method or wildcard * for all
		:param auth_token: token for the user
		:returns: P{ayment method data either as an array or singular object
		"""

		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("id")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error code 10 getting payment details")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error code 9 getting payment details")
			return jsonify(success=False, error=9)

		current_user = User.get_current_user_by_token(args["auth_token"])

		if current_user is None:
			logging.error("Got error code 8 getting payment details")
			return jsonify(success=False, error=8)

		# We need to return all location entries
		if args["id"] == "*":
			logging.info(f"Getting all payment method data for ID {current_user.id}")
			payment_methods = [{"id": x.id,
								"card_type": x.card_type,
								"card_number": x.card_number,
								"bank_name": x.bank_name,
								"expiry_date": x.expiry_date,
								"name_on_card": x.name_on_card,
								"billing_address_id": x.billing_address_id
								}
							   for x in Payment.query.filter_by(user_id=current_user.id).all()]
			return jsonify(success=True, payment_methods=payment_methods)

		elif args["id"] is not (None or "" or " "):
			payment = Payment.query.filter_by(id=args["id"]).first()
			logging.info(f"Getting payment method with ID {payment.id} for ID {current_user.id}")

			if payment is None:
				logging.error("Got error code 3 getting payment details")
				return jsonify(success=False, error=3)

			return jsonify(success=True, id=payment.id, card_type=payment.card_type, card_number=payment.card_number,
						   bank_name=payment.bank_name, expiry_date=payment.expiry_date,
						   name_on_card=payment.name_on_card, billing_address_id=payment.billing_address_id)
		logging.error("Got error code 3 getting payment details")
		return jsonify(success=False, error=3)

	def post(self):
		"""
		:param auth_token: Auth token for the user
		:param card_type: VISA, Mastercard etc
		:param card_number: Card number for the payment method
		:param expiry_date: UNIX epoch for expiry date
		:param name_on_card: Name on the card
		:param billing_address_id: ID of the billing address to associate this card with
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("card_type")
		parser.add_argument("card_number")
		parser.add_argument("expiry_date")
		parser.add_argument("name_on_card")
		parser.add_argument("billing_address_id")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error code 10 when creating new payment method")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error code 9 when creating new payment method")
			return jsonify(success=False, error=9)

		current_user = User.get_current_user_by_token(args["auth_token"])

		payment = Payment(current_user.id, "", args["card_type"], args["card_number"],
						  datetime.fromtimestamp(int(args["expiry_date"])), args["name_on_card"],
						  args["billing_address_id"])
		payment.commit()
		logging.info(f"Created new payment method with ID {payment.id} dor user {current_user.id}")
		return jsonify(success=True)
