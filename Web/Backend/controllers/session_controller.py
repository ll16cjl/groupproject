import logging
from flask_restful import Resource, reqparse
from models.user import User
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty


class SessionController(Resource):
	def get(self):
		"""
		Logs a user in
		:param email: The user's email address
		:param password: The password of the user
		:return: A valid authentication token if the credentials are valid
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('email')
		parser.add_argument('password')
		args = parser.parse_args()
		email = args["email"]
		raw_pass = args["password"]

		if is_any_field_empty(args):
			logging.error("Got error code 9 when getting authentication token")
			return jsonify(success=False, error=9)

		user = User.query.filter_by(email=email).first()

		if user is None:
			logging.error("Got error code 8 when getting authentication token")
			return jsonify(success=False, error=8)

		if User.verify_password(email, raw_pass):
			logging.info(f"Returned auth token with email {email}")
			return jsonify({"success": True, "token": user.generate_token()})
		else:
			logging.error("Got error code 10 when getting authentication token")
			return jsonify(success=False, error=10)

	def delete(self):
		"""
		Logs a user out by deleting and invalidating the current issued token
		:param auth_token: The current auth token for the user
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('auth_token')
		args = parser.parse_args()
		token = args["auth_token"]

		if is_any_field_empty(args):
			logging.error("Got error code 9 when deleting authentication token")
			return jsonify(success=False, error=9)
		if not verify_auth_token(token):
			logging.error("Got error code 10 when deleting authentication token")
			return jsonify(success=False, error=10)

		user = User.get_current_user_by_token(token)
		user.current_token = ""
		user.commit()
		logging.info(f"Deleted authentication token for user with email {user.email} and ID {user.id}")
		return jsonify(success=True)
