import logging
from flask_restful import Resource, reqparse
from models.user import User
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty
from models.billing import Billing


class BillingController(Resource):
	def get(self):
		"""
		:param auth_token: current user's auth token
		:param id: ID of the billing address instance or *
		:return: A billing address instance or array of instances
		"""
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("id")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Got error code 10 when getting billing info")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Got error code 9 when getting billing info")
			return jsonify(success=False, error=9)

		current_user = User.get_current_user_by_token(args["auth_token"])
		# We need to return all location entries
		if args["id"] == "*":
			user_id = args["id"]
			logging.info(f"Returning all billing info for {user_id}")
			billing_addresses = [{"id": x.id,
								  "first_line": x.first_line,
								  "post_code": x.post_code
								  }
								 for x in Billing.query.filter_by(user_id=current_user.id).all()]
			return jsonify(success=True, addresses=billing_addresses)

		elif args["id"] is not (None or "" or " "):
			address = Billing.query.filter_by(id=args["id"]).first()
			logging.info(f"Returning billing info for ID {address.id}")
			return jsonify(success=True, id=address.id, first_line=address.first_line, post_code=address.post_code)
		return jsonify(success=False, error=6)

	def post(self):
		"""
		:param auth_token: Auth token of the current user
		:param first_line: First line of the address
		:param post_code: Post code
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("first_line")
		parser.add_argument("post_code")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Got error code 10 when getting billing info")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Got error code 9 when getting billing info")
			return jsonify(success=False, error=9)

		current_user = User.get_current_user_by_token(args["auth_token"])

		if current_user is None:
			logging.error(f"Got error code 8 when getting billing info")
			return jsonify(success=False, error=8)

		address = Billing(current_user.id, args["first_line"], args["post_code"])
		address.commit()
		logging.info(f"Added new billing details for {current_user.id}")
		return jsonify(success=True, id=address.id)
