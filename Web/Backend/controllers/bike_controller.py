import logging
from flask_restful import Resource, reqparse
from sqlalchemy.orm.exc import UnmappedInstanceError
from models.bike import Bike
from models.location import Location
from flask import jsonify
from app import db
from controllers import verify_auth_token, is_any_field_empty, is_staff
from models.category import Category


class BikeController(Resource):

	def get(self):
		"""
		:param id: ID of the bike to get
		:param auth_token: user's auth token
		:return: A bike object or array of bike objects
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('id')
		parser.add_argument('auth_token')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Error code 10 for returning bike")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Error code 9 for returning bike")
			return jsonify(success=False, error=9)
		logging.info(args["auth_token"])
		logging.info(is_staff(args["auth_token"]))
		id = args["id"]

		if id == "*":
			logging.info(f"Returning all bikes")
			bikes = Bike.query.all()
			return jsonify(success=True, bikes=[{"id": bike.id,
												 "location_id": bike.location,
												 "model": bike.model,
												 "make": bike.make,
												 "category": Category.query.filter_by(id=bike.category_id).first().category,
												 "price": float(bike.price)} for bike in bikes])

		bike = Bike.query.filter_by(id=id).first()

		if bike is None:
			logging.error(f"Error code 7 for returning bike")
			return jsonify(success=False, error=7)

		if bike is not None:
			logging.error(f"Returning bike information for ID {bike.id}")
			return jsonify(success=True,
						   id=bike.id,
						   location_id=bike.location,
						   location_name=Location.query.filter_by(id=bike.location).first().name,
						   model=bike.model,
						   make=bike.make,
						   category=Category.query.filter_by(id=bike.category_id).first().category,
						   price=float(bike.price))
		logging.error(f"Error code 7 for returning bike")
		return jsonify(success=False, error=7)

	def post(self):
		"""
		Requires staff token
		Adds a new bike to the database
		:param location_id: Where the bike is
		:param model: Model of the bike
		:param make: Make of the bike
		:param price: Price in PENNIES per hour.
		:param auth_token: User's auth token
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('location_id')
		parser.add_argument('model')
		parser.add_argument('make')
		parser.add_argument('price')
		parser.add_argument('category_id')
		parser.add_argument('auth_token')
		parser.add_argument('quantity')
		args = parser.parse_args()

		quantity = args["quantity"]
		args.pop("quantity")

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Error code 10 for returning bike")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Error code 9 for returning bike")
			return jsonify(success=False, error=9)
		if args["price"] == "0":
			logging.error(f"Error code 11 for returning bike")
			return jsonify(success=False, error=11)

		if args["category_id"] == "0":
			logging.error(f"Error code 11 for returning bike")
			return jsonify(success=False, error=11)

		if not is_staff(args["auth_token"]):
			logging.error(f"Error code 1000 for returning bike")
			return jsonify(success=False, error=1000)
		if quantity is not None and quantity != "" and quantity != " ":
			logging.info(f"Adding {quantity} bikes")
			quantity = int(quantity)
			ids = []
			for i in range(quantity):
				bike = Bike(args["model"], args["location_id"], args["make"], args["price"], args["category_id"])
				bike.commit()
				ids.append(bike.id)
			return jsonify(success=True, ids=ids)

		bike = Bike(args["model"], args["location_id"], args["make"], args["price"], args["category_id"])
		bike.commit()
		logging.info(f"Added bike with ID {bike.id}")
		return jsonify(success=True, id=bike.id)

	def put(self):
		"""
		Requires staff token
		Updates a bike's info
		:param location_id: Where the bike is
		:param model: Model of the bike
		:param make: Make of the bike
		:param price: Price in PENNIES per hour.
		:param auth_token: User's auth token
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('id')
		parser.add_argument('location_id')
		parser.add_argument('model')
		parser.add_argument('make')
		parser.add_argument('price')
		parser.add_argument('category_id')
		parser.add_argument('auth_token')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Error 10 when updating bike data")
			return jsonify(success=False, error=10)

		if not is_staff(args["auth_token"]):
			logging.error(f"Error 1000 when updating bike data")
			return jsonify(success=False, error=1000)  # TODO: Add error to wiki

		bike = Bike.query.filter_by(id=args["id"]).first()

		if bike is None:
			logging.error(f"Error 80 when updating bike data")
			return jsonify(success=False, error=80)

		bike.model = args["model"] if args["model"] is not None else bike.model
		bike.location = args["location_id"] if args["location_id"] is not None else bike.location
		bike.make = args["make"] if args["make"] is not None else bike.make
		bike.make = args["category_id"] if args["category_id"] is not None else bike.category_id

		if args["price"] is not None and args["price"] is not "":
			bike.price = float(args["price"]) if args["price"] is not None else bike.price

		bike.commit()
		id_bike = args["id"]
		logging.info(f"Updated bike with ID {id_bike}")

		return jsonify(success=True)

	def delete(self):
		"""
		Requires staff token
		Delete a bike from the database
		:param id: ID of the bike to delete
		:param auth_token: User's auth token
		:return: Status
		"""
		try:
			parser = reqparse.RequestParser()
			parser.add_argument('id')
			parser.add_argument('auth_token')
			args = parser.parse_args()
			if not verify_auth_token(args["auth_token"]):
				logging.error(f"Error 10 when updating bike data")
				return jsonify(success=False, error=10)

			if is_any_field_empty(args):
				logging.error(f"Error 9 when updating bike data")
				return jsonify(success=False, error=9)

			if not is_staff(args["auth_token"]):
				logging.error(f"Error 1000 when updating bike data")
				return jsonify(success=False, error=1000)  # TODO: Add error to wiki

			bike = Bike.query.filter_by(id=args["id"]).first()

			if bike is None:
				logging.error(f"Error 80 when updating bike data")
				return jsonify(success=False, error=80)

			bike_id = bike.id

			db.session.delete(bike)
			db.session.commit()
			logging.info(f"Deleted bike with ID {bike_id} from database")
			return jsonify(success=True)
		except UnmappedInstanceError:
			logging.error(f"Error 100 when updating bike data")
			return jsonify(success=False, error=100)
		except AttributeError:
			logging.error(f"Error 100 when updating bike data")
			return jsonify(success=False, error=100)

