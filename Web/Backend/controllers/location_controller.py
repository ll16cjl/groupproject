import logging
from flask_restful import Resource, reqparse
from models.location import Location
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty, is_staff


class LocationController(Resource):
	def get(self):
		"""
		:param id: ID of the location
		:param auth_token: Auth token of the current user
		:return: Either an array of location objects or a single location object
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('id')
		parser.add_argument("auth_token")
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error code 10 when getting location data")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error code 9 when getting location data")
			return jsonify(success=False, error=9)

		# We need to return all location entries
		if args["id"] == "*":
			logging.info(f"Returning all location data")
			locations = [{"id": x.id, "name": x.name, "city": x.city, "latitude": float(x.latitude),
						  "longitude": float(x.longitude)} for x in Location.query.filter_by().all()]
			return jsonify(success=True, locations=locations)
		elif args["id"] is not (None or "" or " "):
			loc_id = args["id"]
			logging.info(f"Returning location data for ID {loc_id}")
			location = Location.query.filter_by(id=args["id"]).first()
			return jsonify(success=True, id=location.id, name=location.name, city=location.city,
						   latitude=float(location.latitude), longitude=float(location.longitude))
		logging.error("Got error code 5 when getting location data")

		return jsonify(success=False, error=5)

	def post(self):
		"""
		Requires staff account
		:param name: Name of the location
		:param city: City it is in
		:param latitude: 
		:param longitude:
		:param auth_token: Auth token of the current user
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('name')
		parser.add_argument('city')
		parser.add_argument('latitude')
		parser.add_argument('longitude')
		parser.add_argument("auth_token")
		args = parser.parse_args()
		if not verify_auth_token(args["auth_token"]):
			logging.error("Got error code 10 when getting location data")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error("Got error code 9 when getting location data")
			return jsonify(success=False, error=9)

		if not is_staff(args["auth_token"]):
			logging.error("Got error code 1000 when getting location data")
			return jsonify(success=False, error=1000)

		location = Location(args["name"],args["city"],args["latitude"],args["longitude"])
		location.commit()

		name = args["name"]
		city = args["city"]
		logging.info(f"Added new location called {name} in {city}")
		return jsonify(success=True, id=location.id)
