import logging
from flask_restful import Resource, reqparse
from models.user import User, priv_levels
from flask import jsonify
from app import db
from serverutil.email import send_registration_email
from controllers import is_any_field_empty
from models.exceptions.invalid_register_details_exception import InvaliRegisterDetailsException

class RegisterController(Resource):
	def post(self):
		"""
		Will input a new user account into the database
		:param email: email address for the new account
		:param first: User's first name
		:param second: User's second name
		:param password: User's password
		:param staff: Whether to create a staff account
		:param staff_pass: Secret key used to authenticate staff account creation
		:return json: The status of the request, success=True or success=False as a JSON object depending on outcome
		"""

		parser = reqparse.RequestParser()
		parser.add_argument('email')
		parser.add_argument('password')
		parser.add_argument('first')
		parser.add_argument('second')
		parser.add_argument('staff')
		parser.add_argument('staff_pass')
		args = parser.parse_args()
		password = args["password"]
		email = args["email"]
		first = args["first"]
		second = args["second"]
		staff = args["staff"]
		staff_pass = args["staff_pass"]

		args.pop("staff")
		args.pop("staff_pass")

		if is_any_field_empty(args):
			logging.error("Got error code 9 when registering a user")
			return jsonify(success=False, error=9)

		try:
			if staff is not None:
				if (staff_pass.encode().decode()) == "CHANGEMETOACOMPLEXPASS":
					user_obj = User(email, first, second, priv_levels["staff"], passw=password) if password is not None\
						else User(email, first, second, priv_levels["staff"])
					if user_obj.verify_register_details():
						db.session.add(user_obj)
						db.session.commit()
						send_registration_email(user_obj)
						logging.info(f"Registered a new staff user with email {user_obj.email}")
						return jsonify(success=True)
			else:
				user_obj = User(email, first, second, priv_levels["user"], passw=password) if password is not None \
					else User(email, first, second, priv_levels["user"])
				if user_obj.verify_register_details():
					db.session.add(user_obj)
					db.session.commit()
					send_registration_email(user_obj)
					logging.info(f"Registered a new normal user with email {user_obj.email}")
					return jsonify(success=True)
			logging.error("Got error 1 when registering new user")
			return jsonify(success=False, error=1)
		except InvaliRegisterDetailsException:
			logging.error("Got error 10002 when registering new user")
			return jsonify(success=False, error=10002)  # TODO: Add error to wiki
