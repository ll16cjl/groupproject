import logging
from flask_restful import Resource, reqparse
from models.bike import Bike
from models.rental import Rental
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty


class BikeFinderController(Resource):
	def get(self):
		"""
		Returns an array of bike objects that can be hired in a given time period from a location.
		:param start_time: UNIX epoch of the start of the rental
		:param end_time: UNIX epoch of the end of the rental
		:param location_id: ID of the location the user wishes to rent from
		:param auth_token: auth token for the user
		:return: An array of available bikes. May be empty.
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('start_time')
		parser.add_argument('end_time')
		parser.add_argument('auth_token')
		parser.add_argument('location_id')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Got error 10 when getting available bikes")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Got error 9 when getting available bikes")
			return jsonify(success=False, error=9)

		start = args["start_time"]
		end = args["end_time"]
		loc_id = args["location_id"]

		if args["location_id"] == "*":
			logging.info(f"Returning available bikes for {start} to {end} at all locations")
			return jsonify(success=True, bikes=Rental.get_available_bikes_all(
																	  int(args["start_time"]),
																	  int(args["end_time"])))

		logging.info(f"Returning available bikes for {start} to {end} at location ID {loc_id}")
		return jsonify(success=True, bikes=Rental.get_available_bikes(int(args["start_time"]),
																	  int(args["end_time"]),
																	  int(args["location_id"])))

