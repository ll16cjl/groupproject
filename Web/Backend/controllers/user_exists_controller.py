import logging
from flask_restful import Resource, reqparse
from models.user import User
from flask import jsonify
from controllers import is_any_field_empty


class UserExistsController(Resource):

	def get(self):
		"""
		Returns whether a user exits in the database. Used mainly for checking email uniqueness
		:param email: The email to check
		:return: True or False
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('email')
		args = parser.parse_args()

		if is_any_field_empty(args):
			logging.error("Got error 9 checking for user existence")
			return jsonify(success=False, error=9)

		user_obj = User.query.filter_by(email=args["email"]).all()

		if user_obj is None:
			logging.error("Got error 8 checking for user existence")
			return jsonify(success=False, error=8)

		if len(user_obj) > 0:
			logging.info(f"Returned user existence status for email {user_obj.email}")
			return jsonify(success=True, exists=True)
		logging.info(f"Returned user existence status for email {user_obj.email}")
		return jsonify(success=True, exists=False)

