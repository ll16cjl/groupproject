import logging
from flask_restful import Resource, reqparse
from models.user import User
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty, is_staff

"""
Responsible for handling all requests relating to user objects
"""


class UserController(Resource):

	"""
	Handles GET request for user data. Will get the id of the user account and will return the JSON object
	for that user
	:param auth_token: A valid authentication token
	:return: The User object if found
	"""
	def get(self):
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("id")
		args = parser.parse_args()
		token = args["auth_token"]
		id = args["id"]  # Can be null, only used by desktop
		args.pop("id")

		if is_any_field_empty(args):
			logging.error("Got error 9 when getting user info")
			return jsonify(success=False, error=9)
		if not verify_auth_token(token):
			logging.error("Got error 10 when getting user info")
			return jsonify(success=False, error=10)

		if id is not None and id is not "":
			if is_staff(token):
				user = User.query.filter_by(id=id).first()
				if user is None:
					logging.error("Got error 8 when getting user info")
					return jsonify(success=False, error=8)
				logging.info(f"Got user info for staff user with ID {user.id}")
				return jsonify(success=True, id=user.id, first_name=user.first_name, email=user.email,
							   second_name=user.second_name)

		user = User.get_current_user_by_token(token)
		if user is not None:
			logging.info(f"Got user info for user with ID {user.id}")
			return jsonify(success=True, id=user.id, first_name=user.first_name, email=user.email,
						   second_name=user.second_name)
		logging.error("Got error 8 when getting user info")

		return jsonify(success=False, error=8)

	def put(self):
		"""
		Will change user details. If a param is null/None it will not be altered
		:return: Status
		"""
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("email")
		parser.add_argument("password")
		parser.add_argument("first_name")
		parser.add_argument("second_name")
		args = parser.parse_args()

		if args["auth_token"] is None:
			logging.error("Got error 9 when updating user info")
			return jsonify(success=False, error=9)

		current_user = User.get_current_user_by_token(args["auth_token"])
		if current_user is None:
			logging.error("Got error 8 when updating user info")
			return jsonify(success=False, error=8)

		current_user.first_name = current_user.first_name if args["first_name"] is None else args["first_name"]
		current_user.first_name = current_user.first_name if args["second_name"] is None else args["second_name"]
		current_user.commit()

		if args["password"] is not None:
			current_user.change_pass(args["password"])

		if args["email"] is not None:
			if not current_user.change_email(args["email"]):
				logging.error("Got error 20 when updating user info")
				return jsonify(success=False, error=20)  # TODO: Add error code to wiki
		logging.info(f"Altered details for user with ID {current_user.id}")
		return jsonify(success=True)


