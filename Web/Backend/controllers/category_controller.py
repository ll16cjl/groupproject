import logging
from flask_restful import Resource, reqparse
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty, is_staff
from models.category import Category


class CategoryController(Resource):

	def get(self):
		"""
		:param auth_token: A valid auth token
		:param name: Name of the category to return
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('name')
		parser.add_argument('auth_token')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Got error code 10 when getting billing info")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Got error code 9 when getting billing info")
			return jsonify(success=False, error=9)

		name = args["name"]

		if name == "*":
			logging.info("Returning all category data")
			categories = Category.query.filter_by().all()
			categories = [
				{
					"id": x.id,
					"name": x.category
				} for x in categories
			]
			return jsonify(success=True, categories=categories)
		else:
			logging.info(f"Returning all category data for name {name}")
			categories = Category.query.filter_by(category=name).all()
			categories = [
				{
					"id": x.id,
					"name": x.category
				} for x in categories
			]
			return jsonify(success=True, categories=categories)


	def post(self):
		"""
		Must be a staff member to do this
		:param auth_token: The auth token
		:param name: Name of the category
		:return:
		"""
		parser = reqparse.RequestParser()
		parser.add_argument('name')
		parser.add_argument('auth_token')
		args = parser.parse_args()

		if not verify_auth_token(args["auth_token"]):
			logging.error(f"Got error code 10 when getting billing info")
			return jsonify(success=False, error=10)

		if is_any_field_empty(args):
			logging.error(f"Got error code 9 when getting billing info")
			return jsonify(success=False, error=9)

		if not is_staff(args["auth_token"]):
			logging.error(f"Got error code 1000 when getting billing info")
			return jsonify(success=False, error=1000)

		category_obj = Category(args["name"])
		category_obj.commit()

		name = args["name"]

		logging.info(f"Created new category with name {name}")
		return jsonify(success=True)

