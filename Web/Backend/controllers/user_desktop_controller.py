import logging
from flask_restful import Resource, reqparse
from models.user import User
from flask import jsonify
from controllers import verify_auth_token, is_any_field_empty, is_staff


class UserDesktopController(Resource):

	"""
	Handles GET request for user data. Will get the id of the user account and will return the JSON object
	for that user
	:param auth_token: A valid authentication token
	:return: The User object if found
	"""
	def get(self):
		parser = reqparse.RequestParser()
		parser.add_argument("auth_token")
		parser.add_argument("id")
		args = parser.parse_args()
		token = args["auth_token"]

		if is_any_field_empty(args):
			logging.error("Got error 9 getting user info for desktop")
			return jsonify(success=False, error=9)
		if not verify_auth_token(token):
			logging.error("Got error 10 getting user info for desktop")
			return jsonify(success=False, error=10)
		if not is_staff(token):
			logging.error("Got error 1000 getting user info for desktop")
			return jsonify(success=False, error=1000)
		if args["id"] is "*":
			logging.info(f"Returned user data for all users")
			users = [{
						"email": x.email,
						"first_name": x.first_name,
						"second_name": x.second_name
					 } for x in User.query.all() if str(x.email) != "database@database.com"]
			return jsonify(success=True, users=users)
		else:
			user = User.query.filter_by(id=args["id"]).first()
			if user is not None:
				user_id = args["id"]
				logging.info(f"Returned user data for desktop with ID {user_id}")
				return jsonify(success=True, id=user.id, first_name=user.first_name, email=user.email,
							   second_name=user.second_name)
		logging.error("Got error 8 getting user info for desktop")
		return jsonify(success=False, error=8)
