from models.location import Location
from models.user import User
from models.bike import Bike
from models.payment import Payment
import pyqrcode
import os

dir_path = os.path.dirname(os.path.realpath(__file__))


def generate_receipt(rental):
	"""
	Will generate and return a JSON object representing a receipt for a rental.
	:param rental:
	:return:
	"""
	user = User.query.filter_by(id=rental.user_id).first()
	bikes = [Bike.query.filter_by(id=bike_id.id).first() for bike_id in rental.bike_ids]  # TODO: This is acting weird..

	bikes = [{
		"make": bike.make,
		"model": bike.model,
		"price": bike.price
	} for bike in bikes]

	location = Location.query.filter_by(id=rental.location_id).first()
	cost, discount = rental.get_cost()

	# generate the QR code
	generate_qr_code(rental)

	payment_method = Payment.query.filter_by(id=rental.payment_id).first()
	return (user.first_name + " " + user.second_name,
			bikes,
			location.city,
			location.name,
			cost,
			discount,
			rental.start_time.timestamp(),
			rental.end_time.timestamp(),
			Payment.query.filter_by(id=rental.payment_id).first().card_number)


def generate_qr_code(rental):
	url = pyqrcode.QRCode(rental.id, error='H')
	url.png(dir_path + f'/qr/{rental.id}.png', scale=10)
