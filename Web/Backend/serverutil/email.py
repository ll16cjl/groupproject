import smtplib
from _socket import gaierror
from email.mime.image import MIMEImage
from serverutil.receipt import generate_receipt
from models.user import User
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from models.location import Location
from threading import Thread
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

ridr_email = "temporaryemailforgroupproject@gmail.com"


def send_registration_email(user):
	"""
	Sends a registration email to the user thanking them for registering and gives them necesary information
	:param user: The user object
	:return:
	"""
	html = None
	message = f"Welcome to <i>ridr</i>! Your account is now fully registered and ready to use," \
		f" navigate to our webpage to look at the services we offer in your area!" \
		f"We believe ridr is the most modern and intuitive bicycle hiring service in the country, so if you think we " \
		f"can improve in any way let us know using our social media channels. Once again, welcome to the ridr club."

	with open(dir_path+"/email_template_welcome.html", "r") as file:
		html = file.read().replace("\n", "")

	html = html.replace("MESSAGE", message)
	html = html.replace("WELCOME", "Dear " + user.first_name + ", ")

	file = open(dir_path+"/Logo/logo-dark.png", "rb")
	logo = MIMEImage(file.read())
	file.close()
	logo.add_header("Content-ID", "<image1>")

	send_mail(user.email, "Welcome to ridr", [MIMEText(html, "html"), logo])


def send_receipt_email(rental):
	"""
	Sends an email to the user once they have made a rental. Will contain information on the rental and
	Will also have an attached PDF receipt
	:param rental: The rental object
	:param user: The user object
	:return:
	"""
	rental_recipt = generate_receipt(rental)
	user = User.query.filter_by(id=rental.user_id).first()

	html = None
	message = f"Thank you for your recent ridr booking. Please accept this email as acceptance of your order. " \
			  f"Please show the assitant the QR code included below to confirm your booking upon arrival. Please arrive " \
			f"15 minutes before the scheduled booking start time to ensure your bikes are ready for use promptly. "


	with open(dir_path+"/email_template_receipt.html", "r") as file:
		html = file.read().replace("\n", "")

	# Add the greetings and messages
	html = html.replace("MESSAGE", message)
	html = html.replace("GREETING", "Dear " + user.first_name + ", ")

	# Add the rental info
	html = html.replace("LOC", Location.query.filter_by(id=rental.location_id).first().city + " - " +
										Location.query.filter_by(id=rental.location_id).first().name)

	html = html.replace("START", str(rental.start_time.strftime("%d/%m/%Y at %H:%M")))
	html = html.replace("END", str(rental.end_time.strftime("%d/%m/%Y at %H:%M")))

	html = html.replace("COST", str(rental_recipt[4]))
	html = html.replace("DISC", str(rental_recipt[5]))

	# Add the logo
	file = open(dir_path+"/Logo/logo-dark.png", "rb")
	logo = MIMEImage(file.read())
	file.close()
	logo.add_header("Content-ID", "<image1>")

	# Add the QR code
	file = open(dir_path+f"/qr/{rental.id}.png", "rb")
	qr = MIMEImage(file.read())
	file.close()
	qr.add_header("Content-ID", "<qr>")

	# Remove the generated QR code as it is no longer needed
	os.remove(dir_path+f"/qr/{rental.id}.png")

	send_mail(user.email, "Your ridr booking", [MIMEText(html, "html"), logo, qr])


def send_mail(recipent, subject, content):
	thread = Thread(target=send_mail_thread, args=(recipent, subject, content))
	thread.start()
	thread.join()


def send_mail_thread(recipient, subject, content):
	try:
		msg = MIMEMultipart("alternative")
		msg["Subject"] = subject
		msg["From"] = ridr_email
		msg["To"] = recipient

		for item in content:
			msg.attach(item)

		# Send the message via local SMTP server.
		smtp_obj = smtplib.SMTP("smtp.gmail.com:587")
		smtp_obj.starttls()
		smtp_obj.login(ridr_email, "UniOfLeeds123")

		smtp_obj.sendmail(ridr_email, recipient, msg.as_string())
		smtp_obj.quit()
	except gaierror:
		# TODO: Add to Q to be sent later
		print("Error sending email!")