from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS, cross_origin
from sqlalchemy import event

app = Flask(__name__)
app.config.from_object('config')
api = Api(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
auth = HTTPBasicAuth()
cors = CORS(app)

# import all of the controllers that serve as endpoints for the REST API
from controllers.user_controller import UserController
from controllers.register_controller import RegisterController
from controllers.session_controller import SessionController
from controllers.bike_controller import BikeController
from controllers.location_controller import LocationController
from controllers.rental_controller import RentalController
from controllers.receipt_controller import ReceiptController
from controllers.billing_controller import BillingController
from controllers.payment_controller import PaymentController
from controllers.bike_finder_controller import BikeFinderController
from controllers.user_exists_controller import UserExistsController
from controllers.category_controller import CategoryController
from controllers.user_desktop_controller import UserDesktopController

api.add_resource(SessionController, "/session")
api.add_resource(RegisterController, "/register")
api.add_resource(UserController, '/user/info')
api.add_resource(BikeController, '/bike')
api.add_resource(RentalController, '/rental')
api.add_resource(LocationController, '/location')
api.add_resource(ReceiptController, '/receipt')
api.add_resource(BillingController, '/billing')
api.add_resource(PaymentController, '/payment')
api.add_resource(BikeFinderController, '/bike/avail')
api.add_resource(UserExistsController, '/user/exists')
api.add_resource(CategoryController, '/category')
api.add_resource(UserDesktopController, '/user/info/desktop')

# Need to list all of the models here so migrate can see them
from models.user import User
from models.location import Location
from models.bike import Bike
from models.rental import Rental, association_table
from models.billing import Billing
from models.payment import Payment
from models.category import Category
import datetime
demo_mode = True

# Add clean DB for demo mode.
if demo_mode:
	for entry in db.session.query(association_table).all():
		db.session.delete(entry)
		db.session.commit()

	for entry in db.session.query(Rental).all():
		db.session.delete(entry)
		db.session.commit()

	locations = []
	for entry in db.session.query(Location).all():
		db.session.delete(entry)
		db.session.commit()

	locations.append(Location("Oxford Street", "London", 51.515419, -0.141099))
	locations.append(Location("Trinity Center", "Leeds", 53.7965, -1.5438))
	locations.append(Location("Carnegie Stadium", "Leeds", 53.8165, -1.5821))
	locations.append(Location("Victoria Train Station", "Manchester", 53.4871, -2.2424))

	for location in locations:
		location.commit()

	users = []
	for entry in db.session.query(User).all():
		db.session.delete(entry)
		db.session.commit()

	default_user = User("database@database.com", "DATABASE", "DATABASE", 1, "DATABASE123")
	default_user.commit()

	users.append(User("user1@demo.com", "Jon", "Snow", 0, "MadmanOrAKing"))
	users.append(User("user2@demo.com", "Nikita", "Mel", 0, "TestPassword"))
	users.append(User("user3@demo.com", "Sansa", "Stark", 0, "LittleBird"))
	users.append(User("user4@demo.com", "Arya", "Stark", 0, "NOWAYMAN"))
	users.append(User("user5@demo.com", "Tormund", "GiantsBane", 0, "ILikeBears"))
	users.append(User("user6@demo.com", "Aegon", "Targaryen", 0, "IAlreadyHaveAnAccount"))
	users.append(User("user7@demo.com", "Demo", "User", 0, "UserPassword"))

	users.append(User("staff1@demo.com", "Staff", "Staff", 1, "staff123"))
	users.append(User("staff2@demo.com", "Staff", "Staff", 1, "staff123"))

	for user in users:
		user.commit()

	categories = []
	for entry in db.session.query(Category).all():
		db.session.delete(entry)
		db.session.commit()
	default_category = Category("Default")
	default_category.commit()
	categories.append(Category("Mountain Bike"))
	categories.append(Category("Road Bike"))
	categories.append(Category("BMX"))

	for category in categories:
		category.commit()

	for entry in db.session.query(Billing).all():
		db.session.delete(entry)
		db.session.commit()
	default_billing = Billing(User.query.filter_by(email="database@database.com").first().id, "TILL", "TILL")
	default_billing.commit()

	for entry in db.session.query(Payment).all():
		db.session.delete(entry)
	till_cash = Payment(User.query.filter_by(email="database@database.com").first().id, "CASH", "TILL", "",
						datetime.datetime.now(), "", Billing.query.filter_by(first_line="TILL").first().id, system=True)
	till_cash.commit()

	till_card = Payment(User.query.filter_by(email="database@database.com").first().id, "CARD", "TILL", "",
						datetime.datetime.now(), "", Billing.query.filter_by(first_line="TILL").first().id, system=True)
	till_card.commit()

	for entry in db.session.query(Bike).all():
		db.session.delete(entry)
		db.session.commit()

	# Add location 1 bikes
	for i in range(10):
		bike = Bike("BMX12", 1, "Amaco", 80, 4)
		bike.commit()
	for i in range(5):
		bike = Bike("Roadmaster", 1, "SpeedBikes", 40, 3)
		bike.commit()
	for i in range(5):
		bike = Bike("Slimline", 1, "Mountainx", 60, 2)
		bike.commit()

	# Add location 1 bikes
	for i in range(10):
		bike = Bike("BMX12", 2, "Amaco", 80, 4)
		bike.commit()
	for i in range(5):
		bike = Bike("Roadmaster", 2, "SpeedBikes", 40, 3)
		bike.commit()
	for i in range(5):
		bike = Bike("Slimline", 2, "Mountainx", 60, 2)
		bike.commit()

	# Add location 1 bikes
	for i in range(10):
		bike = Bike("BMX12", 3, "Amaco", 80, 4)
		bike.commit()
	for i in range(5):
		bike = Bike("Roadmaster", 3, "SpeedBikes", 40, 3)
		bike.commit()
	for i in range(5):
		bike = Bike("Slimline",3, "Mountainx", 60, 2)
		bike.commit()

	# Add location 1 bikes
	for i in range(10):
		bike = Bike("BMX12", 4, "Amaco", 80, 4)
		bike.commit()
	for i in range(5):
		bike = Bike("Roadmaster", 4, "SpeedBikes", 40, 3)
		bike.commit()
	for i in range(5):
		bike = Bike("Slimline", 4, "Mountainx", 60, 2)
		bike.commit()

@event.listens_for(User.__table__, 'after_create')
def insert_initial_user_values(*args, **kwargs):
	default_user = User("database@database.com", "DATABASE", "DATABASE", 1, "DATABASE123")
	default_user.commit()


@event.listens_for(Billing.__table__, 'after_create')
def insert_initial_billing_values(*args, **kwargs):
	default_billing = Billing(User.query.filter_by(email="database@database.com").first().id, "TILL", "TILL")
	default_billing.commit()


@event.listens_for(Payment.__table__, 'after_create')
def insert_initial_payment_values(*args, **kwargs):
	till_cash = Payment(User.query.filter_by(email="database@database.com").first().id, "CASH", "TILL", "",
						datetime.datetime.now(), "", Billing.query.filter_by(first_line="TILL").first().id, system=True)
	till_cash.commit()

	till_card = Payment(User.query.filter_by(email="database@database.com").first().id, "CARD", "TILL", "",
						datetime.datetime.now(), "", Billing.query.filter_by(first_line="TILL").first().id, system=True)
	till_card.commit()


@event.listens_for(Category.__table__, 'after_create')
def insert_initial_category_values(*args, **kwargs):

	default_category = Category("Default")
	default_category.commit()


@event.listens_for(Location.__table__, 'after_create')
def insert_initial_location_values(*args, **kwargs):
	pass

