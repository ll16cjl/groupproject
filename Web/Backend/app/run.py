from app import app
import logging

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, filename="ControllerLog", filemode="a+",
                        format="_____%(asctime)-15s %(levelname)-8s %(message)s_____")
    app.run(debug=True, use_reloader=True)


