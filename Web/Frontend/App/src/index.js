import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import 'semantic-ui-css/semantic.min.css';

import App from './App';
import initStore from './store';

render(
	<Router>
		<Provider store={initStore()}>
			<App />
		</Provider>
	</Router>, document.getElementById('app')
);

// Allow updating module during runtime
if (module.hot) {
	module.hot.accept();
}

serviceWorker.unregister();
