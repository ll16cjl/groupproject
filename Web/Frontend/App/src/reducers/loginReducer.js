export default (state = {}, action) => {
	switch (action.type) {
		case 'SAVE_RESPONSE':
			return {
				...state,
				response: action.response,
				responseSource: action.source
			};

		case 'SAVE_TOKEN':
			return {
				...state,
				token: action.token
			};

		case 'CLEAR':
			return {};

		case 'CLEAR_RESPONSE':
			return {
				...state,
				response: {},
				responseSource: undefined
			};

		case 'GET':
		default:
			return {
				...state
			}
	}
}