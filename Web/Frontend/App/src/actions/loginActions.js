export const clearToken = () => dispatch => {
	dispatch({
		type: 'CLEAR'
	});
};

export const clearResponse = () => dispatch => {
	dispatch({
		type: 'CLEAR_RESPONSE'
	});
};

export const getToken = () => dispatch => {
	dispatch({
		type: 'GET'
	});
};

export const saveToken = (token) => dispatch => {
	dispatch({
		type: 'SAVE_TOKEN',
		token: token
	});
};

export const saveResponse = (response, source) => dispatch => {
	dispatch({
		type: 'SAVE_RESPONSE',
		response: response,
		source: source
	})
};