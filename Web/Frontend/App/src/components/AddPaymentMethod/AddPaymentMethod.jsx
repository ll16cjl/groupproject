import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import moment from 'moment';

import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	Message,
	Segment,
	Select
} from "semantic-ui-react";

import { saveResponse } from '~/actions/loginActions';

import './AddPaymentMethod.scss';

class AddPaymentMethod extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			data: {
				addresses: []
			},
			form: {
				billing_address_id: '',
				card_number: '',
				card_type: '',
				expiry_date: '',
				name_on_card: '',
			},
			loading: true
		}
	}

	componentDidMount() {
		axios.get(`http://localhost:5000/billing?id=*&auth_token=${localStorage.getItem('auth_token')}`)
			.then(res => {
				this.setState({
					data: {
						addresses: res.data.addresses
					},
					loading: false
				});
			});
	}

	handleFormChange = (e, {field, value} ) => {
		this.setState(prevState => ({
			form: {
				...prevState.form,
				[field]: value
			}
		}));
	};

	handleFormSubmit = () => {
		let { card_number, card_type, name_on_card, billing_address_id, expiry_date } = this.state.form;
		axios.post(`http://localhost:5000/payment?auth_token=${localStorage.getItem('auth_token')}&billing_address_id=${billing_address_id}&card_number=${card_number}&card_type=${card_type}&name_on_card=${name_on_card}&expiry_date=${moment(expiry_date, 'MM-YY').unix()}`)
			.then(res => {
				if(res.data.success) {
					this.setState({
						form: {
							billing_address_id: '',
							card_number: '',
							card_type: '',
							expiry_date: '',
							name_on_card: '',
						},
						loading: false,
						success: true
					});
					this.props.saveResponse(res, 'payment method');
				}
				else {
					this.setState({
						error: true,
						loading: false
					});
				}
			});
	};

	isFormValid(form) {
		return form.billing_address_id && form.card_number.length === 16 && form.card_type && form.expiry_date && form.name_on_card;
	}

	render() {
		let { data, form, loading } = this.state;

		const cardTypes = [
			{text: 'Visa', value: 'Visa'},
			{text: 'Visa Debit', value: 'Visa Debit'},
			{text: 'Mastercard', value: 'Mastercard'},
			{text: 'American Express', value: 'American Express'},
		];

		let billingAddresses = [];
		data.addresses.forEach(a => {
			billingAddresses.push({text: `${a.first_line}, ${a.post_code}`, value: a.id});
		});

		if(!localStorage.getItem('auth_token')) return <Redirect to='/login'/>;
		else if(this.state.success) return <Redirect to={'/account'}/>;
		else return (
			<div id={'add-payment-method'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Add Payment Method
						</Header>
						<Form error={!loading && data.addresses.length === 0}>
							<Message error>
								<Header> No billing addresses found </Header>
								To add a payment method you must have at least one billing address on your account.
								Please <Link to={'/account/add-billing-address'}> add a billing address </Link>.
							</Message>
							<Segment stacked>
								<Form.Input
									disabled={data.addresses.length === 0}
									focus
									icon={'user'}
									iconPosition={'left'}
									field={'name_on_card'}
									name={'name_on_card'}
									onChange={this.handleFormChange}
									placeholder={'Cardholder Name'}
									value={form.name_on_card}
									width={16}
								/>
								<Form.Input
									disabled={data.addresses.length === 0}
									focus
									icon={'credit card'}
									iconPosition={'left'}
									field={'card_number'}
									maxLength={16}
									name={'card_number'}
									onChange={this.handleFormChange}
									placeholder={'Card Number'}
									value={form.card_number}
								/>
								<Form.Group widths={'equal'}>
									<Form.Input
										control={Select}
										disabled={data.addresses.length === 0}
										field={'card_type'}
										name={'card_type'}
										placeholder={'Select a card type'}
										onChange={this.handleFormChange}
										options={cardTypes}
										value={form.card_type}
									/>
									<Form.Input
										disabled={data.addresses.length === 0}
										focus
										icon={'calendar'}
										iconPosition={'left'}
										field={'expiry_date'}
										name={'expiry_date'}
										onChange={this.handleFormChange}
										placeholder={'Expiry Date (MM/YY)'}
										value={form.expiry_date}
									/>
								</Form.Group>
								<Form.Input
									control={Select}
									disabled={data.addresses.length === 0}
									field={'billing_address_id'}
									name={'billing_address_id'}
									placeholder={'Select a billing address'}
									onChange={this.handleFormChange}
									options={billingAddresses}
									value={form.billing_address_id}
								/>
								{this.state.loading
									? <Icon name={'circle notched'} loading />
									: <Button color={'blue'} disabled={!this.isFormValid(form)} onClick={this.handleFormSubmit}> Submit </Button>
								}
								<Button as={Link} to={'/account'}> Cancel </Button>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	saveResponse: (response, source) => dispatch(saveResponse(response, source))
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPaymentMethod);