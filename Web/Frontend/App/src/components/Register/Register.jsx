import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	Message,
	Segment
} from "semantic-ui-react";

import { saveResponse } from '~/actions/loginActions';

import './Register.scss';

class Register extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			form: {
				email: '',
				first: '',
				second: '',
				password: ''
			},
			get: undefined,
			loading: false
		}
	}

	handleFormChange = (e, {field, value} ) => {
		this.setState(prevState => ({
			form: {
				...prevState.form,
				[field]: value
			}
		}));
	};

	handleSubmitForm = () => {
		let { email, first, second, password } = this.state.form;

		this.setState({
			loading: true
		});

		axios.post(`http://localhost:5000/register?email=${email}&password=${password}&first=${first}&second=${second}`)
			.then((res) => {
				this.setState({
					get: res,
					loading: false
				}, () => {
					if(!res.data.error) this.props.saveResponse(res);
				});
			})
		.catch(error => {
			this.setState({
				loading: false
			});
		});
	};

	render() {
		let { get } = this.state;
		let { email, first, second, password } = this.state.form;

		if(localStorage.getItem('auth_token')) return <Redirect to='/account'/>;
		else if(get && get.data.success) {
			return <Redirect to={'/login'}/>;
		}
		else return(
			<div id={'register'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Register
						</Header>
						<Form error={(get && !get.data.success)}>
							<Segment stacked>
								<Form.Input
									error={(get && !get.data.success)}
									focus
									icon={'mail'}
									iconPosition={'left'}
									field={'email'}
									name={'email'}
									onChange={this.handleFormChange}
									placeholder={'Email'}
									value={email}
								/>
								<Form.Group widths={'equal'}>
									<Form.Input
										error={(get && !get.data.success)}
										focus
										icon={'user'}
										iconPosition={'left'}
										field={'first'}
										name={'first'}
										onChange={this.handleFormChange}
										placeholder={'First Name'}
										value={first}
									/>
									<Form.Input
										error={(get && !get.data.success)}
										focus
										icon={'user'}
										iconPosition={'left'}
										field={'second'}
										name={'second'}
										onChange={this.handleFormChange}
										placeholder={'Last Name'}
										value={second}
									/>
								</Form.Group>
								<Form.Input
									error={(get && !get.data.success)}
									focus
									icon={'lock'}
									iconPosition={'left'}
									field={'password'}
									name={'password'}
									onChange={this.handleFormChange}
									placeholder={'Password'}
									type={'password'}
									value={password}
								/>
								{this.state.loading
									? <Icon name={'circle notched'} loading />
									: <Button disabled={!(email && first && second && password)} onClick={this.handleSubmitForm}>Register</Button>
								}
								<Message
									error
									header={'Registration Failed'}
									content={`Error code ${get && get.data.error}: ${get && get.data.error === 1 ? 'email already in use.'
										: get && get.data.error === 0 ? 'missing form fields.' : 'unknown error.'}`}
								/>
							</Segment>
						</Form>
						<Message>
							Already have an account? <Link to={'/login'}>Log in here.</Link>
						</Message>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	saveResponse: (response) => dispatch(saveResponse(response))
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);