import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	Message,
	Segment
} from "semantic-ui-react";

import { clearResponse } from '~/actions/loginActions';

import './Account.scss';

class Account extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			data: {
				email: '',
				first_name: '',
				second_name: '',
				payment_methods: ''
			},
			loading: true
		}
	}

	componentDidMount() {
		let authToken = localStorage.getItem('auth_token');
		axios.get(`http://localhost:5000/user/info?auth_token=${authToken}`)
			.then(res => {
				this.setState({
					data: res.data,
					loading: false
				});
			});
		axios.get(`http://localhost:5000/payment?id=*&auth_token=${authToken}`)
			.then(res => {
				this.setState(prevState => ({
					data: {
						...prevState.data,
						payment_methods: res.data.payment_methods
					}
				}));
			});
	}

	componentWillUnmount() {
		this.props.clearResponse();
	}

	handleLogOut = () => {
		let authToken = localStorage.getItem('auth_token');
		axios.delete(`http://localhost:5000/session?auth_token=${authToken}`)
			.then(res => {
				localStorage.removeItem('auth_token');
				this.forceUpdate();
			});
	};

	renderAccountDetails = () => {
		let { data } = this.state;

		return [
			<Form.Input key={'email'} label={'Email'} name={'email'} readOnly value={data.email}/>,
			<Form.Group key={'names'} widths={'equal'}>
				<Form.Input key={'first-name'} label={'First Name'} name={'first-name'} readOnly value={data.first_name}/>
				<Form.Input key={'second-name'} label={'Second Name'}  name={'second-name'} readOnly value={data.second_name}/>
			</Form.Group>
		]
	};
	
	renderSuccess = () => {
		let { response, responseSource } = this.props.loginReducer;
		if(response && responseSource) {
			return <Message content={`Your ${responseSource} was successfully added.`} header={'Success!'} success/>;
		}
	};

	render() {
		let { data, loading } = this.state;

		if(!localStorage.getItem('auth_token')) return <Redirect to='/login'/>;
		else return(
			<div id={'account'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Account Details
						</Header>
						{this.renderSuccess()}
						<Form>
							<Segment loading={!data.email} stacked>
								{this.renderAccountDetails()}
								<Form.Field>
									<Button as={Link} color={'blue'} disabled={loading} to={'account/order-history'}> View Order History </Button>
								</Form.Field><Form.Field>
									<Button as={Link} color={'blue'} disabled={loading} to={'account/add-payment-method'}> Add Payment Method </Button>
								</Form.Field>
								<Form.Field>
									<Button as={Link} color={'blue'} disabled={loading} to={'account/add-billing-address'}> Add Billing Address </Button>
								</Form.Field>
								{loading
									? <Icon name={'circle notched'} loading />
									: <Button onClick={this.handleLogOut}>Log out</Button>
								}
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	clearResponse: () => dispatch(clearResponse())
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);