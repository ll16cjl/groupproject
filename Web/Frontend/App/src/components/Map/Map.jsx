import React from 'react';
import { connect } from 'react-redux';
import GoogleMapReact from 'google-maps-react'
import {Map, InfoWindow, Marker} from 'google-maps-react';

import { clearToken } from '~/actions/loginActions';

import './Map.scss';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class SimpleMap extends React.Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 20
  };
	render() {
    return (
      
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyC1gDSrgNkCgYXR0_uQ_ST6eRdPcoLBAuQ' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="Bike Location"
          />
        </GoogleMapReact>
      </div>
    );
  }
}
export default SimpleMap;


