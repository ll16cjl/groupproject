import React from 'react';

import {
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
import './Map.scss';
const MapWithAMarker = withGoogleMap(props =>(
  <GoogleMap defaultZoom={5} defaultCenter={{lat: props.locationCoords[0], lng: props.locationCoords[1]}}>
		{props.locations &&
		<>
			{props.locations.map(m => {
				return(
					<Marker
						key={`marker-${m.id}`}
						title={m.city}
						position={{ lat: m.latitude, lng: m.longitude}}
						name={m.name}
					/>
				)
			})}
		</>
		}
  </GoogleMap>
));

class SimpleMap extends React.Component {  
	render() {
		return (
      <div id="map">
        <MapWithAMarker
          containerElement={<div style={{ display: 'inline-block', height: '320px', width: '360px', margin: 'auto'  }} /> }
          mapElement={<div style={{ height: '100%'}} /> }
					locations={this.props.locations}
					locationCoords={this.props.locationCoords || [53.8008, -1.5491]}
      />
      </div>
    );
  }
}
export default SimpleMap;


