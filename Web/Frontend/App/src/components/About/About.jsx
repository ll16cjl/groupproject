import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import {
	Form,
	Grid,
	Header,
	Segment
} from "semantic-ui-react";

import './About.scss';

class About extends React.Component {
	render() {
		return(
			<div id={'about'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 880}}>
						<Header>
							Frequently Asked Questions
						</Header>
						<Form>
							<Segment>
								<h4 className="title">What is RIDR?</h4>
								<p>RIDR is a Leeds based company providing customers with an easy way of getting around the city empowering users with modern bikes that take you to your destination in no time.</p>
							</Segment>
							<Segment>
							<h4 className="title">How can I access the bikes?</h4>
								<p>By using our services, you will able to Sign Up and be able to make a booking within two minutes. To get started, click on the <Link to={"register"}>Registration</Link> button in the top right corner. Once you have registered, click on the <Link to={"booking"}>Booking</Link> page where you can put in the details of when you want to book book the bike from, the duration, location, etc</p>
							</Segment>
							<Segment>
								<h4 className="title">How much does the service cost?</h4>
								<p>It's very affordable, so you will be able to go on journeys even on the days before your payday. :-) </p>
							</Segment>
							<Segment>
							<h4 className="title">How can I collect a bike?</h4>
								<p>Once you have completed a booking online, just simply walk in to the designated store, that you have chosen and pick it up without a hassle. Also, you will be able to skip the queue if you have ordered online. Should you need any queries, don't hesitate to talk to one of our staff members.  </p>
							</Segment>
							<Segment>
								<h4 className="title">What happens if I collect my bike late?</h4>
								<p>Normally we are very rational, and understand that issues may arise, making it difficult for you to sometime return the bike in time. In case of returning it late in a reasonable time, we will let you off, and you will not have to pay an excess fee. After this however, you will have to pay an excess fee of  <b className={'Red'}> £5 a day </b>for the first two days after which it grows to <b className={'Red'}>£15 a day</b></p>
							</Segment>
							<Segment>
							<h4 className="title">How is my data being handled?</h4>
								<p>Your data security is very important to us and with us we can guarantee that we will never pass your information on to 3rd parties. All of your sensitive information is hashed, so not even the moderators can view it. </p>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(About);