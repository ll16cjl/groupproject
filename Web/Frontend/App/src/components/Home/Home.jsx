import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import {
	Container,
	Grid,
	Header,
	Icon,
	Segment,
	Button,
	Divider
} from "semantic-ui-react";

import './Home.scss';
//import Map, { MapContainer } from '../Map/Map';
// import SimpleMap from '../Map/Map';
import SimpleMap from '../Map/Map2';

class Home extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			data: {
				email: '',
				first_name: '',
				second_name: ''
			},
			loading: false
		}
	}

	render() {
		let { loading } = this.state;

		return(
			<div id={'home'} style={{marginTop: 40 + 'px'}}>

				<Segment textAlign={'center'} paddingBottom={'5 em'} loading={loading}  vertical style={{backgroundColor: '#1b1c1d', minHeight: 600}}>
					<Container text>
						<Header as={'h1'}
								content={'Ready to RIDR'}
								style={{
									fontSize:'4em',
									fontWeight: 'normal',
									color: 'white',
									marginTop: '3em',
									marginBottom: 0
						}}/>
						<Header
								as={'h2'}
								content='to Work in Style?'
								style={{
									fontSize: '1.7em',
									fontWeight: 'normal',
									marginTop:  '1 em',
									color: 'white',
									marginBottom: '5 em'
								}}
						/>
						<Button primary size='huge' as={Link} to={'Register'}> 
							Sign Up Now
							<Icon name='right arrow' />
						</Button>
					</Container>
					
				</Segment>

				<Segment style={{ padding: '8em 0em' }} vertical>
					<Container text>
						<Header as='h3' style={{ fontSize: '2em' }}>
						What is our mission?
						</Header>
							<p style={{ fontSize: '1.33em' }}>
								Instead of focusing on minimalistic details, we aim to improve your day-to-day life by giving you 
								the most comfortable, inexpensive and healthiest way of commuting to work. Don't let this fool you, 
								many of our clients use our services for leisure too not just for commuting.
							</p>
							<Button as={Link} to={'Register'} size='large' >
							Read More
						</Button>
						<Divider
							as='h4'
							className='header'
							horizontal
							style={{ margin: '3em 0em', textTransform: 'uppercase' }}
						>
						<a href='#'>A final note</a>
						</Divider>
						<Header as='h3' style={{ fontSize: '2em' }}>
							What are we trying to achieve? 
							</Header>
							<p style={{ fontSize: '1.33em' }}>
							We truly believe in saving the environment as much as possible, and reducing our impact on the planet on a 
							global scale. By simply not using your car, or getting on public transport, you are making a difference and we wish 
							to raise awareness so that the whole of UK can be as full of bikes as the Netherlands.
							</p>
							<Button as={Link} to={'Register'} size='large' >
							I'm Sold
							</Button>
					</Container>
				</Segment>
				
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({

});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);