import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	Message,
	Segment
} from "semantic-ui-react";

import { clearResponse } from '~/actions/loginActions';

import './Login.scss';

class Login extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			form: {
				email: '',
				password: ''
			},
			get: undefined,
			loading: false
		}
	}

	handleFormChange = (e, {field, value} ) => {
		this.setState(prevState => ({
			form: {
				...prevState.form,
				[field]: value
			}
		}));
	};

	handleFormSubmit = () => {
		let { email, password } = this.state.form;

		this.setState({
			loading: true
		});

		axios.get(`http://localhost:5000/session?email=${email}&password=${password}`)
			.then((res) => {
				this.setState({
					get: res,
					loading: false
				}, () => {
					this.props.clearResponse();
					if(this.state.get.data.success) {
						localStorage.setItem('auth_token', this.state.get.data.token);
					}
					}
				);
			})
			.catch(error => {
				this.props.clearResponse();
				this.setState({
					loading: false
				});
			});
	};

	renderRegistrationSuccess = () => {
		let { response } = this.props.loginReducer;
		if(response && response.data) return (
			<Message content={'Please fill in your account details to proceed.'} header={'Registration Successful'} success/>
		);
	};

	render() {
		let { get } = this.state;
		let { email, password } = this.state.form;

		if(localStorage.getItem('auth_token')) return <Redirect to='/'/>;
		else return(
			<div id={'login'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Log In
						</Header>
						{this.renderRegistrationSuccess()}
						<Form error={(get && !get.data.success)}>
							<Segment stacked>
								<Form.Input
									error={(get && !get.data.success)}
									icon={'mail'}
									iconPosition={'left'}
									field={'email'}
									name={'email'}
									onChange={this.handleFormChange}
									placeholder={'Email'}
									value={email}
								/>
								<Form.Input
									error={(get && !get.data.success)}
									icon={'lock'}
									iconPosition={'left'}
									field={'password'}
									name={'password'}
									onChange={this.handleFormChange}
									placeholder={'Password'}
									type={'password'}
									value={password}
								/>
								{this.state.loading
									? <Icon name={'circle notched'} loading />
									: <Button disabled={!(email && password)} onClick={this.handleFormSubmit}>Log in</Button>
								}
								<Message
									error
									header={'Login Failed'}
									content={`Error code ${get && get.data.error}: ${get && get.data.error === 10 ? 'invalid email or password.' : 'unknown error.'}`}
								/>
							</Segment>
						</Form>
						<Message>
							Don't have an account yet? <Link to={'/register'}>Register here!</Link>
						</Message>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	clearResponse: () => dispatch(clearResponse())
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);