import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	Message,
	Segment
} from "semantic-ui-react";

import { saveResponse } from '~/actions/loginActions';

import './AddBillingAddress.scss';

class AddBillingAddress extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			error: false,
			form: {
				first_line: '',
				postcode: ''
			},
			loading: false,
			success: false
		}
	}

	handleFormChange = (e, {field, value} ) => {
		this.setState(prevState => ({
			error: false,
			form: {
				...prevState.form,
				[field]: value
			}
		}));
	};

	handleFormSubmit = () => {
		let { first_line, postcode } = this.state.form;

		this.setState({
			error: false,
			loading: true,
		});

		axios.post(`http://localhost:5000/billing?auth_token=${localStorage.getItem('auth_token')}&first_line=${first_line}&post_code=${postcode}`)
			.then(res => {
				if(res.data.success) {
					this.setState({
						form: {
							first_line: '',
							postcode: ''
						},
						loading: false,
						success: true
					});
					this.props.saveResponse(res, 'billing address');
				}
				else {
					this.setState({
						error: true,
						loading: false
					});
				}
			});
	};

	render() {
		let { form } = this.state;

		if(!localStorage.getItem('auth_token')) return <Redirect to='/login'/>;
		else if(this.state.success) return <Redirect to={'/account'}/>;
		else return (
			<div id={'add-billing-address'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Add Billing Address
						</Header>
						<Form error={this.state.error}>
							<Message
								error
								header={'Submission Error'}
								content={'Please make sure you have filled in both fields.'}
							/>
							<Segment stacked>
								<Form.Input
									focus
									icon={'location arrow'}
									iconPosition={'left'}
									field={'first_line'}
									name={'first_line'}
									onChange={this.handleFormChange}
									placeholder={'Address Line 1'}
									value={form.first_line}
								/>
								<Form.Input
									focus
									icon={'map marker'}
									iconPosition={'left'}
									field={'postcode'}
									name={'postcode'}
									onChange={this.handleFormChange}
									placeholder={'Postcode'}
									value={form.postcode}
								/>
								{this.state.loading
									? <Icon name={'circle notched'} loading />
									: <Button color={'blue'} disabled={!(form.first_line && form.postcode)} onClick={this.handleFormSubmit}> Submit </Button>
								}
								<Button as={Link} to={'/account'}> Cancel </Button>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	saveResponse: (response, source) => dispatch(saveResponse(response, source))
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBillingAddress);