import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import moment from 'moment';

import {
	Button,
	Form,
	Grid,
	Header,
	Message,
	Segment
} from "semantic-ui-react";

import './OrderHistory.scss';

class OrderHistory extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			data: {
				orders: []
			},
			currentOrder: 0,
			error: undefined,
			loading: true,
			receipt: undefined
		}
	}

	componentDidMount() {
		axios.get(`http://localhost:5000/rental?auth_token=${localStorage.getItem('auth_token')}`)
			.then(res => {
				if(!res.data.success) {
					this.setState({
						error: res.data.error,
						loading: false
					});
				}
				else {
					this.setState({
						data: {
							orders: res.data.reservations
						},
						currentOrder: res.data.reservations.length - 1,
					})
				}
			});
	}

	handleGetReceipt(id) {
		return axios.get(`http://localhost:5000/receipt?id=${id}&auth_token=${localStorage.getItem('auth_token')}`)
			.then(res => {
				this.setState({
					loading: false,
					receipt: res.data
				});
			})
	};

	renderCurrentOrder() {
		let { currentOrder, receipt } = this.state;
		let { orders } = this.state.data;
		if(orders.length > 0) {
			if(receipt === undefined) this.handleGetReceipt(currentOrder + 1);
			return [
				<Message key={`order-${currentOrder}`}>
					Order #{currentOrder + 1}:<br/><br/>
					{this.renderReceiptInformation()}
				</Message>
			]
		}
	}

	renderReceiptInformation() {
		let { currentOrder, receipt } = this.state;
		let { orders } = this.state.data;
		if(receipt) return (
				<div id={'receipt'}>
					Customer Name: {receipt.name} <br/>
					Store: {receipt.location_city} - {receipt.location_name}<br/><br/>

					Bike{receipt.bikes.length > 1 ? 's:' : ': '}
					{receipt.bikes.length === 1 &&
						receipt.bikes[0].make + ' ' + receipt.bikes[0].model
					}
					{receipt.bikes.length > 1 &&
						<>
							{receipt.bikes.map(b => (
								<div>
									- {b.make} {b.model}
								</div>
							))}
						</>
					}
					<br/>Start: {moment.unix(receipt.start).format('DD/MM/YYYY [at] HH:mm')} <br/>
					End: {moment.unix(receipt.end).format('DD/MM/YYYY [at] HH:mm')}<br/><br/>

					Payment Method: ************{receipt.digits} <br/>
					Payment Date: {moment.unix(orders[currentOrder].reservation_time).format('DD/MM/YYYY [at] HH:mm')}<br/>
					Price: £{receipt.discount ? receipt.discount.toFixed(2) + ` (Money saved: £${(receipt.cost - receipt.discount).toFixed(2)})` : receipt.cost.toFixed(2)} <br/><br/>
				</div>
			);
		// If the receipt data hasn't loaded, pad the size of the segment to avoid resizing when data is loaded
		else return (
			<div>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			</div>
		)
	}

	render() {
		let { error } = this.state;
		if(!localStorage.getItem('auth_token')) return <Redirect to='/login'/>;
		else return(
			<div id={'order-history'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Order History
						</Header>
						<Form>
							<Segment stacked loading={this.state.loading}>
								{error === 10003 &&
									<Message>
										You haven't made any orders yet. <br/>
										Why not <Link to={'/booking'}> create a booking? </Link>
									</Message>
								}

								{this.renderCurrentOrder()}

								<Button onClick={() => {
									if(this.state.currentOrder !== 0) {
										this.setState({
											currentOrder: this.state.currentOrder - 1,
											loading: true,
											receipt: undefined
										})
									}
								}}>
									Previous
								</Button>
								<Button onClick={() => {
									if(this.state.currentOrder !== this.state.data.orders.length - 1) {
										this.setState({
											currentOrder: this.state.currentOrder + 1,
											loading: true,
											receipt: undefined
										})
									}
								}}>
									Next
								</Button>
							</Segment>
							<Button as={Link} to={'/account'}> Back </Button>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);