import axios from 'axios';
import { find, findIndex } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import moment from 'moment';

import DatePicker from 'react-datepicker';
import SimpleMap from '~/components/Map/Map2';
import {
	Button,
	Form,
	Grid,
	Header,
	Icon,
	List,
	Message,
	Select,
	Segment
} from "semantic-ui-react";

import './Booking.scss';
import "react-datepicker/dist/react-datepicker.css";

class Booking extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			data: {
				bikes: {},
				locations: [],
				locationCoords: undefined,
				user: {
					details: {},
					paymentMethods: {}
				}
			},
			error: undefined,
			form: {
				bikes: [],
				start_date: moment().endOf('minute'),
				end_date: moment().add(30, 'minutes'),
				location: undefined,
				paymentMethod: undefined
			},
			get: undefined,
			loading: false,
			price: 0.0,
			summary: []
		}
	}

	componentDidMount() {
		let authToken = localStorage.getItem('auth_token');
		axios.get(`http://localhost:5000/location?id=*&auth_token=${authToken}`)
			.then(res => {
				this.setState(prevState => ({
					data: {
						...prevState.data,
						locations: res.data.locations
					}
				}));
			});

		axios.get(`http://localhost:5000/payment?id=*&auth_token=${authToken}`)
			.then(res => {
				this.setState(prevState => ({
					data: {
						...prevState.data,
							user: {
							...prevState.data.user,
								paymentMethods: res.data.payment_methods
						}
					}
				}));
			});

		axios.get(`http://localhost:5000/user/info?auth_token=${authToken}`)
			.then(res => {
				this.setState(prevState => ({
					data: {
						...prevState.data,
						user: {
							...prevState.data.user,
							details: res.data
						}
					}
				}));
			});
	}
	
	handleAddBike(bike) {
		let index;

		bike.id.forEach(i => {
			if(this.state.form.bikes.indexOf(i) === -1) index = i;
		});

		if(index !== undefined) {
			let bikes = [...this.state.form.bikes];
			bikes.push(index);

			let summary = [...this.state.summary];
			if(find(summary, {bike: bike}) === undefined) summary.push({bike: bike, quantity: 1});
			else find(summary, {bike: bike}).quantity++;
			
			this.setState(prevState => ({
				form: {
					...prevState.form,
					bikes: bikes
				},
				price: this.state.price + bike.price,
				summary: summary
			}));
		}
	}
	
	handleRemoveBike(bike) {
		let index;

		this.state.form.bikes.forEach(i => {
			if(bike.id.indexOf(i) !== -1) index = i;
		});

		if(index !== undefined) {
			let bikes = [...this.state.form.bikes];
			bikes.splice(bikes.indexOf(index), 1);

			let summary = [...this.state.summary];
			if(find(summary, {bike: bike}).quantity !== 1) find(summary, {bike: bike}).quantity--;
			else summary.splice(findIndex(summary, {bike: bike}), 1);

			this.setState(prevState => ({
				form: {
					...prevState.form,
					bikes: bikes
				},
				price: this.state.price - bike.price,
				summary: summary
			}));
		}
	}

	handleFormChange = (e, {field, value} ) => {
		if(field === 'location') {
			let coords = [];
			let loc = find(this.state.data.locations, {id: value});
			coords.push(loc.latitude, loc.longitude);

			this.setState(prevState => ({
				data: {
					...prevState.data,
					locationCoords: coords
				},
				form: {
					...prevState.form,
					bikes: [],
					location: value,
					paymentMethod: undefined
				},
				price: 0.0,
				summary: []
			}), () => {
				this.handleGetAvailableBikes();
			});
		}
		else {
			this.setState(prevState => ({
				form: {
					...prevState.form,
					[field]: value
				}
			}));
		}
	};

	handleGetAvailableBikes = () => {
		let authToken = localStorage.getItem('auth_token');
		let { location, start_date, end_date } = this.state.form;

		let bikes = {};

		axios.get(`http://localhost:5000/bike/avail?auth_token=${authToken}&location_id=${location}&start_time=${start_date.unix()}&end_time=${end_date.unix()}`)
			.then(res => {
				res.data.bikes.forEach((id, i, data) => {
					axios.get(`http://localhost:5000/bike?id=${id}&auth_token=${authToken}`)
						.then(res => {
							let bikeKey = `${res.data.make}-${res.data.model}`;
							if(!bikes.hasOwnProperty(bikeKey)) {
								bikes[bikeKey] = {
									category: res.data.category,
									make: res.data.make,
									model: res.data.model,
									id: [res.data.id],
									price: res.data.price,
									quantity: 1
								};
							}
							else {
								bikes[bikeKey].id.push(res.data.id);
								bikes[bikeKey].quantity++;
							}
							if(i === data.length - 1) {
								this.setState(prevState => ({
									data: {
										...prevState.data,
										bikes: bikes
									}
								}));
							}
						});
					}
				);
			});
	};

	handleSubmitForm = () => {
		let { bikes, location, paymentMethod, start_date, end_date } = this.state.form;
		let authToken = localStorage.getItem('auth_token');
		let billingAddress = find(this.state.data.user.paymentMethods, {id: paymentMethod}).billing_address_id;

		if(!this.isFormValid()) return;

		this.setState({
			loading: true
		});

		axios.post(`http://localhost:5000/rental?location_id=${location}&bike_ids=[${bikes}]&payment_id=${paymentMethod}&billing_id=${billingAddress}&start_time=${start_date.unix()}&end_time=${end_date.unix()}&auth_token=${authToken}`)
			.then(res => {
				this.setState({
					get: res,
					loading: false
				});
			})
			.catch(error => {
				this.setState({
					loading: false
				});
			});
	};

	isFormValid = () => {
		let { form } = this.state;
		return form.paymentMethod
			&& this.getCost() !== '0.00'
			&& !form.end_date.isBefore(form.start_date)
			&& !form.start_date.isBefore(moment())
			&& form.bikes && form.bikes.length > 0;
	};

	getCost() {
		let { form } = this.state;
		let bikePrice = this.state.price / 100;
		let discountFactor = 1;
		let duration = moment.duration(form.end_date.diff(form.start_date)).asHours();

		if(form.end_date.isBefore(form.start_date)) return (0).toFixed(2);

		if(duration >= 24 && duration < 48) discountFactor = 0.75;
		else if(duration >= 48) discountFactor = 0.5;
		return ((bikePrice * duration) * discountFactor).toFixed(2);
	}

	render() {
		let { data, form, get, summary } = this.state;

		let locationOptions = [];
		if(data.locations && data.locations.length > 0) {
			data.locations.forEach(l => {
				locationOptions.push({text: `${l.city} - ${l.name}`, value: l.id});
			});
		}

		let paymentOptions = [];
		if(data.user.paymentMethods && data.user.paymentMethods.length > 0) {
			data.user.paymentMethods.forEach(p => {
				paymentOptions.push({text: `${p.name_on_card} - ${p.card_type} ending in ${p.card_number.slice(12, 16)}`, value: p.id});
			});
		}

		let getMinTime = (value) => value.clone().startOf('day').unix() === moment().startOf('day').unix() ? moment().toDate() : moment().startOf('day').toDate();

		if(!localStorage.getItem('auth_token')) return <Redirect to={'/login'}/>;
		else if(get && get.data.success) return <Redirect to={'/account/order-history'}/>;
		else return(
			<div id={'booking'}>
				<Grid textAlign={'center'} style={{height: '100%'}} verticalAlign={'middle'}>
					<Grid.Column style={{maxWidth: 480}}>
						<Header>
							Create Booking
						</Header>
						<Form error={(get && !get.data.success)}>
							<Segment stacked>
								<Header as={'h5'}>Choose Start Time</Header>
								<Form.Field>
									<DatePicker
										dateFormat={'MMMM d, yyyy h:mm aa'}
										dropdownMode={'select'}
										minDate={moment().toDate()}
										maxDate={moment().add(1, 'months').toDate()}
										minTime={getMinTime(form.start_date)}
										maxTime={moment().endOf('day').toDate()}
										onChange={e => {
											this.setState(prevState => ({
												data: {
													...prevState.data,
													bikes: []
												},
												form: {
													...form,
													bikes: [],
													start_date: moment(e).endOf('minute'),
													end_date: moment(e).endOf('minute')
												},
												price: 0.0,
												summary: []
											}), () => {
												if(form.location) this.handleGetAvailableBikes();
											});
										}}
										selected={form.start_date.toDate()}
										showTimeSelect
										timeCaption={'Time'}
										timeFormat={'HH:mm'}
										timeIntervals={5}
									/>
								</Form.Field>

								<Header as={'h5'}>Choose End Time</Header>
								<Form.Field>
									<DatePicker
										dropdownMode={'select'}
										dateFormat={'MMMM d, yyyy h:mm aa'}
										minTime={getMinTime(form.end_date)}
										maxTime={moment().endOf('day').toDate()}
										minDate={form.start_date.toDate()}
										maxDate={form.start_date.clone().add(1, 'months').toDate()}
										onChange={e => {
											this.setState(prevState => ({
												data: {
													...prevState.data,
													bikes: []
												},
												form: {
													...form,
													bikes: [],
													end_date: moment(e).endOf('minute'),
												},
												price: 0.0,
												summary: []
											}), () => {
												if(form.location) this.handleGetAvailableBikes();
											});
										}}
										selected={form.end_date.toDate()}
										showTimeSelect
										timeCaption={'Time'}
										timeFormat={'HH:mm'}
										timeIntervals={5}
									/>
								</Form.Field>

								<Header as={'h5'}>Select Location</Header>
								<Form.Input
									control={Select}
									disabled={locationOptions.length === 0}
									field={'location'}
									name={'location'}
									placeholder={locationOptions.length === 0 ? 'No Locations Available' : 'Select a Location'}
									onChange={this.handleFormChange}
									options={locationOptions}
								/>

								<SimpleMap locations={data.locations} locationCoords={data.locationCoords}/>

								{form.location &&
									<Message content={`Available bike varieties: ${Object.keys(data.bikes).length}`}/>
								}

								<Header as={'h5'}>Select Bike</Header>
								<List verticalAlign={'middle'}>
									{Object.keys(data.bikes).length === 0 &&
										<>
											Please select a location.
										</>
									}
									<>
										{Object.keys(data.bikes).map(i => {
											let bike = data.bikes[i];
											return (
												<List.Item key={`bike-${i}`}>
													<List horizontal>
														<List.Item>
															<Button circular icon size={'mini'} title={`Remove ${bike.make} ${bike.model}`} onClick={() => {
																this.handleRemoveBike(bike);
															}}>
																<Icon name={'minus'}/>
															</Button>
														</List.Item>

														<List.Item>
															{bike.make} {bike.model} (Quantity: {bike.quantity})
														</List.Item>

														<List.Item>
															<Button circular icon size={'mini'} title={`Add ${bike.make} ${bike.model}`} onClick={() => {
																this.handleAddBike(bike);
															}}>
																<Icon name={'plus'}/>
															</Button>
														</List.Item>
													</List>
												</List.Item>
											)
										})}
									</>
								</List>

								<Header as={'h5'}>Select Payment Method</Header>
								<Form.Input
									control={Select}
									error={paymentOptions.length === 0}
									field={'paymentMethod'}
									name={'paymentMethod'}
									placeholder={paymentOptions.length === 0 ? 'No Payment Methods Available' : 'Select a payment method'}
									onChange={this.handleFormChange}
									options={paymentOptions}
								/>

								{this.isFormValid() &&
									<>
										<Message>
											<Header as={'h5'}>Order Summary</Header>
											<List>
												{summary.length > 0 &&
												<>
													{summary.map(item => (
														<List.Item key={`summary-${item.bike.id[0]}`}>
															{item.bike.make} {item.bike.model} (x{item.quantity})
														</List.Item>
													))}
												</>
												}
												<List.Item>
													<Header as={'h5'}>Subtotal</Header>
													£{this.getCost()}
												</List.Item>
											</List>
										</Message>
									</>
								}

								{this.state.loading
									? <Icon name={'circle notched'} loading />
									: <Button color={'blue'} disabled={!this.isFormValid()} onClick={this.handleSubmitForm}> Complete Booking </Button>
								}
								<Button as={Link} to={'/'}> Cancel </Button>

								<Message
									error
									header={'Booking Failed'}
									content={`Error code ${get && get.data.error}: ${get && get.data.error === 10001 ? 'bike not available.' : 'unknown error.'}`}
								/>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
});

const mapStateToProps = state => ({
	...state
});

export default connect(mapStateToProps, mapDispatchToProps)(Booking);