import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import { Link, Route, Switch, withRouter } from 'react-router-dom';
import {
	Container,
	Icon,
	Menu,
	Responsive
} from 'semantic-ui-react';

import Account from '~/components/Account/Account';
import AddBillingAddress from '~/components/AddBillingAddress/AddBillingAddress';
import AddPaymentMethod from '~/components/AddPaymentMethod/AddPaymentMethod';
import Booking from '~/components/Booking/Booking';
import About from '~/components/About/About';
import Home from '~/components/Home/Home';
import Login from '~/components/Login/Login';
import OrderHistory from '~/components/OrderHistory/OrderHistory';
import Register from '~/components/Register/Register';

import './App.scss';

class App extends React.Component {
	renderAccountOptions() {
		if(localStorage.getItem('auth_token')) {
			return [
				<Menu.Item
					active={this.props.location.pathname === '/booking'}
					as={Link}
					key={'create-booking'}
					title={'Create Booking'}
					to={'/booking'}
				>
					Create Booking
				</Menu.Item>,
				<Menu.Item
					active={this.props.location.pathname === '/account'}
					as={Link}
					key={'account'}
					title={'Account'}
					to={'/account'}
					position={'right'}
				>
					<Icon name={'user'}/>
				</Menu.Item>
			];
		}
		else {
			return [
				<Menu.Item
					active={this.props.location.pathname === '/login'}
					alt={'Log In'}
					as={Link}
					content={'Log In'}
					key={'login-nav-button'}
					position={'right'}
					to={'/login'}
				/>,
				<Menu.Item
					active={this.props.location.pathname === '/register'}
					alt={'Register'}
					as={Link}
					content={'Register'}
					key={'register-nav-button'}
					to={'/register'}
				/>
			];
		}
	}

	renderNavigation() {
		return (
			<Menu fixed={'top'} color={'teal'} inverted pointing secondary>
				<Container>
					<Menu.Item
						active={this.props.location.pathname === '/'}
						as={Link}
						title={'Home'}
						to={'/'}
					>
						<Icon name={'home'}/> ridr
					</Menu.Item>
					<Menu.Item
						active={this.props.location.pathname === '/about'}
						as={Link}
						content={'About'}
						title={'About'}
						to={'/about'}
					/>
					{this.renderAccountOptions()}
				</Container>
			</Menu>
		);
	}

	renderRoutes() {
		return (
			<Switch>
				<Route exact path={'/account'} component={Account}/>
				<Route exact path={'/account/add-billing-address'} component={AddBillingAddress}/>
				<Route exact path={'/account/add-payment-method'} component={AddPaymentMethod}/>
				<Route exact path={'/account/order-history'} component={OrderHistory}/>
				<Route exact path={'/booking'} component={Booking} />
				<Route exact path={'/about'} component={About} />
				<Route exact path={'/login'} component={Login} />
				<Route exact path={'/register'} component={Register} />
				<Route exact path={'/'} component={Home} />
			</Switch>
		);
	}

	render() {
		let auth_token = localStorage.getItem('auth_token');
		// Check to see whether the current authentication token is valid, if it's not, delete it and rerender
		if(auth_token) {
			axios.get(`http://localhost:5000/user/info?auth_token=${auth_token}`)
				.then(res => {
					if (!res.data.success && (res.data.error === 10 || res.data.error === 8)) {
						localStorage.removeItem('auth_token');
						this.forceUpdate();
					}
				})
				.catch(error => {
					// If the server is unreachable, log the user out by deleting the local authentication token
					if(localStorage.getItem('auth_token')) localStorage.removeItem('auth_token');
				})
		}
		return (
			<Responsive>
				{this.renderNavigation()}
				{this.renderRoutes()}
			</Responsive>
		);
	}
}

const mapDispatchToProps = dispatch => ({
});

const mapStateToProps = state => ({
	...state
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
