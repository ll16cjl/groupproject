const {
	override,
	addBabelPlugins
} = require('customize-cra');

// Add the ability to refer to the project root when importing with '~'
const importFromRoot = [
	"root-import",
	{
		rootPathPrefix: "~",
		rootPathSuffix: "src"
	}
]

// Override the webpack config to use the importFromRoot rule
module.exports = override(
	...addBabelPlugins(importFromRoot)
);