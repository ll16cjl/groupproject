#-------------------------------------------------
#
# Project created by QtCreator 2019-02-12T13:56:26
#
#-------------------------------------------------
QT       += core gui network webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bike-hire-desktop-app
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += /usr/local/include $$PWD/include $$PWD/lib/
DEPENDPATH += /usr/local/include $$PWD/include $$PWD/lib/

CONFIG += c++11

SOURCES += \
        src/main.cpp \
        src/mainwindow.cpp \
        src/navigation.cpp \
        src/loginscreen.cpp \
        src/managementscreen.cpp \ 
        src/inventoryview.cpp \
        src/bookingsview.cpp \
        src/reservationsview.cpp \
        src/bookingpage.cpp \
        src/serversetup.cpp \
        src/bikedialog.cpp \
        src/bikeinfo.cpp \
        src/locationdialog.cpp \
        src/createbookingdialog.cpp \
        src/locationsview.cpp \
    src/receiptdialog.cpp

HEADERS += \
        include/mainwindow.h \
        include/navigation.h \
        include/loginscreen.h \
        include/managementscreen.h \ 
        include/inventoryview.h \
        include/bookingsview.h \
        include/reservationsview.h \
        include/bookingpage.h \
        include/serversetup.h \
        include/bikedialog.h \
        include/bikeinfo.h \
        include/locationdialog.h \
        include/createbookingdialog.h \
        include/locationsview.h \
    include/receiptdialog.h

FORMS += \
        forms/mainwindow.ui \
        forms/navigation.ui \
        forms/loginscreen.ui \
        forms/managementscreen.ui \
        forms/inventoryview.ui \
        forms/bookingsview.ui \
        forms/reservationsview.ui \
        forms/bookingpage.ui \
        forms/bikedialog.ui \
        forms/bikeinfo.ui \
        forms/locationdialog.ui \
        forms/createbookingdialog.ui \
        forms/locationsview.ui \
    forms/receiptdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
    resources.qrc
