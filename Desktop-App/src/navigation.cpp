#include "navigation.h"
#include "ui_navigation.h"
#include <QDebug>

/*!
 * \brief Navigation::Navigation Create a new vertical navigation bar.
 * This bar allows for dynamic allocation of actions.
 *
 * \param parent the parent widget
 */
Navigation::Navigation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Navigation)
{
    this->ui->setupUi(this);
    qApp->installEventFilter(this);
    this->navGroups = QMap<QString, QPointer<NavPushButtonGroup>>();
    this->buttonGroup = new QButtonGroup(this);
    this->isFirstButtonAdded = true;
    this->bookingOffset = 0;

    connect(ui->newBookingButton, &QPushButton::clicked, this, &Navigation::newBookingClicked);

    this->setStyleSheet(
        "NavPushButton              { padding-top: 8px; padding-bottom: 8px; }"
        "NavPushButton:checked      { background-color: #3498db; color: white; }"
        "NavPushButton:hover        { background-color: #2980b9; color: white; }"
        "QLabel                     { margin-bottom: 5px; margin-left: 3px; }"
    );
}


/*!
 * \brief Navigation::~Navigation Destructor for the navigation widget.
 */
Navigation::~Navigation()
{
    delete ui;
}


/*!
 * \brief Navigation::addItem Add a new item to the menu,
 * given the group that it belongs to.
 *
 * \param name string to be displayed to the user describing the item.
 * \param action the action that clicking on the item performs.
 * \param navGroup the group that the item belongs to.
 * \param focus should the item be immediately selected?
 */
void Navigation::addItem(QString name, QString navGroup, QWidget *targetWidget, bool focus)
{   
    QPointer<NavPushButtonGroup> group = getGroup(navGroup);
    NavPushButton *newButton = new NavPushButton(name, targetWidget, this);

    // we need to add to a button group to keep exclusivity global,
    // even if the button is in another group.
    this->buttonGroup->addButton(newButton);

    newButton->setAction(new QAction(newButton));
    connect(newButton->getAction(), &QAction::triggered, this, &Navigation::routePages);
    group->addItem(name, newButton);
    if (focus) {
        newButton->click();
    }

    // if it is the first button set it to checked
    if (this->isFirstButtonAdded) {
        newButton->setChecked(true);
        this->isFirstButtonAdded = false;
    }
}


/*!
 * \brief Navigation::addLabel Add a new label to the navigation.
 * \param name The text to be displayed by the label.
 * \param navGroup The navigation group the label belongs to.
 */
void Navigation::addLabel(QString name, QString navGroup)
{
    QPointer<NavPushButtonGroup> group = getGroup(navGroup);
    QLabel *newLabel = new QLabel(name);
    group->addItem(name, newLabel);
}


/*!
 * \brief Navigation::removeItem Remove an item from the nav.
 * \param name Name of item to remove.
 * \param navName Name of the group the item is in.
 */
void Navigation::removeItem(QString name, QString navName)
{
    QPointer<NavPushButtonGroup> group;

    if (this->navGroups.contains(navName)) {
        group = this->navGroups[navName];
        group->removeItem(name);
    }
}


/*!
 * \brief Navigation::routePages Slot to enable the routing of pages.
 * This is done by looking at the targetWidget of the NavPushButton from
 * which the action was triggered.
 */
void Navigation::routePages()
{
    NavPushButton *currentButton = qobject_cast<NavPushButton *>(sender()->parent());
    emit changePage(currentButton->getTargetWidget());
}


/*!
 * \brief Navigation::setBookingOffset set the booking offset value.
 * This is needed since bookings require their position to be displayed
 * in the button label.
 * \param index the offset.
 */
void Navigation::setBookingOffset(int index)
{
    this->bookingOffset = index;
}


/*!
 * \brief Navigation::getBookingOffset get the offset to the start of the bookings.
 * \return int, offset.
 */
int Navigation::getBookingOffset()
{
    return this->bookingOffset;
}


/*!
 * \brief Navigation::getGroup Get a pointer to the group referenced by string.
 * If one doesn't exist, create a new one.
 *
 * \param name The name of the group.
 * \return A QPointer to the group in memory.
 */
QPointer<NavPushButtonGroup> Navigation::getGroup(QString name)
{
    QPointer<NavPushButtonGroup> group;

    if (navGroups[name]) {
        group = navGroups[name];
    } else {
        group = new NavPushButtonGroup();
        navGroups[name] = QPointer<NavPushButtonGroup>(group);
        ui->verticalLayout->addWidget(group);
    }
    return group;
}


/*!
 * \brief NavPushButton::NavPushButton Create a new navigation button.
 *
 * \param navName The name to be displayed to the user.
 * \param action The action that is performed by clicking the button.
 * \param parent The parent widget.
 */
NavPushButton::NavPushButton(QString navName, QWidget *targetWidget, QWidget* parent) :
    QPushButton(parent)
{
    this->navName = navName;
    this->targetWidget = targetWidget;
    this->setText(navName);
    this->setCheckable(true);
}


/*!
 * \brief NavPushButton::paintEvent Override the paint event to make nice looking nav.
 *
 * \param event Current paint event.
 */
void NavPushButton::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QStyleOptionButton option;
    option.initFrom(this);
    option.state |= (this->isChecked() ? QStyle::State_On : QStyle::State_Off);

    style()->drawPrimitive(QStyle::PE_Widget, &option, &painter, this);

    if(!action->icon().isNull()) {
        QIcon::Mode mode = ((option.state & QStyle::State_MouseOver) == 0) ? QIcon::Normal : QIcon::Active;
        if(!isEnabled())
            mode = QIcon::Disabled;
    }

    option.rect = rect().adjusted(10, 0, 0, 0);
    option.text = fontMetrics().elidedText(navName, Qt::ElideRight, option.rect.width(), Qt::TextShowMnemonic);
    style()->drawControl(QStyle::CE_CheckBoxLabel, &option, &painter, this);
}


/*!
 * \brief NavPushButton::getAction Get the action assigned to this button.
 *
 * \return QAction
 */
QAction *NavPushButton::getAction()
{
    return this->action;
}


/*!
 * \brief NavPushButton::setAction Set the push buttons action.
 *
 * \param action the action to be assigned to the push button
 */
void NavPushButton::setAction(QAction *action)
{
    this->action = action;
    connect(this, &QPushButton::clicked, action, &QAction::trigger);
}


/*!
 * \brief NavPushButton::getTargetWidget returns the pointer to the target widget within a QStackedWidget.
 * This allows a 'page' to be assigned to the action. This makes assigning a page to an action much easier,
 * and dynamic.
 *
 * \return QWidget*, pointer to the target widget
 */
QWidget *NavPushButton::getTargetWidget()
{
    return this->targetWidget;
}


/*!
 * \brief NavPushButtonGroup::NavPushButtonGroup Create a push button group for the navigation.
 * This allows the navigation to remain organised.
 *
 * \param name The name to be displayed by the push button.
 * \param parent The parent of the push button.
 */
NavPushButtonGroup::NavPushButtonGroup()
{
    this->navButtons = QMap<QString, QPointer<QWidget>>();
    this->newGroupLayout = new QVBoxLayout();
    this->newGroupLayout->setContentsMargins(0, 10, 0, 0);
    this->setLayout(this->newGroupLayout);
}


/*!
 * \brief NavPushButtonGroup::addItem Add a widget to the group whether it be a label or navButton.
 *
 * \param newWidget New widget to add to the group.
 */
void NavPushButtonGroup::addItem(QString name, QWidget *newWidget)
{
    QWidget *fakeWidget = new QWidget(this);
    QVBoxLayout *newLayout = new QVBoxLayout();
    fakeWidget->setLayout(newLayout);
    fakeWidget->setContentsMargins(0,0,0,0);
    fakeWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    newLayout->setContentsMargins(0,0,0,0);
    newLayout->setMargin(0);
    newLayout->addWidget(newWidget);
    newWidget->setContentsMargins(0,0,0,0);
    this->newGroupLayout->addWidget(fakeWidget);
    this->newGroupLayout->setSpacing(2);
    this->navButtons[name] = QPointer<QWidget>(newWidget);
}


/*!
 * \brief NavPushButtonGroup::removeItem Remove an item from the group.
 * \param name Name of the item to remove.
 */
void NavPushButtonGroup::removeItem(QString name)
{
    if (this->navButtons.contains(name)) {
        layout()->removeWidget(this->navButtons[name]);
        delete this->navButtons[name];
        this->navButtons.remove(name);
    }
}
