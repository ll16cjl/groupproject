#include "mainwindow.h"
#include <QApplication>
#include <QSettings>
#include "serversetup.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    /*
     * Accessiblity Settings
     */
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    /*
     * Create Application
     */
    QApplication a(argc, argv);

    QSettings::setDefaultFormat(QSettings::IniFormat);
    QCoreApplication::setOrganizationName("bike-hire");
    QCoreApplication::setOrganizationDomain("bike-hire.com");
    QCoreApplication::setApplicationName("bike-hire");
    QSettings s;

    /*
     * Default settings
     */
    s.setValue("session-token", "");
    s.setValue("defaultServerIP", "127.0.0.1");
    s.setValue("defaultServerPort", "5000");
    s.setValue("get-address-api-key", "rYKAzx4cO02OUWlZuhlctg18579");
    s.setValue("currentLocationId", 1);

    auto server = Server::makeInstance();

    MainWindow w;
    w.show();
    return a.exec();
}
