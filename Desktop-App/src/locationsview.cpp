#include "locationsview.h"
#include "ui_locationsview.h"


/**
 * @brief LocationsView::LocationsView The location view will allow the user to see all
 * locations that are currently setup within the database, and view the exact location
 * on a map. This will allow staff to help determine how far a customer needs to go
 * should the bike they want not be available at this store.
 * @param parent the parent widget.
 */
LocationsView::LocationsView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LocationsView)
{
    ui->setupUi(this);

    QVBoxLayout *vLayoutButtons = new QVBoxLayout();
    this->engine = new QWebEngineView(this);
    vLayoutButtons->addWidget(this->engine);
    ui->widget->setLayout(vLayoutButtons);

    connect(Server::instance(), &Server::locationsUpdated, this, &LocationsView::updateLocations);
}


/**
 * @brief LocationsView::~LocationsView delete the locations view.
 */
LocationsView::~LocationsView()
{
    delete ui;
}


/**
 * @brief LocationsView::updateLocations update the locations that are available, by retrieving all
 * possible locations and displaying them as a series of buttons.
 * @param locations location data from the server.
 */
void LocationsView::updateLocations(QMap<int, LocationData> locations)
{
    for (auto location : locations) {
        LocationPushButton *newButton = new LocationPushButton(location.nameWithCity(), location.longitude, location.latitude, this);
        ui->verticalLayout_2->addWidget(newButton);
        connect(newButton, &LocationPushButton::clicked, this, &LocationsView::updateMapsUrl);
    }

    QUrl url = QUrl("https://www.google.com/maps/search/?api=1");
    this->engine->load(url);
    this->engine->show();
}


/**
 * @brief LocationsView::updateMapsUrl When a button is clicked the url
 * of the maps API must be updated to the clicked location.
 */
void LocationsView::updateMapsUrl()
{
    LocationPushButton *currentButton = qobject_cast<LocationPushButton *>(sender());
    QUrl url = QUrl("https://www.google.com/maps/search/");
    QUrlQuery params;
    params.addQueryItem("output", "embed");
    params.addQueryItem("api", QString::number(1));
    params.addQueryItem("query", QString::number(currentButton->latitude) + "," + QString::number(currentButton->longitude));

    this->engine->load(url.toString() + "?" + params.toString());
    this->engine->show();
}


/**
 * @brief LocationsView::showEvent when the locations view is shown
 * delete all previous buttons and request the new locations.
 * @param event show event.
 */
void LocationsView::showEvent(QShowEvent *event)
{
    QLayoutItem* item;
    while (( item = ui->verticalLayout_2->takeAt(0)) != nullptr) {
        delete item->widget();
        delete item;
    }
    Server::instance()->requestLocations();
}


/**
 * @brief LocationPushButton::LocationPushButton create a new location button.
 * @param name the name displayed on the button.
 * @param longitude the longitude the button represents.
 * @param latitude the latitude the button represents.
 * @param parent the parent widget.
 */
LocationPushButton::LocationPushButton(QString name, double longitude, double latitude, QWidget *parent) :
    QPushButton (name, parent)
{
    this->longitude = longitude;
    this->latitude = latitude;
}
