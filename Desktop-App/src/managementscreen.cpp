#include "managementscreen.h"
#include "ui_managementscreen.h"
#include <QDebug>
#include <QMessageBox>


/**
 * @brief ManagementScreen::ManagementScreen create the management screen and
 * create the navigation menu that will be used to navigate the stacked
 * widget in the ui.
 * @param parent the parent widget.
 */
ManagementScreen::ManagementScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManagementScreen)
{
    ui->setupUi(this);
    this->openBookings = new QMap<QString, BookingPage *>();

    connect(ui->navigationBar, &Navigation::changePage, this, &ManagementScreen::handleCurrentPage);
    connect(ui->navigationBar, &Navigation::newBookingClicked, this, &ManagementScreen::addNewBookingPage);
    connect(this, &ManagementScreen::sendOpenBookingsMap, ui->inventory, &InventoryView::setOpenBookings);

    connect(
        ui->logoutButton,
        &QAbstractButton::clicked,
        [=]() {
            bool hasOpenBookings = false;
            for (int i = 0; i < ui->pages->count(); ++i) {
                QWidget *page = ui->pages->widget(i);
                if (dynamic_cast<BookingPage*>(page) != nullptr) {
                    hasOpenBookings = true;
                    break;
                }
            }

            if (hasOpenBookings) {
                auto result = QMessageBox::warning(
                    this, 
                    "Log out?", 
                    "Are you sure you want to log out?\nAny unsaved changes or bookings will be lost.",
                    QMessageBox::Yes | QMessageBox::No,
                    QMessageBox::No
                );
                if (result == QMessageBox::Yes) {
                    this->logoutAttempt();
                }
            }
            else {
                this->logoutAttempt();
            }
        }
    );

    this->createInitialNavigation();
    emit sendOpenBookingsMap(this->openBookings);
}


/**
 * @brief ManagementScreen::~ManagementScreen delete the management screen.
 */
ManagementScreen::~ManagementScreen() {
    delete ui;
}


/**
 * @brief ManagementScreen::handleCurrentPage whent the current page changes,
 * set the current widget to the index that that particular page is linked to.
 * @param page
 */
void ManagementScreen::handleCurrentPage(QWidget *page)
{
    this->ui->pages->setCurrentWidget(page);
}


/**
 * @brief ManagementScreen::addNewBookingPage add a new booking page to the screen.
 * This includes adding the page to the navigation and to the stacked widget.
 * This method also keeps track of the total number of pages, which can be use to
 * calculate the booking number.
 */
void ManagementScreen::addNewBookingPage() {
    BookingPage *bookingPage = new BookingPage;
    connect(
        bookingPage,
        &BookingPage::bookingCancelled,
        [=]() {
            removeBookingPage(bookingPage);
        }
    );

    ui->pages->addWidget(bookingPage);

    ++totalBookings;

    QString pageTitle = "Booking " + QString::number(totalBookings);
    bookingPage->setTitle(pageTitle);
    this->openBookings->insert(pageTitle, bookingPage);
    ui->navigationBar->addItem(pageTitle, "bookings", bookingPage, true);
}


/**
 * @brief ManagementScreen::removeBookingPage remove a booking page from the screen.
 * @param page the page to remove.
 */
void ManagementScreen::removeBookingPage(BookingPage *page) {
    if (page) {
        ui->navigationBar->removeItem(page->getTitle(), "bookings");
        this->openBookings->remove(page->getTitle());
        ui->pages->removeWidget(page);
        delete page;
        page = nullptr;
    }
}


/**
 * @brief ManagementScreen::createInitialNavigation create the navigation that
 * will be used to select pages on the stacked widget.
 */
void ManagementScreen::createInitialNavigation()
{
    ui->navigationBar->addLabel("Screens:", "screens");
    ui->navigationBar->addItem("Inventory", "screens", ui->inventory);
    ui->navigationBar->addItem("Reservations", "screens", ui->reservations);
    ui->navigationBar->addItem("Locations", "screens", ui->locations);

    // new bookings
    ui->navigationBar->addLabel("Open bookings:", "bookings");
    ui->navigationBar->setBookingOffset(2);
}


/**
 * @brief ManagementScreen::showEvent when the management screen is displayed get the
 * current user info and state who is logged in.
 * @param event show event
 */
void ManagementScreen::showEvent(QShowEvent *event)
{
    connect(Server::instance(), &Server::currentUserUpdated, this, [=](UserData user) {
        if (user.first == "" && user.last == "") {
            ui->loggedInName->setText("Error, could not retrieve name");
        } else {
            ui->loggedInName->setText("Logged in as:\n" + user.first + " " + user.last);
        }

        // set the current user in the server class
        Server::instance()->setCurrentUser(user);
    });

    Server::instance()->requestCurrentUser();
}
