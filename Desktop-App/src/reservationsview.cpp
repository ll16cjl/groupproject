#include "reservationsview.h"
#include "ui_reservationsview.h"
#include <QDebug>
#include <QPainter>
#include <QFile>
#include <memory>
#include <utility>
#include "receiptdialog.h"

namespace {
    class CustomTreeWidgetItem : public QTreeWidgetItem {
      public:
        CustomTreeWidgetItem(const QStringList &row);
      private:
        bool operator<(const QTreeWidgetItem &other) const;
    };

    /*!
     * \brief CustomTreeWidgetItem::CustomTreeWidgetItem Create a custom tree widget item class.
     * \param strings A list of strings to add to the row.
     */
    CustomTreeWidgetItem::CustomTreeWidgetItem(const QStringList &strings)
        :QTreeWidgetItem(strings)
    {

    }


    /*!
     * \brief CustomTreeWidgetItem::operator < Used when sorting the values in the tree.
     * \param other the other item to compare against
     * \return bool
     */
    bool CustomTreeWidgetItem::operator<(const QTreeWidgetItem &other) const {
        int column = treeWidget()->sortColumn();
        bool ok1, ok2;

        int thisValue = text(column).toInt(&ok1);
        int otherValue = other.text(column).toInt(&ok2);

        if (ok1 && ok2)
            return thisValue < otherValue;

        return text(column).toLower() < other.text(column).toLower();
    }
}


/**
 * @brief ReservationsView::ReservationsView Create the reservations view.
 * @param parent the parent widget.
 */
ReservationsView::ReservationsView(QWidget *parent) : QWidget(parent), ui(new Ui::ReservationsView)
{
    ui->setupUi(this);

    createBikeContextMenu();
    createSearchByMenu();

    ui->searchResultsTree->header()->setStretchLastSection(true);
    ui->searchResultsTree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

    connect(this, &ReservationsView::refreshTree, this, &ReservationsView::getAllReservations);
    connect(ui->refreshListButton, &QPushButton::clicked, this, &ReservationsView::getAllReservations);
    connect(Server::instance(), &Server::bookingsUpdated, this, &ReservationsView::populateTreeView);
    connect(this->printReciept, &QAction::triggered, [=]() {
        QTreeWidgetItem *currentItem = ui->searchResultsTree->currentItem();
        handlePrintReciept(currentItem->data(0, Qt::UserRole).toInt());
    });
    connect(this->viewReciept, &QAction::triggered, [=]() {
        QTreeWidgetItem *currentItem = ui->searchResultsTree->currentItem();
        handleViewReciept(currentItem->data(0, Qt::UserRole).toInt());
    });
}


/**
 * @brief ReservationsView::~ReservationsView Destroy the reservations view.
 */
ReservationsView::~ReservationsView()
{
    delete ui;
}


/*!
 * \brief ReservationsView::createSearchByMenu Simple helper function to create
 * a new menu with the headers in the tree and add them to the search by menu.
 */
void ReservationsView::createSearchByMenu()
{
    this->searchByMenu = new QMenu();
    this->searchByGroup = new QActionGroup(this);
    this->filterColumnIndex = QMap<QString, int>();

    QAction *allAction = new QAction("All");
    searchByGroup->addAction(allAction);
    allAction->setCheckable(true);
    allAction->setChecked(true);
    filterColumnIndex.insert("All", -1);
    connect(allAction, &QAction::triggered, [=]() {this->currentSearchByOption = "All";});

    for (int x = 0; x < ui->searchResultsTree->columnCount(); x++) {
        QString headerName = ui->searchResultsTree->model()->headerData(x, Qt::Horizontal).toString();
        QAction *newAction = new QAction(headerName);
        filterColumnIndex.insert(headerName, x);
        searchByGroup->addAction(newAction);
        newAction->setCheckable(true);
        connect(newAction, &QAction::triggered, [=]() {this->currentSearchByOption = headerName;});
    }
    searchByMenu->addActions(searchByGroup->actions());
    ui->searchByButton->setMenu(searchByMenu);
    this->currentSearchByOption = "All";
}


/*!
 * \brief ReservationsView::getAllBikes get all the bikes in the database from the
 * server and add them to the tree.
 */
void ReservationsView::getAllReservations()
{
    ui->searchResultsTree->clear();

    Server::instance()->requestBookings();
}


/**
 * @brief ReservationsView::populateTreeView Populate the tree view with data from all
 * of the bookings that have been made.
 * @param bookings QMap with all of the bookings.
 */
void ReservationsView::populateTreeView(QMap<int, BookingData> bookings)
{
    for (auto booking : bookings.keys()) {
        QTreeWidgetItem *item;
        QStringList row;
        int bikes = 0;

        row << "- Bikes"
            << bookings.value(booking).location.nameWithCity()
            << bookings.value(booking).user.fullName()
            << QDateTime::fromTime_t(bookings.value(booking).start).toString("dd/MM/yyyy hh:mm")
            << QDateTime::fromTime_t(bookings.value(booking).end).toString("dd/MM/yyyy hh:mm")
            << QDateTime::fromTime_t(bookings.value(booking).reservationTime).toString("dd/MM/yyyy hh:mm");

        item = new QTreeWidgetItem(row);
        item->setData(0, Qt::UserRole, bookings.value(booking).id);

        for (auto bike : bookings.value(booking).bikes) {
            bikes++;
            QStringList bikeRow;
            bikeRow << bike.model + " (" + bike.make + ")";
            QTreeWidgetItem *child = new QTreeWidgetItem(bikeRow);
            item->addChild(child);
        }

        item->setText(0, QString::number(bikes) + " Bike(s)");
        ui->searchResultsTree->insertTopLevelItem(0, item);
    }
}



/*!
 * \brief ReservationsView::showEvent update the bikes when the inventory screen is shown.
 * \param event event deatils.
 */
void ReservationsView::showEvent(QShowEvent *event)
{
    getAllReservations();
}



/*!
 * \brief ReservationsView::searchBikes Iterate over the list of bikes and filter them based on
 * the search term and the search by inputs.
 * \param searchTerm QString the text currently in the text box.
 */
void ReservationsView::searchReservations(QString searchTerm)
{
    int searchByIndex = this->filterColumnIndex.value(this->currentSearchByOption);
    bool addToTree = false;

    QTreeWidgetItemIterator iterator(ui->searchResultsTree);

    while (*iterator) {
        if (searchByIndex == -1) {
            for (int x = 0; x < ui->searchResultsTree->columnCount(); x++) {
                bool containsSearchTerm = (*iterator)->text(x).contains(searchTerm, Qt::CaseInsensitive);
                addToTree = containsSearchTerm;
                if (containsSearchTerm) break;
            }
        } else
            addToTree = (*iterator)->text(searchByIndex).contains(searchTerm, Qt::CaseInsensitive);

        if (addToTree)
            (*iterator)->setHidden(false);
        else
            (*iterator)->setHidden(true);

        ++iterator;
    }
}


/*!
 * \brief ReservationsView::on_searchBar_textChanged Update the search results when text in the
 * search bar has changed.
 * \param text the text currently in the search bar
 */
void ReservationsView::on_searchBar_textChanged(const QString &text)
{
    this->searchReservations(text);
}


/*!
 * \brief ReservationsView::on_searchButton_clicked update the search results when the search
 * button is clicked.
 */
void ReservationsView::on_searchButton_clicked()
{
    this->searchReservations(ui->searchBar->text());
}


/**
 * @brief ReservationsView::createBikeContextMenu create the context menu
 * for a reservation and assign it to each reservation in the tree view.
 */
void ReservationsView::createBikeContextMenu() {
    reservationMenu = new QMenu(this);
    viewReciept = new QAction("View Reciept...");
    printReciept = new QAction("Print Reciept...");
    reservationMenu->addAction(viewReciept);
    reservationMenu->addAction(printReciept);
    ui->searchResultsTree->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->searchResultsTree->setSortingEnabled(true);
}


/*!
 * \brief ReservationsView::on_searchResultsTree_customContextMenuRequested when the treeview is
 * right clicked show a context menu for that specific reservation in the tree.
 * \param pos the position of the click.
 */
void ReservationsView::on_searchResultsTree_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->searchResultsTree->indexAt(pos);

    if (index.isValid()) {
        this->reservationMenu->exec(ui->searchResultsTree->viewport()->mapToGlobal(pos));
    }
}


/**
 * @brief ReservationsView::handlePrintReciept
 */
void ReservationsView::handlePrintReciept(int bookingID) {
    /*
     * get the receipt data
     */
    auto reply = Server::instance()->requestReceiptWithReply(bookingID);
    connect(reply, &QNetworkReply::finished, [=]() {
        ReceiptData data = ReceiptData::fromNetworkReply(reply);
        /*
         * Display print dialog and print.
         */
        QPrinter *printer = new QPrinter;
        QPrintDialog *dialog = new QPrintDialog(printer, this);
        dialog->setWindowTitle("Print Receipt");
        if (dialog->exec() == QDialog::Accepted) {
            ReceiptDialog::print(data, printer);
        }
        delete dialog;
        delete printer;
    });
}

/**
 * @brief ReservationsView::handleViewReciept
 */
void ReservationsView::handleViewReciept(int bookingID) {
    /*
     * get the receipt data
     */
    auto reply = Server::instance()->requestReceiptWithReply(bookingID);
    connect(reply, &QNetworkReply::finished, [=]() {
        ReceiptData data = ReceiptData::fromNetworkReply(reply);
        ReceiptDialog::display(data, this);
    });
}
