#include "createbookingdialog.h"
#include "ui_createbookingdialog.h"

/*!
 * \brief CreateBookingDialog::CreateBookingDialog create a booking dialog with a list of the open bookings.
 * This will be used in the inventory view to select a specific booking to add the bike to.
 * \param bikeData the data about the bike that has been selected.
 * \param openBookings the current open bookings.
 * \param parent
 */
CreateBookingDialog::CreateBookingDialog(BikeData bikeData, QStringList openBookings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateBookingDialog)
{
    ui->setupUi(this);
    ui->selectBookingLabel->setHidden(false);
    ui->comboBox->setHidden(false);
    ui->comboBox->addItems(openBookings);
    addBikeInformation(bikeData);
}


/**
 * @brief CreateBookingDialog::CreateBookingDialog create a new booking dialog.
 * @param bikeData the bike data.
 * @param parent the parent widget.
 */
CreateBookingDialog::CreateBookingDialog(BikeData bikeData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateBookingDialog)
{
    ui->setupUi(this);
    ui->selectBookingLabel->setHidden(true);
    ui->comboBox->setHidden(true);
    addBikeInformation(bikeData);
}


/**
 * @brief CreateBookingDialog::~CreateBookingDialog delete the booking dialog.
 */
CreateBookingDialog::~CreateBookingDialog()
{
    delete ui;
}


/**
 * @brief CreateBookingDialog::addCreateBookingDialog create booking dialog and delete after
 * data is submit.
 * @param bikeData the bike data to be displayed.
 * @param parent the parent widget.
 * @return the result.
 */
CreateBookingDialog::Result CreateBookingDialog::addCreateBookingDialog(BikeData bikeData, QWidget *parent)
{
    std::unique_ptr<CreateBookingDialog> dialog(new CreateBookingDialog(bikeData, parent));

    bool accepted = dialog->exec();
    auto result = dialog->getResult(bikeData);
    result.submit = result.submit && accepted;
    return result;
}


/*!
 * \brief CreateBookingDialog::CreateBookingDialog Create a new booking dialoge.
 * \param bikeData the data about the bike that has been selected.
 * \param openBookings the current open bookings.
 * \param parent
 */
CreateBookingDialog::Result CreateBookingDialog::addCreateBookingDialog(BikeData bikeData, QStringList openBookings, QWidget *parent)
{
    std::unique_ptr<CreateBookingDialog> dialog(new CreateBookingDialog(bikeData, openBookings, parent));

    bool accepted = dialog->exec();
    auto result = dialog->getResult(bikeData);
    result.submit = result.submit && accepted;
    return result;
}


/*!
 * \brief CreateBookingDialog::addBikeInformation Add relevent bike information to remide the user
 * what bike is currently being booked.
 * \param bikeData the data of the currently selected bike.
 */
void CreateBookingDialog::addBikeInformation(const BikeData &bikeData)
{
    ui->makeLabel->setText(bikeData.make);
    ui->modelLabel->setText(bikeData.model);
    ui->priceLabel->setText(QString::number(bikeData.price));
    ui->locationLabel->setText(bikeData.location.nameWithCity());
}


/**
 * @brief CreateBookingDialog::getResult get the result of the data that has been entered in
 * the dialog.
 * @param bikeData used to get the id of the bike.
 * @return the result.
 */
CreateBookingDialog::Result CreateBookingDialog::getResult(const BikeData &bikeData)
{
    CreateBookingDialog::Result result;
    result.bikeId = bikeData.id;
    result.booking = ui->comboBox->currentText();
    result.submit = true;
    return result;
}


/**
 * @brief CreateBookingDialog::accept dialog accepted and data is to be submit.
 */
void CreateBookingDialog::accept()
{
    if (ui->comboBox->currentText() == "") {
        QMessageBox::warning(
            this,
            "No booking selected",
            "You must select a booking to add a bike to",
            QMessageBox::Ok
        );
    } else
        QDialog::accept();
}
