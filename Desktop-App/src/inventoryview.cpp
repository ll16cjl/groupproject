#include "inventoryview.h"
#include "ui_inventoryview.h"
#include <QDebug>

namespace {
    class CustomTreeWidgetItem : public QTreeWidgetItem {
      public:
        CustomTreeWidgetItem(const QStringList &row);
      private:
        bool operator<(const QTreeWidgetItem &other) const;
    };

    /*!
     * \brief CustomTreeWidgetItem::CustomTreeWidgetItem Create a custom tree widget item class.
     * \param strings A list of strings to add to the row.
     */
    CustomTreeWidgetItem::CustomTreeWidgetItem(const QStringList &strings)
        :QTreeWidgetItem(strings)
    {

    }


    /*!
     * \brief CustomTreeWidgetItem::operator < Used when sorting the values in the tree.
     * \param other the other item to compare against
     * \return bool
     */
    bool CustomTreeWidgetItem::operator<(const QTreeWidgetItem &other) const {
        int column = treeWidget()->sortColumn();
        bool ok1, ok2;

        int thisValue = text(column).toInt(&ok1);
        int otherValue = other.text(column).toInt(&ok2);

        if (ok1 && ok2)
            return thisValue < otherValue;

        return text(column).toLower() < other.text(column).toLower();
    }
}


/**
 * @brief InventoryView::InventoryView create the inventory view which will be used to
 * display a list of the available bikes in the shop. The user will be able to search
 * for a bike to view more information, remove it or add it to a booking.
 * @param parent the parent widget.
 */
InventoryView::InventoryView(QWidget *parent) : QWidget(parent), ui(new Ui::InventoryView)
{
    ui->setupUi(this);

    // IMPORTANT, date time setting must remain in this order.
    loadedStatus = false;
    QDateTime current = QDateTime::currentDateTime();
    ui->dateTimeEdit_2->setDateTime(current);
    ui->dateTimeEdit->setDateTime(current);
    loadedStatus = true;

    this->onlyCurrentLocation = false;
    this->inventory = QMap<QString, QList<int>>();

    createSearchByMenu();
    createBikeContextMenu();

    ui->searchResultsTree->header()->setStretchLastSection(true);
    ui->searchResultsTree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);

    connect(removeAction, &QAction::triggered, this, [=]{
        QTreeWidgetItem *itemToRemove = ui->searchResultsTree->currentItem();

        // Get the bike that is referenced by the current item
        QList<int> bikes = this->inventory.value(itemToRemove->data(0, Qt::UserRole).toString());
        BikeData bikeData = Server::instance()->getCachedBike(bikes.takeFirst());

        removeBike(bikeData.id);
    });

    connect(bookingAction, &QAction::triggered, this, [=]{
        QTreeWidgetItem *currentItem = ui->searchResultsTree->currentItem();

        QList<int> bikes = this->inventory.value(currentItem->data(0, Qt::UserRole).toString());
        if (!bikes.empty()) {
            BikeData bikeData = Server::instance()->getCachedBike(bikes[0]);
            showBikeBookingDialog(bikeData.id);
        }
    });

    connect(viewBikeData, &QAction::triggered, this, [=]{
        QTreeWidgetItem *currentItem = ui->searchResultsTree->currentItem();

        QList<int> bikes = this->inventory.value(currentItem->data(0, Qt::UserRole).toString());
        if (!bikes.empty()) {
            BikeData bikeData = Server::instance()->getCachedBike(bikes[0]);
            showBikeInfoDialog(bikeData.id);
        }
    });

    connect(ui->newItemButton, &QPushButton::clicked, this, &InventoryView::showAddBikeDialog);
    connect(this, &InventoryView::refreshTree, this, &InventoryView::getBikes);
    connect(ui->refreshListButton, &QPushButton::clicked, this, &InventoryView::getBikes);
    connect(Server::instance(), &Server::bikesUpdated, this, &InventoryView::populateTreeView);
}


/**
 * @brief InventoryView::~InventoryView delete the inventory view.
 */
InventoryView::~InventoryView()
{
    delete ui;
}


/*!
 * \brief InventoryView::createSearchByMenu Simple helper function to create
 * a new menu with the headers in the tree and add them to the search by menu.
 */
void InventoryView::createSearchByMenu()
{
    this->searchByMenu = new QMenu();
    this->searchByGroup = new QActionGroup(this);
    this->filterColumnIndex = QMap<QString, int>();

    QAction *allAction = new QAction("All");
    searchByGroup->addAction(allAction);
    allAction->setCheckable(true);
    allAction->setChecked(true);
    filterColumnIndex.insert("All", -1);
    connect(allAction, &QAction::triggered, [=]() {this->currentSearchByOption = "All";});

    for (int x = 0; x < ui->searchResultsTree->columnCount(); x++) {
        QString headerName = ui->searchResultsTree->model()->headerData(x, Qt::Horizontal).toString();
        QAction *newAction = new QAction(headerName);
        filterColumnIndex.insert(headerName, x);
        searchByGroup->addAction(newAction);
        newAction->setCheckable(true);
        connect(newAction, &QAction::triggered, [=]() {this->currentSearchByOption = headerName;});
    }
    searchByMenu->addActions(searchByGroup->actions());
    ui->searchByButton->setMenu(searchByMenu);
    this->currentSearchByOption = "All";
}


/*!
 * @brief InventoryView::populateTreeView populate the tree view with the bikes
 * in the database.
 * @param bikeData bikes from the database.
 */
void InventoryView::populateTreeView(QMap<int, BikeData> bikes)
{
    ui->searchResultsTree->clear();
    this->inventory.clear();

    // First we need to create the data structure that determines how many bikes there are.
    for (auto bike : bikes) {
        if (!this->inventory.contains(createInventoryHash(bike))) {
            this->inventory.insert(createInventoryHash(bike), QList<int>());
        }

        this->inventory[createInventoryHash(bike)].append(bike.id);
    }

    // Next we add each of the bike rows
    for (auto bikeModel : this->inventory) {
        if (bikeModel.length() != 0) {
            QStringList row;
            BikeData bikeData = Server::instance()->getCachedBike(bikeModel[0]);

            if ((this->onlyCurrentLocation && bikeData.location.id == s.value("currentLocationId").toInt()) ||
                !this->onlyCurrentLocation) {

                row << QString::number(bikeModel.length())
                    << bikeData.make
                    << bikeData.model
                    << QString::number(bikeData.price)
                    << bikeData.location.nameWithCity();

                CustomTreeWidgetItem *item = new CustomTreeWidgetItem(row);
                item->setData(0, Qt::UserRole, createInventoryHash(bikeData));
                ui->searchResultsTree->insertTopLevelItem(0, item);
            }
        }
    }
}


/*!
 * \brief InventoryView::showAddBikeDialog show the dialog that is used
 * to add a new bike to the inventory.
 */
void InventoryView::showAddBikeDialog()
{
    BikeDialog::Result bikeData = BikeDialog::addBikeDialog(this);

    if (bikeData.submit) {
        BikeData bike;
        bike.location.id = bikeData.locationID;
        bike.model = bikeData.model;
        bike.make = bikeData.make;
        bike.price = bikeData.price;

        Server::instance()->addBike(bike, bikeData.quantity);
    }
}


/*!
 * \brief InventoryView::showEvent update the bikes when the inventory screen is shown.
 * \param event event deatils.
 */
void InventoryView::showEvent(QShowEvent *event)
{
    getBikes();
}


/*!
 * \brief InventoryView::on_searchResultsTree_customContextMenuRequested when the treeview is
 * right clicked show a context menu for that specific bike in the tree.
 * \param pos the position of the click.
 */
void InventoryView::on_searchResultsTree_customContextMenuRequested(const QPoint &pos)
{
    QModelIndex index = ui->searchResultsTree->indexAt(pos);

    if (index.isValid()) {
        this->bikeMenu->exec(ui->searchResultsTree->viewport()->mapToGlobal(pos));
    }
}


/*!
 * \brief InventoryView::removeBike Remove a bike by the ID that is provided.
 * \param id the unique ID of the bike.
 */
void InventoryView::removeBike(int id)
{
    Server *server = Server::instance();

    QJsonObject arguments;
    arguments.insert("id", QString::number(id));
    arguments.insert("auth_token", server->authToken());

    QNetworkReply *reply = server->sendDeleteRequest("bike", arguments);
    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error()) {
            qDebug() << "error";
            return;
        }

        QByteArray data = reply->readAll();
        QJsonDocument json = QJsonDocument::fromJson(data);

        if (json.object()["success"].toBool() == false) {
            qDebug() << "bike not removed";
            return;
        }

        qDebug() << "success";
        emit refreshTree();
    });
}


/*!
 * \brief InventoryView::searchBikes Iterate over the list of bikes and filter them based on
 * the search term and the search by inputs.
 * \param searchTerm QString the text currently in the text box.
 */
void InventoryView::searchBikes(QString searchTerm)
{
    int searchByIndex = this->filterColumnIndex.value(this->currentSearchByOption);
    bool addToTree = false;

    QTreeWidgetItemIterator iterator(ui->searchResultsTree);

    while (*iterator) {
        if (searchByIndex == -1) {
            for (int x = 0; x < ui->searchResultsTree->columnCount(); x++) {
                bool containsSearchTerm = (*iterator)->text(x).contains(searchTerm, Qt::CaseInsensitive);
                addToTree = containsSearchTerm;
                if (containsSearchTerm) break;
            }
        } else
            addToTree = (*iterator)->text(searchByIndex).contains(searchTerm, Qt::CaseInsensitive);

        if (addToTree)
            (*iterator)->setHidden(false);
        else
            (*iterator)->setHidden(true);

        ++iterator;
    }
}


/*!
 * \brief InventoryView::on_searchBar_textChanged Update the search results when text in the
 * search bar has changed.
 * \param text the text currently in the search bar
 */
void InventoryView::on_searchBar_textChanged(const QString &text)
{
    this->searchBikes(text);
}


/*!
 * \brief InventoryView::on_searchButton_clicked update the search results when the search
 * button is clicked.
 */
void InventoryView::on_searchButton_clicked()
{
    this->searchBikes(ui->searchBar->text());
}


/**
 * @brief InventoryView::showBikeInfoDialog shows the bike information dialog.
 * gets the data about the bike and then relays this to the dialog.
 * @param id
 */
void InventoryView::showBikeInfoDialog(int id)
{
    QMetaObject::Connection callback = connect(Server::instance(), &Server::bikeUpdated, this, [this](BikeData bikeData) {
        BikeInfo::addBikeInfoDialog(bikeData, this);
    });

    Server::instance()->requestBike(id);

    disconnect(callback);
}


/**
 * @brief InventoryView::showBikeBookingDialog
 * @param id
 */
void InventoryView::showBikeBookingDialog(int id)
{
    BikeData bikeData = Server::instance()->getCachedBike(id);

    CreateBookingDialog::Result result = CreateBookingDialog::addCreateBookingDialog(bikeData, QStringList(openBookings->keys()), this);

    if (result.submit)
        createNewBooking(bikeData, result.booking);
}


/**
 * @brief InventoryView::createBikeContextMenu create the bike context menu to
 * display actions that can be completed for an individual bike.
 */
void InventoryView::createBikeContextMenu()
{
    bikeMenu = new QMenu(this);
    removeAction = new QAction("Remove Bike");
    bookingAction = new QAction("Add to booking...");
    viewBikeData = new QAction("More Data...");
    bikeMenu->addAction(removeAction);
    bikeMenu->addAction(bookingAction);
    bikeMenu->addAction(viewBikeData);
    ui->searchResultsTree->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->searchResultsTree->setSortingEnabled(true);
}


/**
 * @brief InventoryView::setOpenBookings get the open bookings, this is needed
 * to be able to add a bike to a booking in the add bike dialog.
 * @param openBookings
 */
void InventoryView::setOpenBookings(QMap<QString, BookingPage *> *openBookings)
{
    this->openBookings = openBookings;
}


/**
 * @brief InventoryView::createNewBooking add a bike to a booking page
 * which has been selected.
 * @param bikeData the bike data to add to the booking.
 * @param booking the booking that the bike will be added to.
 */
void InventoryView::createNewBooking(BikeData bikeData, QString booking) {
    // retrieve booking page
    BookingPage *bookingPage = this->openBookings->value(booking);

    if (bookingPage->getLocationId() != bikeData.location.id) {
        QMessageBox::warning(
            this,
            "Bike not at the same location",
            "You cannot add a bike that is not at the location the booking is being made for.",
            QMessageBox::Ok,
            QMessageBox::Ok
        );
        return;
    } else {
        bool availBikeFound = false;
        // Try to find a bike that hasn't already been booked
        for (auto availBike : this->inventory.value(createInventoryHash(bikeData))) {
            if (!bookingPage->getBikes().contains(availBike)) {
                 // Add data to selected booking
                 bookingPage->addBike(availBike);
                 availBikeFound = true;
                 break;
            }
        }

        if (!availBikeFound) {
            QMessageBox::warning(
                this,
                "No more bikes",
                "You have added all of this type of bike to the booking, there are no more bikes left to be added.",
                QMessageBox::Ok,
                QMessageBox::Ok
            );
        }
    }
}


/**
 * @brief InventoryView::createInventoryHash create the hash that is used to find a certain
 * bike in the inventory.
 * @param bikeData the bike data that the string will be made from.
 * @return QString
 */
QString InventoryView::createInventoryHash(BikeData bikeData)
{
    return bikeData.make + bikeData.model + QString::number(bikeData.location.id);
}


/**
 * @brief InventoryView::on_cLocCheckBox_clicked handle toggling of current location only.
 * @param checked whether or not the current location is checked or not.
 */
void InventoryView::on_cLocCheckBox_clicked(bool checked)
{
    ui->searchResultsTree->clear();
    this->onlyCurrentLocation = checked;
    emit refreshTree();
}


/**
 * @brief InventoryView::on_dateTimeEdit_dateTimeChanged
 * @param dateTime
 */
void InventoryView::on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    if (loadedStatus == true)
        getBikes();
}


/**
 * @brief InventoryView::on_dateTimeEdit_2_dateTimeChanged
 * @param dateTime
 */
void InventoryView::on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime)
{
    if (loadedStatus == true)
        getBikes();
}


/**
 * @brief InventoryView::getBikes get all the bikes available between
 * a particular period. If the period is invalid or the period is 0 then
 * show all bikes in the inventory.
 */
void InventoryView::getBikes()
{
    ui->searchResultsTree->clear();

    if (ui->dateTimeEdit->dateTime().toTime_t() == ui->dateTimeEdit_2->dateTime().toTime_t())
        Server::instance()->requestBikes();
    else if (ui->dateTimeEdit->dateTime().toTime_t() < ui->dateTimeEdit_2->dateTime().toTime_t()) {
        Server::instance()->requestAvailableBikes(ui->dateTimeEdit->dateTime(), ui->dateTimeEdit_2->dateTime());
    } else {
        QMessageBox::warning(
            this,
            "Invalid end date",
            "The end date cannot come before the start date",
            QMessageBox::Ok,
            QMessageBox::Ok
        );

        ui->dateTimeEdit_2->setDateTime(ui->dateTimeEdit->dateTime());
        Server::instance()->requestBikes();
        return;
    }
}
