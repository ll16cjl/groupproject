#include "bikeinfo.h"
#include "ui_bikeinfo.h"

/**
 * @brief BikeInfo::BikeInfo create bike info dialog that displays all the information
 * collected about a bike.
 * @param bikeData the data for a single bike.
 * @param parent the parent widget.
 */
BikeInfo::BikeInfo(BikeData bikeData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BikeInfo)
{
    ui->setupUi(this);

    // display data
    ui->makeLabel->setText(bikeData.make);
    ui->modelLabel->setText(bikeData.model);
    ui->priceLabel->setText("£/h: " + QString::number(bikeData.price));
    ui->locationNameLabel->setText(bikeData.location.name);
    ui->locationCityLabel->setText(bikeData.location.city);
    ui->locationLongitudeLabel->setText(QString::number(bikeData.location.longitude));
    ui->locationLatitudeLabel->setText(QString::number(bikeData.location.latitude));
}


/**
 * @brief BikeInfo::~BikeInfo
 */
BikeInfo::~BikeInfo()
{
    delete ui;
}


/**
 * @brief BikeInfo::addBikeInfoDialog add a new bike info dialog and execute it.
 * @param bikeData the data for a single bike.
 * @param parent the parent widget.
 */
void BikeInfo::addBikeInfoDialog(BikeData bikeData, QWidget *parent) {
    std::unique_ptr<BikeInfo> dialog(new BikeInfo(bikeData, parent));
    dialog->exec();
}
