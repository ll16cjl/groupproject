#include "include/locationdialog.h"
#include "ui_locationdialog.h"
#include <memory>

using Result = LocationDialog::Result;


/**
 * @brief LocationDialog::LocationDialog create a new location dialog.
 * @param parent the parent widget.
 */
LocationDialog::LocationDialog(QWidget *parent) : QDialog(parent), ui(new Ui::LocationDialog)
{
    ui->setupUi(this);
    this->setValidators();
}


/**
 * @brief LocationDialog::LocationDialog create a location dialog.
 * @param name the name of the location.
 * @param city the name of the city.
 * @param latitude the latitude of the location.
 * @param longitude the longitude of the location.
 * @param parent the parent widget.
 */
LocationDialog::LocationDialog(QString name, QString city, double latitude, double longitude, QWidget *parent)
    : QDialog(parent), ui(new Ui::LocationDialog)
{
    ui->setupUi(this);
    this->setValidators();
}


/**
 * @brief LocationDialog::setValidators set the validators on the dialog.
 * This makes sure that only a double can be entered for longitude and latitude.
 */
void LocationDialog::setValidators()
{
    ui->latitude->setValidator(new QDoubleValidator(-90, 90, 7));
    ui->longitude->setValidator(new QDoubleValidator(-180, 180, 7));
}


/**
 * @brief LocationDialog::~LocationDialog delete the location dialog.
 */
LocationDialog::~LocationDialog() {
    delete ui;
}


/**
 * @brief LocationDialog::addLocationDialog create a new dialog, execute it
 * then delete it after a result has been retrieved.
 * @param parent the parent widget.
 * @return the result of the location added.
 */
Result LocationDialog::addLocationDialog(QWidget *parent) {
    std::unique_ptr<LocationDialog> dialog(new LocationDialog(parent));
    dialog->setWindowTitle("Add new location");

    bool accepted = dialog->exec();

    Result result = dialog->getResult();
    result.submit = result.submit && accepted;
    return result;
}


/**
 * @brief LocationDialog::updateLocationDialog update the location.
 * @param name the name of the location.
 * @param city the name of the city.
 * @param latitude the latitude of the location.
 * @param longitude the longitude of the location.
 * @param parent the parent widget.
 * @return the result of the location updated.
 */
Result LocationDialog::updateLocationDialog(QString name, QString city, double latitude, double longitude, QWidget *parent) {
    std::unique_ptr<LocationDialog> dialog(new LocationDialog(name, city, latitude, longitude, parent));
    dialog->setWindowTitle("Update location");

    bool accepted = dialog->exec();

    Result result = dialog->getResult();
    result.submit = result.submit && accepted;
    return result;
}


/**
 * @brief LocationDialog::getResult get the result of the inputs in the dialog.
 * @return the result to be submitted.
 */
Result LocationDialog::getResult() const {
    Result result;
    result.name = ui->name->text();
    result.city = ui->city->text();
    result.latitude = ui->latitude->text().toDouble();
    result.longitude = ui->longitude->text().toDouble();
    result.submit = true; // TODO: Location validation?
    return result;
}

