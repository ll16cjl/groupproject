#include "serversetup.h"
#include <iostream>
#include <QEventLoop>
#include <QFile>
#include <QTextStream>

namespace {
    Server *s_serverInstance = nullptr;
}

/**
 * @brief Server::makeInstance make an instance of the Server class.
 * This method is used to create a singleton class that can only be
 * instantiated once.
 * @return the unique pointer to the server class
 */
std::unique_ptr<Server> Server::makeInstance()
{
    if (s_serverInstance) { return nullptr; }
    else {
        auto ptr =  std::unique_ptr<Server>(new Server());
        s_serverInstance = ptr.get(); 
        return ptr;
    }
}


/**
 * @brief Server::instance retrieve the instance of the server class.
 * @return a pointer to the server class in memory.
 */
Server *Server::instance()
{
    return s_serverInstance;
}

/*
 * @brief Constructs a ReceiptData from a network reply by parsing its json.
 * TODO: Figure out how to signal errors!
 */
ReceiptData ReceiptData::fromNetworkReply(QNetworkReply *reply) {
    if (!reply->error()) {
        QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
        if (json.object()["success"].toBool()) {
            ReceiptData data = {};
            data.name = json.object()["name"].toString();

            auto bikeArray = json.object()["bikes"].toArray();  
            for (auto value : bikeArray) {
                auto object = value.toObject();

                ReceiptData::Bike bike = {};
                bike.make = object["make"].toString();
                bike.model = object["model"].toString();
                bike.price = object["price"].toInt();
                data.bikes.append(bike);
            } 

            data.locationCity = json.object()["location_city"].toString();
            data.locationName = json.object()["location_name"].toString();
            data.cost = json.object()["cost"].toDouble();
            data.discount = json.object()["discount"].toDouble();
            data.total = data.cost;
            data.start = static_cast<qint64>(json.object()["start"].toInt());
            data.end = static_cast<qint64>(json.object()["end"].toInt());
            data.digits = json.object()["digits"].toString();

            return data;
        } 
    }

    return {};
}


/**
 * @brief Server::Server Server class constructor. Create a new instance
 * of the network access manager which we will then create a wrapper around.
 */
Server::Server()
{
    manager = new QNetworkAccessManager();
    setAuthToken("");
}


/**
 * @brief Server::buildParams build all the params that are needed for a
 * specific endpoint. Also adds support for json arrays.
 * @param arguments the params to be provided.
 * @return A url query.
 */
QUrlQuery Server::buildParams(QJsonObject arguments)
{
    QUrlQuery params;
    for (QString const& key : arguments.keys()) {
        QJsonValue value = arguments.value(key);
        if (value.isArray()) {
            QString paramStr = "[";

            QJsonArray array = value.toArray();
            for (int i = 0; i < array.size(); ++i) {
                QJsonValue const& value = array.at(i);
                if (i > 0) {
                    paramStr += ",";
                }
                paramStr += value.toString();
            }

            paramStr += "]";
            params.addQueryItem(key, paramStr);
        }
        else {
            params.addQueryItem(key, value.toString());
        }
    }

    return params;
}


/**
 * @brief Server::sendGetRequest send a new get request to the server given
 * the endpoint name and the data to be sent.
 * @param endpoint name of the endpoint.
 * @param arguments the arguments that will be sent with the request.
 * @return a network reply object that can be used to retrieve the reply data.
 */
QNetworkReply* Server::sendGetRequest(QString endpoint, QJsonObject arguments)
{
    QString port = "";

    if (s.value("defaultServerPort").toString() != "")
        port = ":" + s.value("defaultServerPort").toString();

    // create request with no parameters
    QString requestURL = "http://" + s.value("defaultServerIP").toString() +
            port +
            "/" + endpoint;

    QUrlQuery params = buildParams(arguments);

    if (LOG_REQUESTS)
        qDebug() << requestURL << "?" << params.toString();

    QNetworkRequest *request = new QNetworkRequest(QUrl(requestURL + "?" + params.toString()));
    return this->manager->get(*request);
}


/**
 * @brief Server::sendPostRequest send a new post request to the server given
 * the endpoint name and the data to be sent.
 * @param endpoint name of the endpoint.
 * @param arguments the arguments that will be sent with the request.
 * @return a network reply object that can be used to retrieve the reply data.
 */
QNetworkReply* Server::sendPostRequest(QString endpoint, QJsonObject arguments)
{
    QString port = "";

    if (s.value("defaultServerPort").toString() != "")
        port = ":" + s.value("defaultServerPort").toString();

    // create request with no parameters
    QString requestURL = "http://" + s.value("defaultServerIP").toString() +
            port +
            "/" + endpoint;

    QNetworkRequest *request = new QNetworkRequest(QUrl(requestURL));
    request->setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));

    QUrlQuery params = buildParams(arguments);

    QByteArray requestArguments;

    if(LOG_REQUESTS)
        qDebug() << requestURL << "?" << params.toString();

    requestArguments.append(params.toString());
    return this->manager->post(*request, requestArguments);
}


/**
 * @brief Server::sendExternalGetRequest make a request to an external source that is not related
 * the applications server.
 * @param endpoint the address of the endpoint
 * @param arguments the arguments to send with the endpoint
 * @return a network reply object that can be used to retrieve the reply data.
 */
QNetworkReply* Server::sendExternalGetRequest(QString endpoint, QJsonObject arguments)
{
    QUrlQuery params = buildParams(arguments);

    QNetworkRequest *request = new QNetworkRequest(QUrl(endpoint + "?" + params.toString()));
    return this->manager->get(*request);
}


/**
 * @brief Server::sendDeleteRequest send a new delete request to the server given
 * the endpoint name and the data to be sent.
 * @param endpoint name of the endpoint.
 * @param arguments the arguments that will be sent with the request.
 * @return a network reply object that can be used to retrieve the reply data.
 */
QNetworkReply* Server::sendDeleteRequest(QString endpoint, QJsonObject arguments)
{
    // create request with no parameters
    QString requestURL = "http://" + s.value("defaultServerIP").toString() +
            ":" + s.value("defaultServerPort").toString() +
            "/" + endpoint;

    QUrlQuery params = buildParams(arguments);

    if (LOG_REQUESTS)
        qDebug() << requestURL << "?" << params.toString();

    QNetworkRequest *request = new QNetworkRequest(QUrl(requestURL + "?" + params.toString()));
    return this->manager->deleteResource(*request);
}


/**
 * @brief Server::sendPutRequest send a new put request to the server given
 * the endpoint name and the data to be sent.
 * @param endpoint name of the endpoint.
 * @param arguments the arguments that will be sent with the request.
 * @return a network reply object that can be used to retrieve the reply data.
 */
QNetworkReply* Server::sendPutRequest(QString endpoint, QJsonObject arguments)
{
    // create request with no parameters
    QString requestURL = "http://" + s.value("defaultServerIP").toString() +
            ":" + s.value("defaultServerPort").toString() +
            "/" + endpoint;

    QNetworkRequest *request = new QNetworkRequest(QUrl(requestURL));

    QUrlQuery params = buildParams(arguments);

    QByteArray requestArguments;
    requestArguments.append(params.toString());

    if (LOG_REQUESTS)
        qDebug() << requestURL << "?" << params.toString();

    return this->manager->put(*request, requestArguments);
}


/**
 * @brief Server::requestLocationWithReply request a location and return the reply.
 * This will then be used to perform an action upon recieving the data.
 * @param locationId the ID of the location that is to be fetched.
 * @return a network reply object.
 */
QNetworkReply *Server::requestLocationWithReply(int locationId)
{
    QJsonObject arguments;
    arguments.insert("id", QString::number(locationId));
    arguments.insert("auth_token", authToken());
    return sendGetRequest("location", arguments);
}


/**
 * @brief Server::requestLocation request a location, update the
 * location cache and emit a signal stating that the request has
 * successfully finished.
 * @param locationId the id of the requested location.
 */
void Server::requestLocation(int locationId)
{
    QNetworkReply *request = requestLocationWithReply(locationId);
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            LocationData location;
            location.name = json["name"].toString();
            location.city = json["city"].toString();
            location.latitude = json["latitude"].toDouble();
            location.longitude = json["longitude"].toDouble();
            location.id = json["id"].toInt();

            this->cachedLocations.insert(location.id, location);
            emit locationUpdated(location);
        } else {
            handleServerError(json.object()["error"].toInt());
            emit locationUpdated(LocationData());
        }
    });
}


/**
 * @brief Server::requestLocationsWithReply request all locations and
 * return the reply.
 * @return a network reply object.
 */
QNetworkReply *Server::requestLocationsWithReply()
{
    QJsonObject arguments;
    arguments.insert("id", "*");
    arguments.insert("auth_token", authToken());
    return sendGetRequest("location", arguments);
}


/**
 * @brief Server::requestLocations request all the locations, update all
 * the location cache and emit a signal stating that the request has finished
 * and the data can be interpreted and read.
 */
void Server::requestLocations()
{
    QNetworkReply *request = requestLocationsWithReply();
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) { 
            this->cachedLocations.clear();

            QJsonArray locationsJson = json.object()["locations"].toArray();
            for (auto val : locationsJson) {
                QJsonObject locationJson = val.toObject();
                
                LocationData location;
                location.name = locationJson["name"].toString();
                location.city = locationJson["city"].toString();
                location.latitude = locationJson["latitude"].toDouble();
                location.longitude = locationJson["longitude"].toDouble();
                location.id = locationJson["id"].toInt();
                this->cachedLocations.insert(location.id, location);
            }
            emit locationsUpdated(this->cachedLocations);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::addLocation add a location to the database request.
 * @param name the name of the location.
 * @param city the city the location is in.
 * @param longitude
 * @param latitude
 */
void Server::addLocation(QString name, QString city, double longitude, double latitude)
{
    QJsonObject arguments;
    arguments.insert("name", name);
    arguments.insert("city", city);
    arguments.insert("longitude", QString::number(longitude));
    arguments.insert("latitude", QString::number(latitude));
    arguments.insert("auth_token", authToken());

    QNetworkReply *request = sendPostRequest("location", arguments);
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            LocationData location;
            location.id = json.object()["id"].toInt();
            location.name = name;
            location.city = city;
            location.longitude = longitude;
            location.latitude = latitude;

            this->cachedLocations.insert(location.id, location);

            emit locationsUpdated(this->cachedLocations);
            emit locationAdded(location);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::requestBikesWithReply request all bikes from the database
 * and return a network reply.
 * @return a network reply object.
 */
QNetworkReply *Server::requestBikesWithReply()
{
    QJsonObject arguments;
    arguments.insert("id", "*");
    arguments.insert("auth_token", authToken());
    return sendGetRequest("bike", arguments);
}


/**
 * @brief Server::requestBikes request all bikes, update the bikes cache and
 * emit a signal stating that the request has finished and the data can be
 * retrieved. The function also get the relevent location data if needed.
 */
void Server::requestBikes()
{
    QNetworkReply *request = requestBikesWithReply();

    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) { 
            this->cachedBikes.clear();

            QJsonArray bikesJson = json.object()["bikes"].toArray();
            for (auto val : bikesJson) {
                QJsonObject bikesJson = val.toObject();
                
                BikeData *bike = new BikeData();
                bike->id = bikesJson["id"].toInt();
                bike->model = bikesJson["model"].toString();
                bike->make = bikesJson["make"].toString();
                bike->price = bikesJson["price"].toInt() / 100.0;
                bike->location.id = bikesJson["location_id"].toInt();

                bike->location = getCachedLocation(bike->location.id);
                this->cachedBikes.insert(bike->id, *bike);
            }
            emit bikesUpdated(this->cachedBikes);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::requestBikesWithReply request a bike from the database
 * and return a network reply.
 * @param bikeId the bike id to request.
 * @return a network reply object.
 */
QNetworkReply *Server::requestBikeWithReply(int bikeId)
{
    QJsonObject arguments;
    arguments.insert("id", QString::number(bikeId));
    arguments.insert("auth_token", authToken());
    return sendGetRequest("bike", arguments);
}


/**
 * @brief Server::requestBike request bike, update the bikes cache and
 * emit a signal stating that the request has finished and the data can be
 * retrieved. The function also get the relevent location data if needed.
 * @param bikeId the bike to be requested.
 */
void Server::requestBike(int bikeId)
{
    // first check cache for bike
    if (this->cachedBikes.contains(bikeId)) {
        emit bikeUpdated(this->cachedBikes.value(bikeId));
        return;
    }

    // if not in cache make network request
    QNetworkReply *request = requestBikeWithReply(bikeId);
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            BikeData *bike = new BikeData();
            bike->id = json.object()["id"].toInt();
            bike->make = json.object()["make"].toString();
            bike->model = json.object()["model"].toString();
            bike->price = json.object()["price"].toInt() / 100.0;
            bike->location.id = json.object()["location_id"].toInt();

            bike->location = getCachedLocation(bike->location.id);
            emit bikeUpdated(*bike);
        } else {
            handleServerError(json.object()["error"].toInt());
            emit bikeUpdated(BikeData());
        }
    });
}


/**
 * @brief Server::addBike send a request to add a new bike to the
 * database.
 * @param data the bike data to add.
 */
void Server::addBike(BikeData data, int quantity)
{
    QJsonObject arguments;
    arguments.insert("location_id", QString::number(data.location.id));
    arguments.insert("model", data.model);
    arguments.insert("make", data.make);
    arguments.insert("price", QString::number(int(data.price * 100)));
    arguments.insert("category_id", QString::number(1));
    arguments.insert("quantity", QString::number(quantity));
    arguments.insert("auth_token", authToken());

    QNetworkReply *reply = sendPostRequest("bike", arguments);
    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(reply->readAll());

        bool successful = json.object()["success"].toBool();
        if (successful) {
            BikeData bikeData;

            if (quantity == 1) {
                 addSingleBike(json.object()["id"].toInt(), data);
            } else {
                QJsonArray bikes = json.object()["ids"].toArray();

                for (auto bike : bikes)
                    addSingleBike(bike.toInt(), data);
            }

            emit bikesUpdated(this->cachedBikes);
            emit bikeAdded(bikeData);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}

BikeData Server::addSingleBike(int id, BikeData data) {
    BikeData bikeData;
    bikeData.id = id;
    bikeData.model = data.model;
    bikeData.make = data.make;
    bikeData.price = data.price;
    bikeData.location = this->getCachedLocation(data.location.id);

    this->cachedBikes.insert(bikeData.id, bikeData);
    return bikeData;
}


/**
 * @brief Server::addBooking submit a booking to the server.
 * @param data the data to be submitted.
 */
void Server::addBooking(BookingData data)
{
    QJsonObject arguments;
    arguments.insert("location_id", QString::number(data.location.id));

    QJsonArray bikes;
    for (BikeData const& bike : data.bikes) {
        bikes.append(QString::number(bike.id));
    }

    arguments.insert("bike_ids", bikes);
    arguments.insert("start_time", QString::number(data.start));
    arguments.insert("end_time", QString::number(data.end));
    arguments.insert("auth_token", authToken());
    arguments.insert("payment_id", QString::number(data.paymentId));
    arguments.insert("billing_id", QString::number(data.billing.id));

    QNetworkReply *reply = sendPostRequest("rental", arguments);
    connect(reply, &QNetworkReply::finished, [=]() {
        QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
        if (reply->error()) { handleRequestError(); }

        if (json.object()["success"].toBool()) {
            BookingData booking = data;
            booking.id = json.object()["id"].toInt();

            this->cachedBookings.insert(booking.id, booking);

            emit bookingUpdated(booking);
            emit bookingAdded(booking);
        } else {
            emit bookingError(json.object()["error"].toInt());
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::requestBookingWithReply request a booking from the database
 * and return a network reply.
 * @return a network reply object.
 */
QNetworkReply *Server::requestBookingsWithReply()
{
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    return sendGetRequest("rental", arguments);
}

/**
 * @brief Server::requestBookings request all bookings from the server,
 * matching the relevent data like bikes with the cache and emit a signal
 * stating that the booking data has been retrieved.
 */
void Server::requestBookings()
{
    QNetworkReply *request = requestBookingsWithReply();
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            this->cachedBookings.clear();

            QJsonArray bookingsJson = json.object()["reservations"].toArray();
            for (auto val : bookingsJson) {
                QJsonObject bookingJson = val.toObject();

                BookingData *booking = new BookingData();
                booking->id = bookingJson["id"].toInt();
                booking->start = bookingJson["start_time"].toVariant().toUInt();
                booking->end = bookingJson["end_time"].toVariant().toUInt();
                booking->reservationTime = bookingJson["reservation_time"].toVariant().toUInt();
                booking->location.id = bookingJson["location_id"].toInt();
                booking->user.id = bookingJson["user_id"].toInt();

                // get each individual bike
                for (auto bike : bookingJson["bikes_ids"].toArray()) {
                    BikeData newBike = getCachedBike(bike.toInt());
                    booking->bikes.insert(newBike.id, newBike);
                }

                // get the location, user and billing data.
                booking->location = getCachedLocation(booking->location.id);
                booking->user = getCachedUser(booking->user.id);
                booking->billing = getCachedBilling(booking->billing.id);
                this->cachedBookings.insert(booking->id, *booking);
            }
            emit bookingsUpdated(this->cachedBookings);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::addBillingAddress send a billing address to the server
 * for verification. This is saved in cache as it will be needed for the payment data.
 * @param data the billing address data.
 */
void Server::addBillingAddress(BillingAddressData data)
{
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    arguments.insert("post_code", data.postCode);
    arguments.insert("first_line", data.addressLine1);

    // if the cache is gets too big clear it.
    if (cachedBillingData.size() >= 30) {
        cachedBillingData.clear();
    }

    QNetworkReply *reply = sendPostRequest("billing", arguments);
    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error()) { return; }

        QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
        if (json.object()["success"].toBool()) {
            BillingAddressData billing;
            billing.id = json.object()["id"].toInt();
            billing.postCode = data.postCode;
            billing.addressLine1 = data.addressLine1;

            cachedBillingData.insert(billing.id, billing);
            emit billingAddressUpdated(billing);
            emit billingAddressAdded(billing);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}


/**
 * @brief Server::getCachedBike get a bike by first checking whether it is
 * in the cache and then whether it is on the server. This method is used when
 * seconary data needs to be fetched. E.g. a booking requires previously known
 * data about a bike and a location.
 * @param bikeId the bike id that needs fetching.
 * @return BikeData
 */
BikeData Server::getCachedBike(int bikeId)
{
    // check if bike is in cache
    bool bikeFound = false;
    for (auto bike : this->cachedBikes) {
        bikeFound = bike.id == bikeId;
        if (bikeFound) {
            return bike;
        }
    }

    // if not, revert to requesting it from the server.
    if (!bikeFound) {
        // create blocking network request to find the location not in cache
        QEventLoop loop;
        QMetaObject::Connection callback = connect(this, &Server::bikeUpdated, this, [this, &loop](BikeData bikeData) {
            loop.quit();
            this->cachedBikes.insert(bikeData.id, bikeData);
        });

        requestBike(bikeId);
        loop.exec();
        disconnect(callback);

        return this->cachedBikes.value(bikeId);
    }

    // bike not found
    return BikeData();
}


/**
 * @brief Server::getCachedLocation get a location by first checking whether it is
 * in the cache and then whether it is on the server. This method is used when
 * seconary data needs to be fetched. E.g. a booking requires previously known
 * data about a bike and a location.
 * @param locationId the location id that needs fetching.
 * @return LocationData
 */
LocationData Server::getCachedLocation(int locationId)
{
    // check if location is in cache
    bool locationFound = false;
    for (auto location : this->cachedLocations) {
        locationFound = location.id == locationId;
        if (locationFound) {
            return location;
        }
    }

    // if not, revert to requesting it from the server.
    if (!locationFound) {
        // create blocking network request to find the location not in cache
        QEventLoop loop;
        QMetaObject::Connection callback = connect(this, &Server::locationUpdated, this, [this, &loop](LocationData location) {
            loop.quit();
            this->cachedLocations.insert(location.id, location);
        });

        requestLocation(locationId);
        loop.exec();
        disconnect(callback);

        return this->cachedLocations.value(locationId);
    }

    return LocationData();
}


/**
 * @brief Server::getCachedUser get a user by first checking whether it is
 * in the cache and then whether it is on the server. This method is used when
 * seconary data needs to be fetched. E.g. a booking requires previously known
 * data about a bike and a location.
 * @param userId the user id that needs fetching.
 * @return userData
 */
UserData Server::getCachedUser(int userId)
{
    // check if user is in cache
    bool userFound = false;
    for (auto user : this->cachedUsers) {
        userFound = user.id == userId;
        if (userFound) {
            return user;
        }
    }

    // if not, revert to requesting it from the server.
    if (!userFound) {
        // create blocking network request to find the location not in cache
        QEventLoop loop;
        QMetaObject::Connection callback = connect(this, &Server::userUpdated, this, [this, &loop](UserData user) {
            loop.quit();
            this->cachedUsers.insert(user.id, user);
        });

        requestUser(userId);
        loop.exec();
        disconnect(callback);

        return this->cachedUsers.value(userId);
    }

    // user not found
    return UserData();
}


/**
 * @brief Server::getCachedBilling get a billing address by first checking whether it is
 * in the cache and then whether it is on the server. This method is used when
 * seconary data needs to be fetched. E.g. a booking requires previously known
 * data about a bike and a location.
 * @param billingId the billing id to be fetched.
 * @return billing address data.
 */
BillingAddressData Server::getCachedBilling(int billingId)
{
    // check if billing address is in cache
    bool billingFound = false;
    for (auto billing : this->cachedBillingData) {
        billingFound = billing.id == billingId;
        if (billingFound) {
            return billing;
        }
    }

    // if not, revert to requesting it from the server.
    if (!billingFound) {
        // create blocking network request to find the location not in cache
        QEventLoop loop;
        QMetaObject::Connection callback = connect(this, &Server::billingAddressUpdated, this, [this, &loop](BillingAddressData billing) {
            loop.quit();
            this->cachedBillingData.insert(billing.id, billing);
        });

        requestBillingAddress(billingId);
        loop.exec();
        disconnect(callback);

        return this->cachedBillingData.value(billingId);
    }

    // user not found
    return BillingAddressData();
}


/**
 * @brief Server::requestUser request a user from the server. Once retrieved emit a
 * signal stating that the user has been retrieved.
 * @param userId the user id to be retrieved.
 */
void Server::requestUser(int userId)
{
    QNetworkReply *request = requestUserWithReply(userId);
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            UserData user;
            user.id = json["id"].toInt();
            user.first = json["first_name"].toString();
            user.last = json["second_name"].toString();
            user.email = json["email"].toString();

            this->cachedUsers.insert(user.id, user);
            emit userUpdated(user);
        } else {
            handleServerError(json.object()["error"].toInt());
            emit userUpdated(UserData());
        }
    });
}


/**
 * @brief Server::requestUserWithReply request a user and return a network reply.
 * @param userId the user id to be retrieved.
 * @return a network reply object.
 */
QNetworkReply *Server::requestUserWithReply(int userId)
{
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    arguments.insert("id", QString::number(userId));
    return sendGetRequest("user/info", arguments);
}


/**
 * @brief Server::requestCurrentUser request the current user from the server. Once retrieved emit a
 * signal stating that the user has been retrieved.
 * @param userId the user id to be retrieved.
 */
void Server::requestCurrentUser()
{
    QNetworkReply *request = requestCurrentUserWithReply();
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());

        if (json.object()["success"].toBool()) {
            UserData user;
            user.id = json["id"].toInt();
            user.first = json["first_name"].toString();
            user.last = json["second_name"].toString();
            user.email = json["email"].toString();

            this->cachedUsers.insert(user.id, user);
            emit currentUserUpdated(user);
        } else {
            handleServerError(json.object()["error"].toInt());
            emit currentUserUpdated(UserData());
        }
    });
}


/**
 * @brief Server::requestCurrentUserWithReply request the current user and return a network reply.
 * @param userId the user id to be retrieved.
 * @return a network reply object.
 */
QNetworkReply *Server::requestCurrentUserWithReply()
{
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    return sendGetRequest("user/info", arguments);
}



/**
 * @brief Server::requestBillingAddress Request a billing address from the server.
 * Once recieved emit a signal.
 * @param billingId the id of the billing item.
 */
void Server::requestBillingAddress(int billingId)
{
    QNetworkReply *request = requestBillingAddressWithReply(billingId);
    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());
        if (json.object()["success"].toBool()) {
            BillingAddressData billing;
            billing.id = json["id"].toInt();
            billing.postCode = json["post_code"].toString();
            billing.addressLine1 = json["address_line"].toString();

            this->cachedBillingData.insert(billing.id, billing);
            emit billingAddressUpdated(billing);
        } else {
            handleServerError(json.object()["error"].toInt());
            emit billingAddressUpdated(BillingAddressData());
        }
    });
}


/**
 * @brief Server::requestBillingAddressWithReply request a billing address and return a
 * server reply.
 * @param billingId the id of the billing
 * @return
 */
QNetworkReply *Server::requestBillingAddressWithReply(int billingId)
{
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    arguments.insert("id", billingId);
    return sendGetRequest("billing", arguments);
}


/**
 * @brief Server::requestAvailableBikesWithReply request all bikes from the database
 * and return a network reply.
 * @param start time
 * @param end time
 * @return a network reply object.
 *
 */
QNetworkReply *Server::requestAvailableBikesWithReply(QDateTime start, QDateTime end)
{
    QJsonObject arguments;
    arguments.insert("location_id", "*");
    arguments.insert("start_time", QString::number(start.toSecsSinceEpoch()));
    arguments.insert("end_time", QString::number(end.toSecsSinceEpoch()));
    arguments.insert("auth_token", authToken());
    return sendGetRequest("bike/avail", arguments);
}


/**
 * @brief Server::requestAvailableBikes request all bikes, update the bikes cache and
 * emit a signal stating that the request has finished and the data can be
 * retrieved. The function also get the relevent location data if needed.
 * @param start time
 * @param end time
 */
void Server::requestAvailableBikes(QDateTime start, QDateTime end)
{
    QNetworkReply *request = requestAvailableBikesWithReply(start, end);

    connect(request, &QNetworkReply::finished, [=]() {
        if (request->error()) { handleRequestError(); }

        QJsonDocument json = QJsonDocument::fromJson(request->readAll());

        if (json.object()["success"].toBool()) {
            QJsonArray bikesJson = json.object()["bikes"].toArray();
            this->cachedAvailableBikes.clear();

            for (auto val : bikesJson) {
                BikeData bike = this->getCachedBike(val.toInt());
                this->cachedAvailableBikes.insert(bike.id, bike);
            }

            emit bikesUpdated(this->cachedAvailableBikes);
        } else {
            handleServerError(json.object()["error"].toInt());
        }
    });
}

/**
 * @brief Request receipt data for specific booking ID.
 * @param booking ID
 */
QNetworkReply *Server::requestReceiptWithReply(int bookingID) {
    QJsonObject arguments;
    arguments.insert("auth_token", authToken());
    arguments.insert("id", QString::number(bookingID));
    return sendGetRequest("receipt", arguments);
}

/**
 * @brief Server::handleServerError display a nice error message rather
 * than a server error code.
 * @param errorCode int.
 */
void Server::handleServerError(int errorCode)
{
    QString message;

    switch (errorCode) {
        case 0:
            message = "Missing parameters in user registration";
            break;
        case 1:
            message = "Email already in use when attempting to register new user";
            break;
        case 2:
            message = "Could not find payment method with given ID in database";
            break;
        case 3:
            message = "Could not find payment method with given ID in database";
            break;
        case 4:
            message = "Payment method with given ID does not exist";
            break;
        case 5:
            message = "Location with given ID does not exist";
            break;
        case 6:
            message = "Billing address with given ID does not exist";
            break;
        case 7:
            message = "Bike with given ID does not exist in database";
            break;
        case 8:
            message = "Could not find user in database";
            break;
        case 9:
            message = "Missing fields in request";
            break;
        case 10:
            message = "Invalid authentication token";
            break;
        case 20:
            message = "Cannot change email as it is already in use";
            break;
        case 100:
            message = "Database error";
            break;
        case 1000:
            message = "Tried to access staff only resource as normal user";
            break;
        case 3000:
            message = "Attempted to use payment method ID the user does not own";
            break;
        case 3001:
            message = "Attempted to use billing address ID the user does not own";
            break;
        case 4000:
            message = "End time is before start time of rental";
            break;
        case 4001:
            message = "Start time is in the past";
            break;
        case 10001:
            message = "Tried to rent bikes that are no longer available in given time period at given location";
            break;
        case 10002:
            message = "Invalid registration details (validation error)";
            break;
        case 10003:
            message = "Could not find any rentals for given user";
            break;
        default:
            message = "An unxplained server error occured";
    }

    qDebug() << "An internal server error occured, " << message;
}


/**
 * @brief Server::handleRequestError Handle the case when a connection
 * error occured to the server.
 */
void Server::handleRequestError()
{
    qDebug() << "A connection to the server could not be established";
}
