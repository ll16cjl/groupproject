#include "bikedialog.h"
#include "ui_bikedialog.h"
#include "serversetup.h"
#include "locationdialog.h"
#include <memory>
#include <QMessageBox>


/**
 * @brief BikeDialog::BikeDialog create a new bike dialog that will be used to add a new
 * bike to the inventory view.
 * @param parent the parent widget.
 */
BikeDialog::BikeDialog(QWidget *parent) : QDialog(parent), ui(new Ui::BikeDialog) {
    ui->setupUi(this);

    connect(Server::instance(), &Server::locationsUpdated, this, &BikeDialog::populateLocationComboBox);
    Server::instance()->requestLocations();

    connect(Server::instance(), &Server::locationAdded, this, &BikeDialog::locationAdded);

    connect(ui->locationID, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &BikeDialog::locationIDChanged);
}


/**
 * @brief BikeDialog::BikeDialog create a new bike dialog that will be used to add a new
 * bike to the inventory view.
 * @param locationID the id of the location that will be added.
 * @param model the model of the bike.
 * @param make the make of the bike.
 * @param price the price of the bike per hour
 * @param parent the parent widget.
 */
BikeDialog::BikeDialog(int locationID, QString model, QString make, int price, QWidget *parent) 
    : QDialog(parent), ui(new Ui::BikeDialog) {
    ui->setupUi(this);

    connect(Server::instance(), &Server::locationsUpdated, this, &BikeDialog::populateLocationComboBox);
    Server::instance()->requestLocations();

    connect(Server::instance(), &Server::locationAdded, this, &BikeDialog::locationAdded);

    connect(ui->locationID, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &BikeDialog::locationIDChanged);

    ui->model->setText(model);
    ui->make->setText(make);
    ui->price->setValue(price);
}


/**
 * @brief BikeDialog::~BikeDialog delete the bike dialog.
 */
BikeDialog::~BikeDialog() {
    delete ui;
}


/**
 * @brief BikeDialog::addBikeDialog create a bike dialog and delete it
 * once the data has been submitted.
 * @param parent the parent widget.
 * @return the result.
 */
BikeDialog::Result BikeDialog::addBikeDialog(QWidget *parent) {
    std::unique_ptr<BikeDialog> dialog(new BikeDialog(parent));
    dialog->setWindowTitle("Add new bike");

    bool accepted = dialog->exec();

    auto result = dialog->getResult();
    result.submit = result.submit && accepted;
    return result;
}


/**
 * @brief BikeDialog::updateBikeDialog create a bike dialog and delete it
 * once the data has been submitted.
 * @param locationID the locationId of the bike.
 * @param model the model of the bike.
 * @param make the make of the bike.
 * @param price the price per hour of the bike.
 * @param parent the parent widget.
 * @return the result.
 */
BikeDialog::Result BikeDialog::updateBikeDialog(int locationID, QString model, QString make, int price, QWidget *parent) {
    std::unique_ptr<BikeDialog> dialog(new BikeDialog(locationID, model, make, price, parent));
    dialog->setWindowTitle("Update bike");

    bool accepted = dialog->exec();

    auto result = dialog->getResult();
    result.submit = result.submit && accepted;
    return result;
}


/**
 * @brief BikeDialog::getResult get the data input into the
 * dialog and return the result.
 * @return the result.
 */
BikeDialog::Result BikeDialog::getResult() {
    BikeDialog::Result result;
    result.locationID = ui->locationID->currentData().toInt();
    result.model = ui->model->text();
    result.make = ui->make->text();
    result.price = ui->price->value();
    result.quantity = ui->quantitySpinBox->value();
    // TODO: Only set this to true if the values were changed from the defaults. (We don't want to always update
    // existing bikes.)
    result.submit = result.locationID >= 0; 
    return result;
}


/**
 * @brief BikeDialog::accept when the dialog is accepted make sure that
 * no fields are empty and then accept. If they are output an error.
 */
void BikeDialog::accept() {
    bool hasEmptyFields = ui->model->text().size() == 0 &&
                          ui->make->text().size() == 0;

    if (hasEmptyFields) {
        QMessageBox::critical(this, "Empty fields", "Cannot add bike. Some of the fields are empty!");
    }
    else { 
        QDialog::accept(); 
    }
}


/**
 * @brief BikeDialog::reject if the dialog is rejected then close.
 */
void BikeDialog::reject() {
    // TODO: Check if the fields have been modified and warn the user before closing.
    QDialog::reject();
}


/**
 * @brief BikeDialog::populateLocationComboBox populate the combo box with the locations
 * that can be selected.
 * @param locations all the locations.
 */
void BikeDialog::populateLocationComboBox(QMap<int, LocationData> locations) {
    ui->locationID->clear();

    for (LocationData data : locations) {
        ui->locationID->addItem(data.nameWithCity(), QVariant(data.id));

        if (data.id == s.value("currentLocationId").toInt())
            ui->locationID->setCurrentText(data.nameWithCity());
    }

    /*
     * Special option for adding a new location.
     */
    ui->locationID->addItem("New location...", QVariant(NEW_LOCATION_OPTION));
}


/**
 * @brief BikeDialog::locationAdded if a location is added, add it to the
 * combo box.
 * @param data the location data added.
 */
void BikeDialog::locationAdded(LocationData data) {
    int index = [=]() {
        for (int index = 0; index < ui->locationID->count(); ++index) {
            if (ui->locationID->itemData(index).toInt() == data.id) { 
                return index; 
            }
        }
        /*
         * If we could not find an item with the same index,
         * return -1 so we select nothing at all.
         */
        return -1;
    }();

    ui->locationID->setCurrentIndex(index);
}


/**
 * @brief BikeDialog::locationIDChanged when the location id has changed
 * set the current index of the combo box.
 * @param id the location id.
 */
void BikeDialog::locationIDChanged(int id) {
    if (ui->locationID->currentData().toInt() == NEW_LOCATION_OPTION) {
        ui->locationID->setCurrentIndex(-1);

        auto result = LocationDialog::addLocationDialog(this);
        if (result.submit) {
            Server::instance()->addLocation(result.name, result.city, result.longitude, result.latitude);
        }
    }
}
