#include "bookingpage.h"
#include "ui_bookingpage.h"

#include <QMessageBox>
#include <QDebug>


/**
 * @brief BookingPage::BookingPage create a booking page that will be used to process a booking
 * The user will be able to change the following tabs:
 * 1. Order
 * 2. Customer
 * 3. Payment
 * 4. Summary
 * @param parent the parent widget.
 */
BookingPage::BookingPage(QWidget *parent) : QWidget(parent), ui(new Ui::BookingPage) {
    ui->setupUi(this);
    this->customerInfo = new QMap<QString, QString>();
    this->paymentInfo = new QMap<QString, QString>();
    this->totalPricePerHour = 0;
    this->inventory = QMap<QString, QList<int>>();

    ui->discount->setStyleSheet("color: #f97f66;");
    ui->discount_2->setStyleSheet("color: #f97f66;");
    ui->tabWidget->setTabEnabled(4, false);

    QDateTime current = QDateTime::currentDateTime();
    ui->endTimeEdit->setDateTime(QDateTime::currentDateTime());
    ui->startTimeEdit->setDateTime(QDateTime::currentDateTime());

    connect(Server::instance(), &Server::locationsUpdated, this, [this](QMap<int, LocationData> locationData) {
        ui->locationSelect->clear();
        for (auto location : locationData) {
            ui->locationSelect->addItem(location.nameWithCity(), location.id);

            if (location.id == s.value("currentLocationId").toInt()) {
                locationId = location.id;
                ui->locationSelect->setCurrentText(location.nameWithCity());
                ui->locationSelect->setItemData(0, locationId, Qt::UserRole);
            }
        }
    });
    populateLocations();

    connect(this, &BookingPage::updateSubtotal, this, &BookingPage::setSubtotal);

    connect(ui->pushButton_4, &QPushButton::clicked, this, [=]{
        QTreeWidgetItem *currentItem = ui->bikeTreeWidget->currentItem();

        QList<int> bikes = this->inventory.value(currentItem->data(0, Qt::UserRole).toString());
        if (!bikes.empty()) {
            BikeData bikeData = Server::instance()->getCachedBike(bikes[0]);
            showBikeInfoDialog(bikeData.id);
        }
    });

    connect(Server::instance(), &Server::bookingError, this, &BookingPage::handleBookingError);


    // Field validation
    QRegExp phoneRe("^[0-9]{11}$");
    QRegExpValidator *phoneValidator = new QRegExpValidator(phoneRe, this);
    ui->phoneEdit->setValidator(phoneValidator);

    // Email validation
    // regex from: https://stackoverflow.com/questions/9101887/how-to-validate-email-address-using-qregexp/13800414
    QRegExp emailRe("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    emailRe.setCaseSensitivity(Qt::CaseInsensitive);
    emailRe.setPatternSyntax(QRegExp::RegExp);
    QRegExpValidator *emailValidator = new QRegExpValidator(emailRe, this);
    ui->emailEdit->setValidator(emailValidator);

    // Name validation
    QRegExp nameRe("^[A-Za-z\\s]+$");
    QRegExpValidator *nameValidator = new QRegExpValidator(nameRe, this);
    ui->firstNameEdit->setValidator(nameValidator);
    ui->lastNameEdit->setValidator(nameValidator);
    ui->nameEdit->setValidator(nameValidator);

    // Card number
    QRegExp cardRe("^[0-9]{16}$");
    QRegExpValidator *cardValidator = new QRegExpValidator(cardRe, this);
    ui->cardNoEdit->setValidator(cardValidator);

    // CSV number
    QRegExp csvRe("^[0-9]{3}$");
    QRegExpValidator *csvValidator = new QRegExpValidator(csvRe, this);
    ui->securityCodeEdit->setValidator(csvValidator);
}


/**
 * @brief BookingPage::~BookingPage delete the booking page.
 */
BookingPage::~BookingPage() {
    delete ui;
}


/*!
 * \brief BookingPage::buildCustomerInfoMap Create the customer information map by
 * pulling the info from the textEdit fields in the form.
 */
void BookingPage::buildCustomerInfoMap() {
    this->customerInfo->insert("first name", ui->firstNameEdit->text());
    this->customerInfo->insert("last name", ui->lastNameEdit->text());
    this->customerInfo->insert("phone", ui->phoneEdit->text());
    this->customerInfo->insert("email", ui->emailEdit->text());
    this->customerInfo->insert("address", ui->selectAddress->currentText());
}


/*!
 * \brief BookingPage::buildPaymentInfoMap Create the payment information map by
 * pulling the info from the textEdit fields in the form.
 */
void BookingPage::buildPaymentInfoMap() {
    this->paymentInfo->insert("full name", ui->nameEdit->text());
    this->paymentInfo->insert("card number", ui->cardNoEdit->text());
    this->paymentInfo->insert("expiry date", ui->dateEdit->date().toString("dd/MM/yyyy"));
    this->paymentInfo->insert("security code", ui->securityCodeEdit->text());
    this->paymentInfo->insert("address", ui->customerAddress->isChecked() ? ui->selectAddress->currentText() : ui->selectAddress_2->currentText());
    this->paymentInfo->insert("postcode", ui->customerAddress->isChecked() ? ui->postcodeEdit->text() : ui->postcodeEdit_2->text());
}


/*!
 * \brief BookingPage::setTitle Set the title of the booking page, visible in the navigation
 * and bottom left corner.
 * \param title QString title name
 */
void BookingPage::setTitle(QString const& title) {
    m_title = title;
    ui->titleLabel->setText("Editing booking: " + m_title);
}


/*!
 * \brief BookingPage::on_completeButton_clicked Complete the booking,
 * first check that all fields have been filled, then send the data
 * back to the server and close the booking.
 */
void BookingPage::on_completeButton_clicked()
{
    if (detectEmptyFields()) {
        QMessageBox::warning(
            this,
            "Incomplete Booking",
            "You cannot complete the booking when there are empty fields",
            QMessageBox::Ok,
            QMessageBox::Ok
        );
        return;
    }

    if (ui->endTimeEdit->dateTime().toSecsSinceEpoch() < ui->startTimeEdit->dateTime().toSecsSinceEpoch()) {
        QMessageBox::warning(
            this,
            "Rental time invalid",
            "End date is before the start date",
            QMessageBox::Ok,
            QMessageBox::Ok
        );
        return;
    // 15 minutes minimum
    } else if (ui->endTimeEdit->dateTime().toSecsSinceEpoch() - ui->startTimeEdit->dateTime().toSecsSinceEpoch() < 15*60) {
        QMessageBox::warning(
            this,
            "Rental time too short",
            "You cannot rent a bike for less than 15 minutes",
            QMessageBox::Ok,
            QMessageBox::Ok
        );
        return;
    }

    auto complete = QMessageBox::warning(
        this,
        "Complete Booking?",
        "Are you sure you want to complete this booking?",
        QMessageBox::Yes | QMessageBox::No,
        QMessageBox::No // Defaults to No to prevent accidental submission.
    );

    if (complete == QMessageBox::Yes) {
        makeBooking();
    }
}


/*!
 * \brief BookingPage::on_customerAddress_clicked toggle billing address is the same as the
 * customer address.
 * \param checked whether or not the check box has been checked.
 */
void BookingPage::on_customerAddress_clicked(bool checked)
{
    if (checked) {
        ui->houseNoEdit->setEnabled(false);
        ui->postcodeEdit_2->setEnabled(false);
        ui->findAddressButton_2->setEnabled(false);
        ui->selectAddress_2->setEnabled(false);
    } else {
        ui->houseNoEdit->setEnabled(true);
        ui->postcodeEdit_2->setEnabled(true);
        ui->findAddressButton_2->setEnabled(true);
        ui->selectAddress_2->setEnabled(true);
    }
}


/*!
 * @brief BookingPage::getPotentialAddresses uses the getAddress.io API to find an address given the
 * house number and postcode and return a list of possible addresses.
 * @param postcode the customers postcode.
 * @param houseNumber the customers houseNumber.
 * @param outputList the comboBox that the list should be added to once the data is recieved.
 */
void BookingPage::getPotentialAddresses(QString postcode, QString houseNumber, QComboBox *outputList)
{
    Server *server = Server::instance();

    QJsonObject arguments;
    arguments.insert("api-key", s.value("get-address-api-key").toString());

    QString endpoint = "https://api.getAddress.io/find/" + houseNumber + "/" + postcode;
    QNetworkReply *reply = server->sendExternalGetRequest(endpoint, arguments);

    connect(reply, &QNetworkReply::finished, [=]() {

        // check any errors that may have been recieved, if not continue as normal.
        if (reply->error()) {
            QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
            if (statusCode.isValid()){
                ui->errMsgCustomer->setVisible(true);
                switch(statusCode.toInt()) {
                    case 404:
                        ui->errMsgCustomer->setText("No addresses could be found for this postcode.");
                        break;
                    case 400:
                        ui->errMsgCustomer->setText("The api key has expired.");
                        break;
                    case 401:
                        ui->errMsgCustomer->setText("The api-key provided is not valid.");
                        break;
                    case 429:
                        ui->errMsgCustomer->setText("Request limit exceeded!");
                        break;
                    default:
                        ui->errMsgCustomer->setText("Oops, getAddress.io doesn't seem to be working.");
                        break;
                }
            } else {
                qDebug() << "Could not connect to the getAddress.io API. Please check your connection and try again.";
            }
            return;
        }

        QByteArray data = reply->readAll();
        QJsonDocument json = QJsonDocument::fromJson(data);
        QStringList list = json.object()["addresses"].toVariant().toStringList();
        QStringList newList;

        // the API returns additional ,'s in the string we need to remove
        // TODO: theres probably a better way to do this.
        QRegularExpression regex(QStringLiteral("(, ){2,}"));
        QString replace(", ");
        for (auto item : list) {
            newList << item.replace(regex, replace);
        }

        outputList->clear();
        outputList->addItems(newList);
    });
}


/*!
 * @brief BookingPage::on_findAddressButton_clicked get the addresses when clicked
 */
void BookingPage::on_findAddressButton_clicked()
{
    if (ui->postcodeEdit->text() == "") {
        ui->errMsgCustomer->setText("You must provide a postcode to select an address.");
        ui->errMsgCustomer->setVisible(true);
        return;
    }
    ui->errMsgCustomer->setVisible(false);
    getPotentialAddresses(ui->houseNumberEdit->text(), ui->postcodeEdit->text(), ui->selectAddress);
}


/*!
 * @brief BookingPage::on_findAddressButton_2_clicked get the addresses when clicked
 */
void BookingPage::on_findAddressButton_2_clicked()
{
    if (ui->postcodeEdit_2->text() == "") {
        ui->errMsgPayment->setText("You must provide a postcode to select an address.");
        ui->errMsgPayment->setVisible(true);
        return;
    }
    ui->errMsgPayment->setVisible(false);
    getPotentialAddresses(ui->houseNoEdit->text(), ui->postcodeEdit_2->text(), ui->selectAddress_2);
}


/*!
 * \brief BookingPage::detectEmptyFields Detect if any of the input fields are empty.
 * \return bool
 */
bool BookingPage::detectEmptyFields()
{
    bool emptyFields = false;
    buildCustomerInfoMap();
    buildPaymentInfoMap();

    //TODO detect if any bikes have been added to the booking.

    // detect empty field in customer tab
    for (auto item : customerInfo->keys()) {
        if (customerInfo->value(item) == "" || customerInfo->value(item).isNull()) {
            emptyFields = true;
        }
    }

    // detect empty field in payment tab
    if (!ui->radioButton->isChecked()) {
        for (auto item : paymentInfo->keys()) {
            if (paymentInfo->value(item) == "" || paymentInfo->value(item).isNull()) {
                emptyFields = true;
            }
        }
    }

    return emptyFields;
}


/**
 * @brief BookingPage::on_cancelButton_clicked if the cancel booking
 * button is clicked we want to ask the user if they want to cancel the
 * booking for sure.
 */
void BookingPage::on_cancelButton_clicked()
{
    auto result = QMessageBox::warning(
        this,
        "Cancel booking?",
        "Are you sure you want to cancel this booking?\nAll changes will be lost.",
        QMessageBox::Yes | QMessageBox::No,
        QMessageBox::No // Defaults to No to prevent accidental data loss.
    );

    if (result == QMessageBox::Yes)
        emit this->bookingCancelled();
}


/**
 * @brief BookingPage::updateSummaryInfo when the summary tab is opened
 * we want to display all of the information that has been entered in the previous
 * tabs. This will allow the user to check that their data is correct before
 * being submitted.
 */
void BookingPage::updateSummaryInfo()
{
    QString customerHeadings;
    QString customerData;
    QString paymentHeadings;
    QString paymentData;
    buildCustomerInfoMap();
    buildPaymentInfoMap();

    // customer headings
    for (auto item : customerInfo->keys()) {
        customerHeadings.append(toCamelCase(item) + ":\n");
    }

    // customer data
    for (auto item : customerInfo->keys()) {
        customerData.append(customerInfo->value(item) + "\n");
    }

    if (ui->radioButton->isChecked()) {
        paymentHeadings.append("Cash payment selected.");
        paymentData.append(" ");
    } else {
        // payment headings
        for (auto item : paymentInfo->keys()) {
            paymentHeadings.append(toCamelCase(item) + ":\n");
        }

        // payment data
        for (auto item : paymentInfo->keys()) {
            paymentData.append(paymentInfo->value(item) + "\n");
        }
    }

    ui->custHeadingsLabel->setText(customerHeadings);
    ui->custValueLabel->setText(customerData);
    ui->pmtHeadingsLabel->setText(paymentHeadings);
    ui->pmtValueLabel->setText(paymentData);
}


/**
 * @brief BookingPage::on_tabWidget_currentChanged when the current tab has been changed
 * perform specific functions.
 * @param index the index that the tab widget has been changed to.
 */
void BookingPage::on_tabWidget_currentChanged(int index)
{
    if (index == 3) {
        updateSummaryInfo();
    }
}


/*!
 * \brief BookingPage::toCamelCase
 * From: https://stackoverflow.com/questions/41253334/in-qt-what-is-the-best-method-to-capitalise-the-first-letter-of-every-word-in-a
 * \param s
 * \return
 */
QString BookingPage::toCamelCase(const QString& s)
{
    QStringList parts = s.split(' ', QString::SkipEmptyParts);
    for (int i = 0; i < parts.size(); ++i)
        parts[i].replace(0, 1, parts[i][0].toUpper());

    return parts.join(" ");
}


/**
 * @brief BookingPage::addBike add a bike to the booking
 * @param bike the bike that is to be added to the booking.
 */
void BookingPage::addBike(int bike)
{
    bikes.insert(bike);
    populateTreeView();
}


/**
 * @brief BookingPage::populateTreeView populate the tree view with the bikes that
 * have been added to the booking.
 */
void BookingPage::populateTreeView()
{
    ui->bikeTreeWidget->clear();
    ui->orderTree->clear();
    this->inventory.clear();
    this->totalPricePerHour = 0;

    // First we need to create the data structure that determines how many bikes there are.
    for (auto bike : bikes) {
        BikeData bikeData = Server::instance()->getCachedBike(bike);
        if (!this->inventory.contains(createInventoryHash(bikeData))) {
            this->inventory.insert(createInventoryHash(bikeData), QList<int>());
        }

        this->inventory[createInventoryHash(bikeData)].append(bikeData.id);
    }

    // Next we add each of the bike rows
    for (auto bikeModel : this->inventory) {
        if (bikeModel.length() != 0) {
            QStringList row;
            BikeData bikeData = Server::instance()->getCachedBike(bikeModel[0]);

            row << QString::number(bikeModel.length())
                << bikeData.make
                << bikeData.model
                << QString::number(bikeData.price)
                << bikeData.location.nameWithCity();

            QTreeWidgetItem *item = new QTreeWidgetItem(row);
            QTreeWidgetItem *item2 = new QTreeWidgetItem(row);
            item->setData(0, Qt::UserRole, createInventoryHash(bikeData));
            item2->setData(0, Qt::UserRole, createInventoryHash(bikeData));
            ui->orderTree->insertTopLevelItem(0, item);
            ui->bikeTreeWidget->insertTopLevelItem(0, item2);

            // Add to the total price
            this->totalPricePerHour += bikeData.price * bikeModel.length();
        }
    }
    emit updateSubtotal();
}


/**
 * @brief BookingPage::createInventoryHash create the hash that is used to find a certain
 * bike in the inventory.
 * @param bikeData the bike data that the string will be made from.
 * @return QString
 */
QString BookingPage::createInventoryHash(BikeData bikeData)
{
    return bikeData.make + bikeData.model + QString::number(bikeData.location.id);
}


/**
 * @brief BookingPage::setSubtotal set the subtotal amount to the total price of
 * all of the bikes multiplied by the number of hours that the bikes
 * are being rented for.
 */
void BookingPage::setSubtotal()
{
    QDateTime startDate = ui->startTimeEdit->dateTime();
    QDateTime endDate = ui->endTimeEdit->dateTime();

    if (endDate.toTime_t() > startDate.toTime_t()) {
        double pricePerSecond = static_cast<double>(totalPricePerHour) / (60*60);
        double discountOffset = 1;
        bool discountApplied = false;
        int seconds = static_cast<int>(endDate.toTime_t() - startDate.toTime_t());

        if (seconds >= 3*24*60*60) {
            discountOffset = 0.5;
            discountApplied = true;
        } else if (seconds >= 1*24*60*60) {
            discountOffset = 0.75;
            discountApplied = true;
        }

        QString subTotal = "£" + QString::number(pricePerSecond * seconds * discountOffset, 'f', 2);
        QString discount = "Value Saved: £" + QString::number((pricePerSecond * seconds * (1 - discountOffset)), 'f', 2);

        if (discountApplied)
            discount += " (discount applied)";

        ui->subTotalLabel->setText(subTotal);
        ui->subTotalLabel_2->setText(subTotal);
        ui->discount->setText(discount);
        ui->discount_2->setText(discount);
    } else {
        ui->subTotalLabel->setText("£0.00");
        ui->subTotalLabel_2->setText("£0.00");
        ui->discount->setText("Value Saved: £0.00");
        ui->discount_2->setText("Value Saved: £0.00");
    }


}


/**
 * @brief BookingPage::on_startTimeEdit_dateTimeChanged when the time is changed emit a signal.
 * @param dateTime
 */
void BookingPage::on_startTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{   
    //checkTimeRange();
    emit updateSubtotal();
}


/**
 * @brief BookingPage::on_endTimeEdit_dateTimeChanged when the time is changed emit a signal.
 * @param dateTime
 */
void BookingPage::on_endTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    //checkTimeRange();
    emit updateSubtotal();
}


/**
 * @brief BookingPage::checkTimeRange make sure that the user cannot enter a time range that is invalid.
 */
void BookingPage::checkTimeRange()
{
    if (ui->endTimeEdit->dateTime().toTime_t() < ui->startTimeEdit->dateTime().toTime_t()) {
        QMessageBox::warning(
            this,
            "Invalid time range",
            "The end time cannot be before the start time",
            QMessageBox::Ok,
            QMessageBox::Ok
        );

        ui->endTimeEdit->setDateTime(ui->startTimeEdit->dateTime());
    }
}


/**
 * @brief BookingPage::makeBooking create a booking and send it to the server.
 */
void BookingPage::makeBooking()
{
    buildCustomerInfoMap();
    buildPaymentInfoMap();

    billing.postCode = paymentInfo->value("postcode");
    billing.addressLine1 = paymentInfo->value("address");

    // issue blocking request to get billing address
    QEventLoop loop;
    QMetaObject::Connection callback = connect(Server::instance(), &Server::billingAddressAdded, this, [=, &loop](BillingAddressData data) {
        billing = data;
        loop.quit();
    });

    Server::instance()->addBillingAddress(billing);
    loop.exec();
    disconnect(callback);

    // collect booking data
    booking.start = ui->startTimeEdit->dateTime().toSecsSinceEpoch();
    booking.end = ui->endTimeEdit->dateTime().toSecsSinceEpoch();
    booking.billing = billing;
    booking.user = Server::instance()->currentUser();

    QMap<int, BikeData> bikeData;
    for (auto bike : this->bikes) {
        BikeData bikeInformation = Server::instance()->getCachedBike(bike);
        bikeData.insert(bikeInformation.id, bikeInformation);
    }

    booking.bikes = bikeData;
    booking.paymentId = 1;
    booking.location.id = locationId;

    // blocking request to send booking to the server
    QEventLoop loop2;
    QMetaObject::Connection callback2 = connect(Server::instance(), &Server::bookingAdded, this, [=](BookingData data){
        qDebug() << "booking successful";
        ui->tabWidget->setTabEnabled(4, true);
        ui->tabWidget->setCurrentIndex(4);
    });

    Server::instance()->addBooking(booking);
    loop2.exec();
    disconnect(callback2);
}


/**
 * @brief BookingPage::on_pushButton_2_clicked remove a bike from the booking
 */
void BookingPage::on_pushButton_2_clicked()
{
    if (ui->bikeTreeWidget->currentItem()) {
        QTreeWidgetItem *itemToRemove = ui->bikeTreeWidget->currentItem();

        // Get the bike that is referenced by the current item
        QList<int> bikes = this->inventory.value(itemToRemove->data(0, Qt::UserRole).toString());
        BikeData bikeData = Server::instance()->getCachedBike(bikes.takeFirst());

        this->totalPricePerHour -= bikeData.price;
        this->bikes.remove(bikeData.id);
        emit populateTreeView();
    }
}


/**
 * @brief BookingPage::showBikeInfoDialog show the bike information dialog. This
 * view allows you to see all the data held about a certain bike.
 * @param id
 */
void BookingPage::showBikeInfoDialog(int id)
{
    QMetaObject::Connection callback = connect(Server::instance(), &Server::bikeUpdated, this, [this](BikeData bikeData) {
        BikeInfo::addBikeInfoDialog(bikeData, this);
    });

    Server::instance()->requestBike(id);

    disconnect(callback);
}

/**
 * @brief BookingPage::on_radioButton_2_clicked customer is paying with cash.
 */
void BookingPage::on_radioButton_2_clicked()
{
    ui->nameEdit->setEnabled(true);
    ui->cardNoEdit->setEnabled(true);
    ui->dateEdit->setEnabled(true);
    ui->securityCodeEdit->setEnabled(true);
    ui->customerAddress->setEnabled(true);
    ui->houseNoEdit->setEnabled(true);
    ui->postcodeEdit_2->setEnabled(true);
    ui->findAddressButton_2->setEnabled(true);
    ui->selectAddress_2->setEnabled(true);
}


/**
 * @brief BookingPage::on_radioButton_clicked customer is paying with card.
 */
void BookingPage::on_radioButton_clicked()
{
    ui->nameEdit->setEnabled(false);
    ui->cardNoEdit->setEnabled(false);
    ui->dateEdit->setEnabled(false);
    ui->securityCodeEdit->setEnabled(false);
    ui->customerAddress->setEnabled(false);
    ui->houseNoEdit->setEnabled(false);
    ui->postcodeEdit_2->setEnabled(false);
    ui->findAddressButton_2->setEnabled(false);
    ui->selectAddress_2->setEnabled(false);
}


/**
 * @brief BookingPage::populateLocations populate the selection combo box with all the locations that are available.
 */
void BookingPage::populateLocations()
{
    Server::instance()->requestLocations();
}


/**
 * @brief BookingPage::on_locationSelect_currentIndexChanged the location that the booking is representing has changed
 * @param index the index of the location in the combo box.
 */
void BookingPage::on_locationSelect_currentIndexChanged(int index)
{
    locationId = ui->locationSelect->currentData().toInt();
}


/**
 * @brief BookingPage::getLocationId the location id that is currently selected.
 * @return the id of the current location
 */
int BookingPage::getLocationId()
{
    return this->locationId;
}

/**
 * @brief BookingPage::handleBookingError handle a server side error that has occured
 * @param errorCode
 */
void BookingPage::handleBookingError(int errorCode)
{
    QString message;

    switch(errorCode) {
        case 4000:
            message = "End time is before the start of the rental.";
            break;
        case 4001:
            message = "Start time is in the past";
            break;
        case 10001:
            message = "Tried to rent bikes that are no longer available in given time period, for the given location";
            break;
        default:
            message = "Internal server error, please try again later";
            break;
    }

    QMessageBox::warning(
        this,
        "Booking incomplete, error occured",
        message,
        QMessageBox::Ok,
        QMessageBox::Ok
    );
}


/**
 * @brief BookingPage::getBikes get the bikes that are part of the booking.
 * @return QSet
 */
QSet<int> BookingPage::getBikes()
{
    return this->bikes;
}


/**
 * @brief BookingPage::on_pushButton_clicked cancelling booking has same
 * functionality as completing at this point. Closes the booking page.
 */
void BookingPage::on_pushButton_clicked()
{
    emit this->bookingCancelled();
}
