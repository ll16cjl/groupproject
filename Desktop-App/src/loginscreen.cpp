#include "loginscreen.h"
#include "ui_loginscreen.h"


/**
 * @brief LoginScreen::LoginScreen create the login screen. On this screen the user
 * will type their credentials ot be able to login and recieve an authentication token.
 * @param parent the parent widget.
 */
LoginScreen::LoginScreen(QWidget *parent) : QWidget(parent), ui(new Ui::LoginScreen) {
    ui->setupUi(this);

    /*
     * Trigger the login attempt signal when the login button is clicked.
     */
    connect(
        ui->loginButton, 
        &QPushButton::clicked, 
        [=]() {
            this->loginAttempt(this->ui->usernameField->text(), this->ui->passwordField->text());
        }
    );

    /*
     * Connect the signals triggered when the user hits enter in the username or password fields.
     *
     * Hitting enter in the username field switches focus to the password field.
     * Hitting enter in the password field acts like the user hit the login button.
     */
    connect(
        ui->usernameField,
        &QLineEdit::returnPressed,
        ui->loginButton,
        [=]() {
            // TabFocusReason makes this behave exactly as if the user hit Tab to switch fields. 
            ui->passwordField->setFocus(Qt::TabFocusReason);
        }
    );

    connect(
        ui->passwordField,
        &QLineEdit::returnPressed,
        ui->loginButton,
        &QAbstractButton::click
    );
}


/**
 * @brief LoginScreen::setErrMsg if an error occured, show it.
 * @param errMsg the error message to display.
 */
void LoginScreen::setErrMsg(QString errMsg) {
    ui->errMsgLabel->setText(errMsg);
}


/**
 * @brief LoginScreen::~LoginScreen delete the login screen.
 */
LoginScreen::~LoginScreen() {
    delete ui;
}
