#include "include/receiptdialog.h"
#include "ui_receiptdialog.h"
#include <QDateTime>
#include <memory>

ReceiptDialog::ReceiptDialog(ReceiptData data, ReceiptDialog::Mode mode, QWidget *parent) : QDialog(parent), ui(new Ui::ReceiptDialog) {
    ui->setupUi(this);

    QDateTime currentTime = QDateTime::currentDateTime();
    ui->dateLabel->setText(currentTime.toString("dd-mm-yyyy HH:mm"));

    ui->customerNameLabel->setText(data.name);
    ui->locationNameLabel->setText(data.locationName + " (" + data.locationCity + ")");

    QDateTime startTime = QDateTime::fromSecsSinceEpoch(data.start);
    QDateTime endTime = QDateTime::fromSecsSinceEpoch(data.end);

    const QString timeFormat = "dd-mm-yyyy HH:mm";
    ui->timeLabel->setText(startTime.toString(timeFormat) + " to " + endTime.toString(timeFormat));

    ui->priceLabel->setText("Cost: \u00A3" + QString::number(data.cost));
    ui->discountLabel->setText("Discount: \u00A3" + QString::number(data.discount));
    ui->totalLabel->setText("Total: \u00A3" + QString::number(data.total));

    bool paidByCard = data.digits.size() > 0;
    if (paidByCard) {
        ui->billingLabel->setText("\u00A3" + QString::number(data.total) + " withdrawn from the account ending in " + data.digits);
    }
    else {
        ui->billingLabel->setText("\u00A3" + QString::number(data.total) + " paid at the till.");
    }

    auto *layout = ui->layout;
    int bikeIndex = -1;
    for (int i = 0; i < layout->count(); ++i) {
        auto *layoutItem = layout->itemAt(i);
        QLabel *label = dynamic_cast<QLabel*>(layoutItem->widget());
        if (label && label == ui->bikesLabel) {
            bikeIndex = i;
            break;
        }
    }

    for (auto bike : data.bikes) {
        layout->insertWidget(bikeIndex + 1, new QLabel(bike.make + " " + bike.model + " (Price: \u00A3" + QString::number(bike.price) + ")"));
    }

    if (mode == ReceiptDialog::Print) {
        ui->buttons->clear();
    }
}

ReceiptDialog::~ReceiptDialog() {
    delete ui;
}

void ReceiptDialog::display(ReceiptData receipt, QWidget *parent) {
    std::unique_ptr<ReceiptDialog> dialog(new ReceiptDialog(receipt, ReceiptDialog::Display, parent));
    dialog->exec(); // We don't care what the result is.
}

void ReceiptDialog::print(ReceiptData receipt, QPaintDevice *printer) {
    std::unique_ptr<ReceiptDialog> dialog(new ReceiptDialog(receipt, ReceiptDialog::Print));

    QPalette palette = dialog->palette();
    palette.setColor(QPalette::Background, Qt::white);
    dialog->setPalette(palette);

    dialog->render(printer);
}
