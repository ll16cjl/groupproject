#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QJsonObject>

/**
 * @brief MainWindow::MainWindow Create the MainWindow, this is the widget that
 * will be visible whilst the application is in use.
 * @param parent the parent widget to the main window.
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    connect(ui->loginScreen, &LoginScreen::loginAttempt, this, &MainWindow::loginAttempt);

    connect(
        ui->managementScreen,
        &ManagementScreen::logoutAttempt,
        this,
        &MainWindow::logoutAttempt
    );

    connect(this, &MainWindow::setErrMsg, ui->loginScreen, &LoginScreen::setErrMsg);
}


/**
 * @brief MainWindow::loginAttempt Attempt to connect to the server and login.
 * If successful, change the view to the main management view.
 *
 * @param username QString current login details entered.
 * @param password QString current login details entered.
 */
void MainWindow::loginAttempt(QString username, QString password) {
    Server *serverRequest = Server::instance();

    QJsonObject arguments;
    arguments.insert("email", username);
    arguments.insert("password", password);

    QNetworkReply *loginReply = serverRequest->sendGetRequest("session", arguments);
    connect(loginReply, &QNetworkReply::finished, [=](){
        loginResult(loginReply, username);
    });
}


/**
 * @brief MainWindow::loginResult handle a server reply for the login.
 * If the login is incorrect then do not allow access,
 * whilst if the login is correct the allow the user to enter the app.
 *
 * @param loginReply QNetworkReply network reply to the login request
 */
void MainWindow::loginResult(QNetworkReply *loginReply, QString username) {
    // no connection to the server
    if (loginReply->error()) {
        setErrMsg("A connection to the server could not be established");
        ui->loginScreen->setStyleSheet("QLabel{ color: red; }");
        return;
    }

    QByteArray response_data = loginReply->readAll();
    QJsonDocument json = QJsonDocument::fromJson(response_data);

    // user not authenticated
    if (json.object()["success"].toBool() == false) {
        setErrMsg("User could not be authenticated.");
        ui->loginScreen->setStyleSheet("QLabel{ color: red; }");
        return;
    }

    Server::instance()->setAuthToken(json.object()["token"].toString());
    s.setValue("username", username);
    qDebug() << Server::instance()->authToken();

    ui->screens->setCurrentWidget(ui->managementScreen);
    ui->loginScreen->setStyleSheet("");
    setErrMsg("");
    loginReply->deleteLater();
}


/**
 * @brief MainWindow::logoutAttempt attempt to log out presenting the user with the
 * login screen again.
 */
void MainWindow::logoutAttempt() {
    /*
     * Send DELETE request to end current session.
     *
     * TODO: Currently we intentionally ignore the reply and 'log out' anyway. Perhaps there's a better way to handle this?
     */
    QJsonObject arguments;
    arguments.insert("auth_token", Server::instance()->authToken());
    Server::instance()->sendDeleteRequest("session", arguments);

    /*
     * Focus the login screen
     */
    ui->screens->setCurrentWidget(ui->loginScreen);

    /*
     * Delete the current instance of the management screen to destroy the current session.
     */
    ui->screens->removeWidget(ui->managementScreen);
    delete ui->managementScreen; ui->managementScreen = nullptr;
    ui->managementScreen = new ManagementScreen(this);
    ui->screens->addWidget(ui->managementScreen);

    /*
     * We need to reconnect logoutAttempt because we have a new managementScreen
     */
    connect(
        ui->managementScreen,
        &ManagementScreen::logoutAttempt,
        this,
        &MainWindow::logoutAttempt
    );

    /*
     * We need to clear the authToken in Server
     */
    Server::instance()->setAuthToken("");
}


/**
 * @brief MainWindow::~MainWindow main window destructor.
 */
MainWindow::~MainWindow() {
    delete ui;
}
