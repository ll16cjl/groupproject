#include "bookingsview.h"
#include "ui_bookingsview.h"

BookingsView::BookingsView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookingsView)
{
    ui->setupUi(this);
}

BookingsView::~BookingsView()
{
    delete ui;
}
