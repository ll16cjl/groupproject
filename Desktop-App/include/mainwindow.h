#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QSettings>

#include "serversetup.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void setErrMsg(QString errMsg);

private slots:
    void loginAttempt(QString username, QString password);
    void loginResult(QNetworkReply *loginReply, QString username);
    void logoutAttempt();

private:
    Ui::MainWindow *ui;
    QSettings s;
};

#endif // MAINWINDOW_H
