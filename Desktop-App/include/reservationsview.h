#pragma once
#ifndef RESERVATIONSVIEW_H
#define RESERVATIONSVIEW_H

#include <QWidget>
#include <QMenu>
#include <QActionGroup>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTreeWidget>
#include <QPrinter>
#include <QPrintDialog>

#include "serversetup.h"
#include "bookingpage.h"
#include "createbookingdialog.h"

namespace Ui {
    class ReservationsView;
}

class ReservationsView : public QWidget {
    Q_OBJECT

public:
    explicit ReservationsView(QWidget *parent = nullptr);
    ~ReservationsView();
    void createSearchByMenu();
    void getAllReservations();
    void populateTreeView(QMap<int, BookingData> bookings);
    void showEvent(QShowEvent *event);
    void searchReservations(QString searchTerm);
    void createBikeContextMenu();
    void handlePrintReciept(int bookingID);
    void handleViewReciept(int bookingID);

signals:
    void refreshTree();

private slots:
    void on_searchBar_textChanged(const QString &text);
    void on_searchButton_clicked();

    void on_searchResultsTree_customContextMenuRequested(const QPoint &pos);

private:
    Ui::ReservationsView *ui;
    QMenu *searchByMenu;
    QActionGroup *searchByGroup;
    QMap<QString, int> filterColumnIndex;
    QString currentSearchByOption;

    QMenu *reservationMenu;
    QAction *viewReciept;
    QAction *printReciept;
};

#endif // RESERVATIONSVIEW_H
