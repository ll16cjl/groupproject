#pragma once
#ifndef INVENTORYVIEW_H
#define INVENTORYVIEW_H

#include <QWidget>
#include <QMenu>
#include <QActionGroup>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTreeWidget>

#include "bikeinfo.h"
#include "serversetup.h"
#include "bikedialog.h"
#include "bookingpage.h"
#include "createbookingdialog.h"

namespace Ui {
    class InventoryView;
}

class InventoryView : public QWidget {
    Q_OBJECT

public:
    explicit InventoryView(QWidget *parent = nullptr);
    ~InventoryView();
    void createSearchByMenu();
    void populateTreeView(QMap<int, BikeData> bikes);
    void showEvent(QShowEvent *event);
    void searchBikes(QString searchTerm);
    void createBikeContextMenu();
    void setOpenBookings(QMap<QString, BookingPage *> *openBookings);
    void createNewBooking(BikeData bikeData, QString booking);
    void getBikes();
    QString createInventoryHash(BikeData bikeData);

signals:
    void refreshTree();

private slots:
    void showAddBikeDialog();
    void showBikeInfoDialog(int id);
    void showBikeBookingDialog(int id);
    void removeBike(int id);
    void on_searchResultsTree_customContextMenuRequested(const QPoint &pos);
    void on_searchBar_textChanged(const QString &text);
    void on_searchButton_clicked();
    void on_cLocCheckBox_clicked(bool checked);
    void on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime);
    void on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime);

private:
    Ui::InventoryView *ui;
    QSettings s;
    QMenu *searchByMenu;
    QActionGroup *searchByGroup;
    bool onlyCurrentLocation;
    QString currentSearchByOption;
    QMap<QString, int> filterColumnIndex;
    QMap<QString, BookingPage *> *openBookings;
    QMap<QString, QList<int>> inventory;
    bool loadedStatus;

    QMenu *bikeMenu;
    QAction *removeAction;
    QAction *bookingAction;
    QAction *viewBikeData;
};

#endif // INVENTORYVIEW_H
