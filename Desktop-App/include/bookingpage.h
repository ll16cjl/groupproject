#pragma once
#ifndef BOOKINGPAGE_H
#define BOOKINGPAGE_H

#include <QWidget>
#include <QString>
#include <QMap>
#include <QSettings>
#include <QAction>
#include <QComboBox>

#include "serversetup.h"
#include "bikeinfo.h"

namespace Ui {
    class BookingPage;
}

class BookingPage : public QWidget {
    Q_OBJECT

public:
    explicit BookingPage(QWidget *parent = nullptr);
    ~BookingPage();

    void setTitle(QString const& title);
    QString const& getTitle() const { return m_title; }
    QString &getTitle() { return m_title; }
    void buildCustomerInfoMap();
    void buildPaymentInfoMap();
    void getPotentialAddresses(QString postcode, QString houseNumber, QComboBox *outputList);
    void addBooking();
    bool detectEmptyFields();
    void updateSummaryInfo();
    QString toCamelCase(const QString& s);
    void addBike(int bike);
    void populateTreeView();
    void makeBooking();
    void populateLocations();
    int getLocationId();
    QSet<int> getBikes();
    void checkTimeRange();
    QString createInventoryHash(BikeData bikeData);

signals:
    void bookingCancelled();
    void updateSubtotal();

private slots:
    void setSubtotal();
    void on_completeButton_clicked();
    void on_customerAddress_clicked(bool checked);
    void on_findAddressButton_clicked();
    void on_findAddressButton_2_clicked();
    void on_cancelButton_clicked();
    void on_tabWidget_currentChanged(int index);
    void on_startTimeEdit_dateTimeChanged(const QDateTime &dateTime);
    void on_endTimeEdit_dateTimeChanged(const QDateTime &dateTime);
    void on_pushButton_2_clicked();
    void showBikeInfoDialog(int id);
    void on_radioButton_2_clicked();
    void on_radioButton_clicked();
    void on_locationSelect_currentIndexChanged(int index);
    void handleBookingError(int errorCode);

    void on_pushButton_clicked();

private:
    QSettings s;
    Ui::BookingPage *ui;
    QString m_title;

    QMap<QString, QString> *customerInfo;
    QMap<QString, QString> *paymentInfo;

    BillingAddressData billing;
    BookingData booking;

    QSet<int> bikes;
    QMap<QString, QList<int>> inventory;
    double totalPricePerHour;
    int locationId;
};

#endif 
