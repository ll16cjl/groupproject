#ifndef SERVERSETUP_H
#define SERVERSETUP_H

#include <QSettings>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QWebEngineView>
#include <QUrlQuery>
#include <QDebug>
#include <QPaintDevice>
#include <QPrinter>
#include <QVector>
#include <memory>

struct UserData {
    int id;
    QString first;
    QString last;
    QString email;

    /*
     * Get first and last name as string.
     */
    QString fullName() const {
        return QString(first + last);
    }

    /*
     * Returns the full name + email in teh format:
     * FirstName LastName (email@address.com)
     */
    QString fullDetails() const {
        return QString(first + " " + last + " (" + email + ")");
    }
};

struct LocationData {
    int id;
    QString name;
    QString city;
    double longitude;
    double latitude;

    /*
     * Returns a nicely formatted string containing the name of the location and the city it is in.
     */
    QString nameWithCity() const {
        if (city.length() > 0) {
            return name + " (" + city + ")";
        }
        else {
            return name;
        }
    }
};

struct BikeData {
    int id;
    QString model;
    QString make;
    double price;
    LocationData location = {};
};

struct BillingAddressData {
    int id;
    QString postCode;
    QString addressLine1;
};

struct BookingData {
    int id;
    UserData user = {};
    LocationData location = {};
    uint start;
    uint end;
    uint reservationTime;
    QMap<int, BikeData> bikes;
    BillingAddressData billing = {};
    int paymentId; // for the desktop app 0 => Cash payment, 1 => Card payment.
};

struct ReceiptData {
    /*
     * Cannot use BikeData as the /receipts GET doesn't return all the data required for a
     * BikeData object.
     */
    struct Bike {
        QString make;
        QString model;
        int price;
    };

    QString name;
    QVector<Bike> bikes;
    QString locationName, locationCity;
    double cost;
    double discount;
    double total;
    qint64 start;
    qint64 end;
    QString digits;

    static ReceiptData fromNetworkReply(QNetworkReply *reply);
};

class Server : public QObject {
    Q_OBJECT

    public:
        QNetworkReply* sendPostRequest(QString endpoint, QJsonObject arguments);
        QNetworkReply* sendGetRequest(QString endpoint, QJsonObject arguments);
        QNetworkReply* sendExternalGetRequest(QString endpoint, QJsonObject arguments);
        QNetworkReply* sendDeleteRequest(QString endpoint, QJsonObject arguments);
        QNetworkReply* sendPutRequest(QString endpoint, QJsonObject arguments);

        static std::unique_ptr<Server> makeInstance();
        static Server *instance();

        void setAuthToken(QString token) { currentAuthToken = token; }
        QString &authToken() { return currentAuthToken; }
        QString const& authToken() const { return currentAuthToken; }

        void setCurrentUser(UserData data) { loggedInUser = data; }
        UserData &currentUser() { return loggedInUser; }
        UserData const& currentUser() const { return loggedInUser; }

        QMap<int, BikeData> &bikes() { return cachedBikes; }
        QMap<int, BikeData> const& bikes() const { return cachedBikes; }

        QMap<int, LocationData> &locations() { return cachedLocations; }
        QMap<int, LocationData> const& locations() const { return cachedLocations; }

        BikeData getCachedBike(int bikeId);
        LocationData getCachedLocation(int locationId);
        UserData getCachedUser(int userId);
        BillingAddressData getCachedBilling(int billingId);

        void handleServerError(int errorCode);
        void handleRequestError();

        /*
         * Return by value for now. Check the ID matches!!! Empty, default constructed
         * BookingData returned when the ID does not exist!
         *
         * Returns booking data from cached bookings! 
         * You should requestBookingsWithReply first, then take the bookingdata on
         * QNetworkReply::finished (connect a lambda to it).
         */
        BookingData getBookingData(int bookingID) {
            return this->cachedBookings.value(bookingID);
        }

    public slots:
        /*
         * Sends a request for location(s).
         * Once the locations are received, triggers the locationsUpdates signal.
         */
        void requestLocations();
        void requestLocation(int locationId);
        QNetworkReply *requestLocationsWithReply();
        QNetworkReply *requestLocationWithReply(int locationId);

        /*
         * Adds a new location and triggers locationAdded and locationsUpdated.
         */
        void addLocation(QString name, QString city, double longitude, double latitude);

        /*
         * Sends a request for bikes.
         * Once the bikes have been received, triggers the bikesUpdated signal.
         */
        void addBike(BikeData data, int quantity);
        BikeData addSingleBike(int id, BikeData data);
        void requestBikes();
        void requestBike(int bikeId);
        QNetworkReply *requestBikesWithReply();
        QNetworkReply *requestBikeWithReply(int bikeId);

        /*
         * Add/Request booking
         */
        void addBooking(BookingData data);
        void requestBookings();
        QNetworkReply *requestBookingsWithReply();

        /*
         * Request a user
         */
        QNetworkReply *requestUserWithReply(int userId);
        void requestUser(int userId);

        /*
         * Request the current user
         */
        QNetworkReply *requestCurrentUserWithReply();
        void requestCurrentUser();

        /*
         * Add/Request billing address
         */
        void addBillingAddress(BillingAddressData data);
        void requestBillingAddress(int billingId);
        QNetworkReply *requestBillingAddressWithReply(int billingId);

        /*
         * Get the bikes available between two times
         */
        void requestAvailableBikes(QDateTime start, QDateTime end);
        QNetworkReply *requestAvailableBikesWithReply(QDateTime start, QDateTime end);

        /*
         * Request receipt
         */
        QNetworkReply *requestReceiptWithReply(int bookingID);

    signals:
        void locationsUpdated(QMap<int, LocationData> locations);
        void locationUpdated(LocationData location);
        void locationAdded(LocationData location);
        void bikesUpdated(QMap<int, BikeData> bikes);
        void bikeUpdated(BikeData bike);
        void bikeAdded(BikeData bike);
        void bookingsUpdated(QMap<int, BookingData> bookings);
        void bookingUpdated(BookingData booking);
        void bookingAdded(BookingData booking);
        void userUpdated(UserData user);
        void currentUserUpdated(UserData user);
        void billingAddressUpdated(BillingAddressData billing);
        void billingAddressAdded(BillingAddressData billing);
        void availableBikesUpdated(QMap<int, BikeData> bikes);
        void bookingError(int errorCode);

    private:
        Server();

        QSettings s;
        QNetworkAccessManager *manager;

        QString currentAuthToken;
        UserData loggedInUser;

        /*
         * We keep the latest copy of the bike and location data just in case it's needed.
         */
        QMap<int, BikeData> cachedBikes;
        QMap<int, LocationData> cachedLocations;
        QMap<int, BookingData> cachedBookings;
        QMap<int, UserData> cachedUsers;
        QMap<int, BillingAddressData> cachedBillingData;
        QMap<int, BikeData> cachedAvailableBikes;

        QUrlQuery buildParams(QJsonObject params);

        const bool LOG_REQUESTS = true;
};

#endif // SERVERSETUP_H
