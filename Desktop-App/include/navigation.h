#ifndef NAVIGATION_H
#define NAVIGATION_H

#include <QWidget>
#include <QStyleOptionButton>
#include <QStylePainter>
#include <QPushButton>
#include <QActionGroup>
#include <QLabel>
#include <QVBoxLayout>
#include <QPointer>
#include <QButtonGroup>
#include <QKeyEvent>

namespace Ui {
class Navigation;
}

class NavPushButton;
class NavPushButtonGroup;

class Navigation : public QWidget
{
    Q_OBJECT

    public:
        explicit Navigation(QWidget *parent = nullptr);
        ~Navigation();
        void addItem(QString name, QString navGroup, QWidget *targetWidget, bool focus = false);
        void addLabel(QString name, QString navGroup);
        void removeItem(QString name, QString navName);
        QPointer<NavPushButtonGroup> getGroup(QString name);
        void setBookingOffset(int index);
        int getBookingOffset();

    signals:
        void changePage(QWidget *targetWidget);
        void newBookingClicked();

    private slots:
        void routePages();

    private:
        Ui::Navigation *ui;
        QActionGroup *actionGroup;
        QMap<QString, QPointer<NavPushButtonGroup>> navGroups;
        QButtonGroup *buttonGroup;
        bool isFirstButtonAdded;
        int bookingOffset;
};

class NavPushButtonGroup : public QWidget
{
    Q_OBJECT

    public:
        NavPushButtonGroup();
        void addItem(QString name, QWidget *newWidget);
        void removeItem(QString name);

    private:
        QVBoxLayout *newGroupLayout;
        QMap<QString, QPointer<QWidget>> navButtons;
};

class NavPushButton : public QPushButton
{
    Q_OBJECT

    public:
        explicit NavPushButton(QString navName, QWidget *targetWidget = nullptr, QWidget* parent = nullptr);
        void paintEvent(QPaintEvent *) override;
        QAction *getAction();
        QWidget *getTargetWidget();
        void setAction(QAction *action);

    private:
        QString navName;
        QAction *action;
        QWidget *targetWidget;
};


#endif // NAVIGATION_H
