#pragma once
#ifndef BIKEDIALOG_H
#define BIKEDIALOG_H

#include <QDialog>
#include <QVector>
#include <serversetup.h>

namespace Ui {
    class BikeDialog;
}

class BikeDialog : public QDialog
{
    Q_OBJECT

public:
    struct Result {
        int locationID;
        QString model;
        QString make;
        int price;
        int quantity;
        bool submit = false;
    };

    explicit BikeDialog(QWidget *parent = 0);
    explicit BikeDialog(int locationID, QString model, QString make, int price, QWidget *parent = 0);
    ~BikeDialog();

    static Result addBikeDialog(QWidget *parent = nullptr);
    static Result updateBikeDialog(int locationID, QString model, QString make, int price, QWidget *parent = nullptr);

    Result getResult();

    void accept();
    void reject();

protected slots:
    void populateLocationComboBox(QMap<int, LocationData> locations);
    void locationAdded(LocationData data);
    void locationIDChanged(int id);

private:
    QSettings s;
    const int NEW_LOCATION_OPTION = -1;

    Ui::BikeDialog *ui;
};

#endif // BIKEDIALOG_H
