#pragma once
#ifndef LOCATIONDIALOG_H
#define LOCATIONDIALOG_H

#include <QDialog>
#include <QDoubleValidator>

namespace Ui {
    class LocationDialog;
}

class LocationDialog : public QDialog {
    Q_OBJECT

public:
    struct Result {
        QString name;
        QString city;
        double latitude;
        double longitude;
        bool submit = false;
    };

    ~LocationDialog();

    static Result addLocationDialog(QWidget *parent = nullptr);
    static Result updateLocationDialog(QString name, QString city, double latitude, double longitude, QWidget *parent = nullptr);

    Result getResult() const;

private:
    explicit LocationDialog(QWidget *parent = nullptr);
    explicit LocationDialog(QString name, QString city, double latitude, double longitude, QWidget *parent = nullptr);
    void setValidators();

    Ui::LocationDialog *ui;
};

#endif // LOCATIONDIALOG_H
