#ifndef RECEIPTDIALOG_H
#define RECEIPTDIALOG_H

#include <QDialog>
#include <QPaintDevice>
#include "serversetup.h"

namespace Ui {
    class ReceiptDialog;
}

class ReceiptDialog : public QDialog
{
    Q_OBJECT

public:
    enum Mode {
        Display,
        Print
    };

    explicit ReceiptDialog(ReceiptData receipt, Mode mode, QWidget *parent = nullptr);
    ~ReceiptDialog();

    static void display(ReceiptData receipt, QWidget *parent = nullptr);
    static void print(ReceiptData receipt, QPaintDevice *printer);

private:
    Ui::ReceiptDialog *ui;
};

#endif // RECEIPTDIALOG_H
