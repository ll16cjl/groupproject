#ifndef LOCATIONSVIEW_H
#define LOCATIONSVIEW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QUrl>
#include <QUrlQuery>
#include <QWebEngineView>
#include <QPushButton>
#include "serversetup.h"

namespace Ui {
class LocationsView;
}

class LocationsView : public QWidget
{
    Q_OBJECT

public:
    explicit LocationsView(QWidget *parent = nullptr);
    void updateLocations(QMap<int, LocationData> locations);
    void showEvent(QShowEvent *event);
    ~LocationsView();

public slots:
    void updateMapsUrl();

private:
    Ui::LocationsView *ui;
    QWebEngineView *engine;
};

class LocationPushButton : public QPushButton
{
    Q_OBJECT

public:
    explicit LocationPushButton(QString name, double longitude, double latitude, QWidget *parent = nullptr);
    double longitude;
    double latitude;
};

#endif // LOCATIONSVIEW_H
