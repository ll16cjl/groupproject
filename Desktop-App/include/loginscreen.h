#pragma once
#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H

#include <QWidget>

namespace Ui {
    class LoginScreen;
}

class LoginScreen : public QWidget {
    Q_OBJECT

public:
    explicit LoginScreen(QWidget *parent = nullptr);
    virtual ~LoginScreen();

signals:
    void loginAttempt(QString username, QString password);

public slots:
    void setErrMsg(QString errMsg);

private:
    Ui::LoginScreen *ui;


};

#endif /*LOGINSCREEN_H*/
