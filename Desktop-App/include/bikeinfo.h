#ifndef BIKEINFO_H
#define BIKEINFO_H

#include <QDialog>
#include <QMessageBox>

#include "bookingpage.h"

namespace Ui {
class BikeInfo;
}

class BikeInfo : public QDialog
{
    Q_OBJECT

public:
    explicit BikeInfo(BikeData bikeData, QWidget *parent = nullptr);
    ~BikeInfo();

    static void addBikeInfoDialog(BikeData bikeData, QWidget *parent = nullptr);

private:
    Ui::BikeInfo *ui;
};

#endif // BIKEINFO_H
