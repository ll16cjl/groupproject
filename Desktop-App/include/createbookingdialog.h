#ifndef CREATEBOOKINGDIALOG_H
#define CREATEBOOKINGDIALOG_H

#include <QDialog>
#include <QMessageBox>

#include "serversetup.h"

namespace Ui {
class CreateBookingDialog;
}

class CreateBookingDialog : public QDialog
{
    Q_OBJECT

public:
    struct Result {
        int bikeId;
        QString booking;
        bool submit = false;
    };

    explicit CreateBookingDialog(BikeData bikeData, QWidget *parent = nullptr);
    explicit CreateBookingDialog(BikeData bikeData, QStringList openBookings, QWidget *parent = nullptr);
    ~CreateBookingDialog();
    void addBikeInformation(const BikeData &bikeData);
    CreateBookingDialog::Result getResult(const BikeData &bikeData);
    void accept();

    static CreateBookingDialog::Result addCreateBookingDialog(BikeData bikeData, QWidget *parent = nullptr);
    static CreateBookingDialog::Result addCreateBookingDialog(BikeData bikeData, QStringList openBookings, QWidget *parent = nullptr);

private:
    Ui::CreateBookingDialog *ui;
};

#endif // CREATEBOOKINGDIALOG_H
