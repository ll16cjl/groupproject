#ifndef BOOKINGSVIEW_H
#define BOOKINGSVIEW_H

#include <QWidget>

namespace Ui {
class BookingsView;
}

class BookingsView : public QWidget
{
    Q_OBJECT

public:
    explicit BookingsView(QWidget *parent = nullptr);
    ~BookingsView();

private:
    Ui::BookingsView *ui;
};

#endif // BOOKINGSVIEW_H
