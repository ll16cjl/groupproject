#pragma once
#ifndef MANAGEMENTSCREEN_H
#define MANAGEMENTSCREEN_H

#include <QWidget>
#include "bookingpage.h"

namespace Ui {
    class ManagementScreen;
}

class ManagementScreen : public QWidget {
    Q_OBJECT

public:
    explicit ManagementScreen(QWidget *parent = nullptr);
    virtual ~ManagementScreen();
    void showEvent(QShowEvent *event);

signals:
    void logoutAttempt();
    void sendOpenBookingsMap(QMap<QString, BookingPage *> *openBookings);

private slots:
    void handleCurrentPage(QWidget *page);
    void addNewBookingPage();
    void removeBookingPage(BookingPage *page);
    void createInitialNavigation();

private:
    Ui::ManagementScreen *ui;
    int totalBookings = 0;
    QSettings s;
    QMap<QString, BookingPage *> *openBookings;
};

#endif /*MANAGEMENTSCREEN_H*/
