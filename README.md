# Ridr
Welcome to Ridr! Our system will provide you with a central server system to keep track of bikes for your rental business. This project provides you with a till management system, that allows you to manage your inventory and process in-store rentals. The project also provides you with a user friendly and responsive web app for users to be able to complete bookings.

## Contents
- [Group Members](#group)
- [Project Documentation](#documentation)
- [Test Logins](#testlogin)

## Group Members
COMP2913 Software Engineering Project.
Team members:
- Luke Hutton (sc17ljth)
- Alexander Naggar(sc17agan)
- Christopher London(ll16cjl)
- Vytautas Kontrimas(ll16vk)
- Joshua Keenan (sc17jk)
- Dom Hackl (sc18d2h)

## Project Documentation
- Scrum Master: Dom Hackl (Sprint 1), Luke Hutton (Sprint 2), Alex Naggar (Sprint 3)
- Project Brief: https://docs.google.com/document/d/1vVwvecaOXyX8U62logoWGHlywUdEbhn2XxB4zQVj6aA
- Wiki: https://gitlab.com/ll16cjl/groupproject/wikis/home

## Test logins
- (User Login) Email: `user@user.com` Password: `pass`
- (Staff Login) Email: `staff@staff.com`, Password: `pass`

# SCRUM Masters
## Sprint 1
Sprint 1 will have Dom Hackl as SCRUM master. His main aims are:
- bla

## Sprint 2
Sprint 2 will have Vytautas Kontrimas as SCRUM master. His main aims are: 
- To move from the basic prototype implementation to a more presentable application.
- Ensure the team sticks to the defined code style and conventions.
- Make sure any large issues are resolved in time for Sprint 3.
- Check that the components function separately and together.


## Sprint 3
Sprint 3 will have Chris London as SCRUM master. Chris said his main aims for sprint three are:
- Ensure all of the key deliverables are ready and at least close to being finished 
- Tie up any integration issues with the three components
- Clean up the codebases for all three components
- Ensure testing has been properly set up and ready to deploy. 
- Prioritize last minute bugs

# Meetings

## Meeting 1 (04/02/19)
Present:
- Chris
- Josh
- Alex
- Dom 
- Vytautas
- Luke

Decided on the project we will work on, likely tech to research for next meeting, discussed Git, issue and naming conventions. Dom will be the SCRUM master for sprint 1 and he has decided his main aims are to get the project off the ground in terms of planning the tech we will use, the designs of the frontend applications and ensuring all of the tech can interconnect correctly.

- Working on the bike rental system. Desktop application and web front end. 
- Use TODO’s to highlight points in code 
_Issues and bugs_
- All bugs and issues you find should be tracked on the Gitlab issue tracker and assigned to a person even if you fix it right away.

Person that makes an issue has to give an assignee instantly.

Issue labels:
- Feature Request
- Critical
- Minor
- Trivial
- Moderate

Branches
- Feature branches have: Feature-{FEATURENAME}-{USERNAME}
- Master branch only gets merged when in meeting so everyone signs it off

Technologies
- Use JSON for data transmission between front and back end, format for different objects should be defined in .JSON files and named appropriately. 
Web front end (Alex, Dom):
- React (JavaScript ES6+)
- CSS (SCSS)
- HTML
Desktop Application (Luke and Vyt):
- C++
- Qt
- Cutelyst REST api (https://cutelyst.org/2018/05/17/creating-restful-applications-with-qt-and-cutelyst)
Back-end (Chris, Josh):
- Java
- Play framework
Database (Josh, Chris):
- SQLite 
- Java API made by us (facilitate putting our data into correct format)

## Meeting 2 (11/02/19)
Present:
- Chris
- Josh
- Alex
- Dom 
- Vytautas
- Luke


Discussed key technologies we will be using and discussed the next steps after planning. Created some user stories to use for the design of the application. Organised into teams properly and started to ensure the technologies we wanted tyo use would be feasible.

_Actionable_ 
- Look into Kanban board for Gitlab [Chris]
- Database set up [Josh]
- Get Jooby fully set up and running with REST API [Chris and Josh]
- Get basic web server working with front-end [Alex and Dom]
- Get desktop app interfacing with web app (Log in or just send some data or something) [Luke and Vyt]
- Look into getting basic web server set up [Chris and Luke]

_Tests_
- Every feature has to have unit tests before merged into the main branch; All tests must pass before being merged into main branch 

_Front-end Tech_
- HTML
- SCSS for styling
- JavaScript (ES6+)
- React + Redux
- REST
- Packages:
- Babel to convert ES6+ to ES5
- Jest for Unit-Testing
- Enzyme and Sinon for test extension
-  Moment for getting date/time
- React-Router for routing by URL
- uuid for key generation
- Webpack

_Back-end Tech_
- Use MVC for the design of the web application
- Jooby framework for the back end

_Database_
- sqliteJDBC 3.23.1
- SQLite with JDBC swapping out for MySql if time allows
- Need some diagrams to show what tables will have 

_Database Schema_
User Table:
user ID (primary key) : integer
username : string
(hash of) password : string
email : string
privilege level : integer

Bicycle Table:
bicycle ID (primary key)  : integer
brand : string
model : string
price : float

Hire Table:
hire ID (primary key) : integer
bicycle ID : integer
user ID : integer
start date : date
end date : date

_Desktop application_
- C++
- Qt
- Cutelyst REST api (https://cutelyst.org/2018/05/17/creating-restful-applications-with-qt-and-cutelyst)

## Meeting 3 (04/02/19)
Present:
- Chris
- Alex
- Dom 
- Vytautas
- Luke

_Desktop design mockup_
Finished the early designs for the Week 1 mockup. The idea is to just get the look and layout working. We’ll implement the actual functionality later. (Design mockups are available on the meeting google doc).

_Web application_
React now works nicely with the end points that have been set up.

_Actionables_
- Continue work on getting the navigation to work properly with a stacked widget [Luke]
- Work with chris to get logging in working with the login end point [Chris and Vytautas]
- Convert server to python [Chris]
- Work on logging in on the web app, being sure to store auth token for future requests [Alex and Dom]


## Meeting 4 (19/02/19)
Present:
- Chris
- Alex
- Dom 
- Vytautas
- Luke

_Python server_
Basic endpoints are now up and working and up on the wiki.

_Desktop_app
Most of the design has been implemented in Qt now. However most of the functionality is not there yet, so the design is mostly a placeholder that matches the desktop app mockup.

_Actionables_
- Set up the database to store rental and booking information.
- Implement the login system for the desktop app.
- Implement basic navigation for the desktop app.
- Start implementing the communication between the desktop app and the backend server.
- Set up login system for the web frontend.

## Meeting 5 (22/02/19)
Present:
- Chris
- Alex
- Dom 
- Vytautas

_Server_
The database has now been expanded to support relational tables, where rental and booking information can be stored. Endpoints are now runnning for the frontend applications to use.

_Desktop App_
The desktop application now has a working login system and navigation. Bikes can be viewed from the inventory page and we have created ui's for many of the other pages, they just need implementing.

_Web App_
The web application now manages logins for customers only. The app can communicate well over the RESTapi, which proved to be difficult to get to work due to react technicalities.

_Actions_
- Create endpoints to view rentals that have been made [Chris]
- Start building and implementing the booking process [Luke]
- Implement dialog to add bikes and locations to the database using the endpoints [Vytautas]
- Copy inventory view to rental view [Vytautas]
- Show bikes at locations [Alex]
- Create location map using google maps api [Dom]

## Meeting 6 (12/03/19)
Present:
- Chris
- Luke
- Alex
- Dom 
- Vytautas

_Backend_
The backend has several new endpoints now, including the rentals endpoint for viewing existing rentals. The endpoint implementation has been improved.

_Desktop App_
More of the interface is now functional. The user is able to view existing inventory. Work has been done designing and partially implementing the booking dialog.


## Meeting 7 (26/03/19)
Present:
- Chris
- Luke
- Alex
- Dom 
- Vytautas

_Desktop App_
Looking into an address lookup api - getaddress.io. This will allow the user to input their postcode and get their address.

_Server_
Looking into qr codes for reciepts. These should be generated on the server side.

_Web_
Booking process is completed however need to update error messages.

_Actionable_
- Booking process still needs validation to make sure the correct data is being input [Luke]
- Update error messages and work on creating FAQ [Alex & Dom]
- Add qr codes to recipt generation [Chris]
- Get rentals view working correctly [Vytautas]

## Meeting 8 (20/04/19)
Present:
- Chris
- Alex
- Dom 
- Vytautas
- Luke

Discussed how we wish to tie everything together in terms of deploying the solution. Decided on creating a bash script that will start all of the required services for the solution to run. Cleaned the database up to add new data that fits with the endpoint changes, discussed common bugs that need to be ironed out and also discussed any major changes that we wished to make to the project (none stood out). Alex spoke about the website design. The things that were still left to do were also discussed.

_Bugs_
- Some endpoints return the wrong error code
- Front end gets different response to some endpoints than the desktop, need to look into how they work differently
- Desktop can't add multiple bikes at a time yet

_Organization_ 
- Chris will create bash script to run all three components
- Git branches still seem logical and organised, no changes needed

_Database_
- Significant changes to endpoints means some of the old data is incorrect and is causing crashes
- Need to rebuild and add new data

_Website design_
- Alex led us through what the website should look like once finished 
- Colour scheme decided

_Still to complete_
- Key website design 
- Fix some bugs on endpoints
- Polish desktop app rental creation screens
- Tidy up code base
- Finish unit tests for all three components
- Deploy solution and test once deployed


